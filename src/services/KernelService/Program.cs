using System;
using System.Threading.Tasks;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace KernelService.Host
{
    public class Program
    {
        private static ILogger _logger;

        public static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += (s, e) =>
            {
                if (!(e.ExceptionObject is Exception ex)) return;

                var msg = $"Unhandled exception of type {ex.GetType()} from sender of type {s?.GetType()}.";

                if (e.IsTerminating)
                    _logger.LogCritical(ex, msg);
                else
                    _logger.LogError(ex, msg);
            };

            TaskScheduler.UnobservedTaskException += (s, ex) =>
            {
                _logger.LogWarning(ex.Exception, nameof(TaskScheduler.UnobservedTaskException));
                ex.SetObserved();
            };

            var host = CreateHostBuilder(args).Build();
            _logger = host.Services.GetService<ILogger<Program>>();
            Console.Title = host.Services.GetService<IWebHostEnvironment>().ApplicationName;
            host.Run();
        }

        internal static IHostBuilder CreateHostBuilder(string[] args) =>
            Microsoft.Extensions.Hosting.Host.CreateDefaultBuilder(args)
                     .ConfigureWebHostDefaults(webBuilder => webBuilder.UseStartup<Startup>())
                     .UseAutofac()
                     .UseSerilog((ctx, loggerConfiguration) => loggerConfiguration
                           .ReadFrom.Configuration(ctx.Configuration)
                           .Enrich.WithProperty("app", ctx.HostingEnvironment.ApplicationName)
                           .Enrich.WithProperty("env", ctx.HostingEnvironment.EnvironmentName));
    }
}