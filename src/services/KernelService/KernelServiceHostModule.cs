﻿using System;
using AgentIdentity.Application;
using AgentIdentity.Application.Devices;
using AgentIdentity.Application.Devices.Identity;
using AgentIdentity.Application.UserAgents;
using AgentIdentity.HttpApi;
using AgentIdentity.MongoDB.MongoDB;
using EmployeeManagement;
using EmployeeManagement.MongoDB;
using KernelService.Host.Infrastructure.Filters;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using ScreenRecordingAdministration;
using StorageManagement.Application;
using StorageManagement.MongoDB.MongoDB;
using TimelineTracking;
using TimelineTracking.MongoDB;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc.Conventions;
using Volo.Abp.Auditing;
using Volo.Abp.AuditLogging.MongoDB;
using Volo.Abp.Autofac;
using Volo.Abp.EventBus;
using Volo.Abp.EventBus.SignalR;
using Volo.Abp.Modularity;

namespace KernelService.Host
{
    [DependsOn(typeof(AbpAutofacModule),
        typeof(AbpAuditLoggingMongoDbModule),
        typeof(AbpEventBusModule),
        typeof(AbpSignalRDistributedEventModule),
        typeof(AgentIdentityHttpApiModule),
        typeof(AgentIdentityMongoDbModule),
        typeof(AgentIdentityApplicationModule),
        typeof(EmployeeManagementApplicationModule),
        typeof(EmployeeManagementMongoDbModule),
        typeof(StorageManagementApplicationModule),
        typeof(ScreenRecordingMongoDbModule),
        typeof(TimelineTrackingApplicationModule),
        typeof(TimelineTrackingMongoDbModule))]
    public class KernelServiceHostModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            var configuration = context.Services.GetConfiguration();
#if DEBUG
            context.Services.AddAlwaysAllowAuthorization();
#endif

            context.Services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "CWT Kernel Service API", Version = "v1" });
                options.DocInclusionPredicate((docName, description) => true);
                options.CustomSchemaIds(type => type.FullName);
                options.OperationFilter<AuthorizeCheckOperationFilter>();
            });

            context.Services.AddAuthentication(options =>
                   {
                       options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                       options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                   })
                   .AddCookie(setup => setup.ExpireTimeSpan = TimeSpan.FromDays(2))
                   .AddIdentityServerAuthentication(JwtBearerDefaults.AuthenticationScheme,
                                                    options =>
                                                    {
                                                        options.Authority = configuration["AuthServer:Authority"];
                                                        options.ApiName = configuration["AuthServer:ApiName"];
                                                        options.RequireHttpsMetadata = false;
                                                    });

            context.Services.AddHttpContextAccessor();

            Configure<AbpAuditingOptions>(options =>
            {
                options.IsEnabledForGetRequests = true;
                options.ApplicationName = "KernelService";
            });

            Configure<AbpAspNetCoreMvcOptions>(options =>
            {
                options.ConventionalControllers.Create(typeof(AgentIdentityApplicationModule).Assembly,
                                                       opts => opts.TypePredicate = t
                                                           => t.FullName == typeof(UserAgentService).FullName
                                                           || t.FullName == typeof(DeviceService).FullName);
                options.ConventionalControllers.Create(typeof(AgentIdentityApplicationModule).Assembly,
                                                       ConfigureUserAgentAuthConventionalControllers);
                options.ConventionalControllers.Create(typeof(AgentIdentityApplicationModule).Assembly,
                                                       ConfigureDeviceIdentityConventionalControllers);
                options.ConventionalControllers.Create(typeof(EmployeeManagementApplicationModule).Assembly);
                options.ConventionalControllers.Create(typeof(StorageManagementApplicationModule).Assembly);
                options.ConventionalControllers.Create(typeof(TimelineTrackingApplicationModule).Assembly);
                options.ConventionalControllers.Create(typeof(ScreenRecordingAdministrationApplicationModule).Assembly);
            });
        }

        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
            var app = context.GetApplicationBuilder();

            if (!context.GetEnvironment().IsDevelopment())
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseCorrelationId();

            app.UseRouting();

            // Add authentication to request pipeline
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseAuthorization();
            //app.Use(async (ctx, next) =>
            //{
            //    var currentPrincipalAccessor = ctx.RequestServices.GetRequiredService<ICurrentPrincipalAccessor>();
            //    var map = new Dictionary<string, string>
            //    {
            //        { "sub", AbpClaimTypes.UserId },
            //        { "role", AbpClaimTypes.Role },
            //        { "email", AbpClaimTypes.Email }
            //        //any other map
            //    };
            //    var mapClaims = currentPrincipalAccessor.Principal.Claims.Where(p => map.Keys.Contains(p.Type)).ToList();
            //    currentPrincipalAccessor.Principal.AddIdentity(
            //        new ClaimsIdentity(mapClaims.Select(p => new Claim(map[p.Type], p.Value, p.ValueType, p.Issuer))));
            //    await next();
            //});
            
            //app.UseSignalR(builder => builder.MapHub<>())
            //app.UseEndpoints(builder => builder.MapHub<AgentsHub>("/agents/connection"));

            app.UseSwagger();
            app.UseSwaggerUI(options => { options.SwaggerEndpoint("/swagger/v1/swagger.json", "CWT Kernel Service API"); });
            app.UseAuditing();
            app.UseMvcWithDefaultRouteAndArea();
        }

        private void ConfigureUserAgentAuthConventionalControllers(ConventionalControllerSetting opts)
        {
            opts.TypePredicate = t => t.FullName == typeof(UserAgentAuthService).FullName;
            opts.RootPath = "agents";
            opts.UrlControllerNameNormalizer = urlActionNameNormalizerContext
                => string.Equals(urlActionNameNormalizerContext.ControllerName, "userAgentAuth", StringComparison.OrdinalIgnoreCase)
                    ? "userAgent"
                    : urlActionNameNormalizerContext.ControllerName;
        }

        private void ConfigureDeviceIdentityConventionalControllers(ConventionalControllerSetting opts)
        {
            opts.TypePredicate = t => t.FullName == typeof(DeviceIdentityService).FullName;
            opts.RootPath = "agents";
            opts.UrlControllerNameNormalizer = urlActionNameNormalizerContext
                => string.Equals(urlActionNameNormalizerContext.ControllerName, "deviceIdentity", StringComparison.OrdinalIgnoreCase)
                    ? "device"
                    : urlActionNameNormalizerContext.ControllerName;
        }
    }
}