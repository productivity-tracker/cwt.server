using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace KernelService.Host
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services) => services.AddApplication<KernelServiceHostModule>();

        public void Configure(IApplicationBuilder app) => app.InitializeApplication();
    }
}
