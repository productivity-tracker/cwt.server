﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using ScreenRecording.Application.Contracts.ScreenshotRecording;
using ScreenRecording.Application.ScreenshotRecording;
using ScreenRecording.HttpApi;
using ScreenRecording.HttpApi.Controllers;
using ScreenRecording.MinIO;
using Volo.Abp;
using Volo.Abp.Autofac;
using Volo.Abp.Modularity;

namespace StorageAdapter.Host
{
    [DependsOn(typeof(ScreenshotRecordingApplicationModule),
        typeof(ScreenRecordingMinIOModule),
        typeof(ScreenRecordingHttpApiModule),
        typeof(AbpAutofacModule))]
    public class StorageAdapterModule : AbpModule
    {
        internal const string SERVICE_NAME = "Storage Adapter API";

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            var publicUrl = context.Services.GetConfiguration().GetConnectionString("PublicUrl");
            context.Services.AddSingleton<IScreenshotUrlFactory>(new ScreenshotUrlFactory(publicUrl));

            context.Services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo { Title = SERVICE_NAME, Version = "v1" });
                options.DocInclusionPredicate((docName, description) => true);
                options.CustomSchemaIds(type => type.FullName);
            });
        }

        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
            var app = context.GetApplicationBuilder();

            app.UseCorrelationId();
            app.UseRouting();
            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", SERVICE_NAME);
            });

            app.UseMvcWithDefaultRouteAndArea();
        }
    }
}