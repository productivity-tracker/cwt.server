using System;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace StorageAdapter.Host
{
    public class Program
    {
        public static int Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
#if DEBUG
                         .MinimumLevel.Debug()
#else
                         .MinimumLevel.Information()
#endif
                         .Enrich.WithProperty("Application", "IdentityService")
                         .Enrich.FromLogContext()
                         .WriteTo.File("Logs/logs.txt")
                         .WriteTo.ColoredConsole()
                         .CreateLogger();

            try
            {
                Log.Information("Starting web host.");
                using (var host = CreateHostBuilder(args).Build())
                {
                    Console.Title = host.Services.GetService<IHostEnvironment>().ApplicationName;
                    Console.OutputEncoding = Encoding.UTF8;
                    host.Run();
                    return 0;
                }
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly!");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        internal static IHostBuilder CreateHostBuilder(string[] args)
            => Microsoft.Extensions.Hosting.Host.CreateDefaultBuilder(args)
                        .ConfigureWebHostDefaults(webBuilder =>
                            webBuilder.UseStartup<Startup>())
                        .UseAutofac()
                        .UseSerilog();
    }
}