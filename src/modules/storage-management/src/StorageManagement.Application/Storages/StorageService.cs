﻿using System;
using Microsoft.AspNetCore.Authorization;
using StorageManagement.Application.Contracts.Storages;
using StorageManagement.Application.Contracts.Storages.DTO;
using StorageManagement.Domain.AggregatesModel.StorageAggregate;
using Volo.Abp.Application.Services;

namespace StorageManagement.Application.Storages
{
    //[Authorize]
    public class StorageService : CrudAppService<Storage, StorageDto, Guid>, IStorageService
    {
        public StorageService(IStorageRepository repository)
            : base(repository)
        { }
    }
}