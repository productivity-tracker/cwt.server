﻿using AutoMapper;
using StorageManagement.Application.Contracts.Storages.DTO;
using StorageManagement.Domain.AggregatesModel.StorageAggregate;

namespace StorageManagement.Application.Storages
{
    public class StorageManagementAppAutoMapperProfile : Profile
    {
        public StorageManagementAppAutoMapperProfile()
        {
            CreateMap<Storage, StorageDto>(MemberList.Destination).ReverseMap();
        }
    }
}