﻿using StorageManagement.Application.Contracts;
using StorageManagement.Application.Contracts.Storages.DTO;
using StorageManagement.Domain;
using StorageManagement.Domain.AggregatesModel.StorageAggregate;
using Volo.Abp.Application;
using Volo.Abp.AutoMapper;
using Volo.Abp.EventBus.Distributed;
using Volo.Abp.Modularity;

namespace StorageManagement.Application
{
    [DependsOn(
        typeof(StorageManagementDomainModule),
        typeof(StorageManagementApplicationContractsModule),
        typeof(AbpDddApplicationModule),
        typeof(AbpAutoMapperModule))]
    public class StorageManagementApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpAutoMapperOptions>(options =>
            {
                options.AddMaps<StorageManagementApplicationModule>(validate: true);
            });

            Configure<AbpDistributedEventBusOptions>(options =>
            {
                options.EtoMappings.Add<Storage, StorageDto>();
            });
        }
    }
}