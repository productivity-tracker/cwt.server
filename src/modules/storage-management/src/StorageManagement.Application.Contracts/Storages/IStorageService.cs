﻿using System;
using StorageManagement.Application.Contracts.Storages.DTO;
using Volo.Abp.Application.Services;

namespace StorageManagement.Application.Contracts.Storages
{
    public interface IStorageService : ICrudAppService<StorageDto, Guid>
    {
        
    }
}