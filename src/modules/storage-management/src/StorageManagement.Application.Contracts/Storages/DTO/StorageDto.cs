﻿using System;
using System.ComponentModel.DataAnnotations;
using StorageManagement.Domain.Shared;
using Volo.Abp.Application.Dtos;

namespace StorageManagement.Application.Contracts.Storages.DTO
{
    [Serializable]
    public class StorageDto : AuditedEntityDto<Guid>
    {
        /// <summary>
        ///     Id from identity service
        /// </summary>
        public string AdapterId { get; set; }

        [Required]
        public StorageType Type { get; set; }

        /// <summary>
        ///     The name of storage
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        ///     Adapter connection endpoint, like: https://127.0.0.1:5000
        /// </summary>
        [Required]
        public string Endpoint { get; set; }

        public bool IsActive { get; set; }

        /// <summary>
        ///     Available disk space
        /// </summary>
        public double Capacity { get; set; }

        /// <summary>
        ///     Usage of disk space
        /// </summary>
        public double Usage { get; set; }
    }
}