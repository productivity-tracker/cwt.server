﻿using StorageManagement.Domain.Shared;
using Volo.Abp.Application;
using Volo.Abp.Authorization;
using Volo.Abp.Modularity;

namespace StorageManagement.Application.Contracts
{
    [DependsOn(
        typeof(ScreenManagementDomainSharedModule),
        typeof(AbpDddApplicationContractsModule),
        typeof(AbpAuthorizationModule))]
    public class StorageManagementApplicationContractsModule : AbpModule
    {
        
    }
}