﻿using Microsoft.Extensions.DependencyInjection;
using StorageManagement.Application.Contracts;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;

namespace StorageManagement.HttpApi.Client
{
    [DependsOn(
        typeof(StorageManagementApplicationContractsModule),
        typeof(AbpHttpClientModule))]
    public class StorageManagementHttpApiClientModule : AbpModule
    {
        public const string RemoteServiceName = "StorageManagement";

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddHttpClientProxies(typeof(StorageManagementApplicationContractsModule).Assembly,
                                                  RemoteServiceName);
        }
    }
}