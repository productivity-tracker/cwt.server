﻿using System;
using StorageManagement.Domain.AggregatesModel.StorageAggregate;
using StorageManagement.MongoDB.MongoDB;
using Volo.Abp.Domain.Entities.Events;
using Volo.Abp.Domain.Repositories.MongoDB;
using Volo.Abp.EventBus.Distributed;
using Volo.Abp.MongoDB;
using StorageAggregate = StorageManagement.Domain.AggregatesModel.StorageAggregate.Storage;

namespace StorageManagement.MongoDB.Storages
{
    public class StorageRepository : MongoDbRepository<IStorageManagementMongoDbContext, StorageAggregate, Guid>, 
                                     IStorageRepository
    {
        public StorageRepository(IMongoDbContextProvider<IStorageManagementMongoDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }
    }
}