﻿using MongoDB.Driver;
using Volo.Abp.MongoDB;
using StorageAggregate = StorageManagement.Domain.AggregatesModel.StorageAggregate.Storage;

namespace StorageManagement.MongoDB.MongoDB
{
    public class StorageManagementMongoDbContext : AbpMongoDbContext, IStorageManagementMongoDbContext
    {
        [MongoCollection(nameof(Storages))]
        public IMongoCollection<StorageAggregate> Storages => Collection<StorageAggregate>();
    }
}