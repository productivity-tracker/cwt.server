﻿using Microsoft.Extensions.DependencyInjection;
using StorageManagement.Domain;
using StorageManagement.MongoDB.Storages;
using Volo.Abp.Modularity;
using Volo.Abp.MongoDB;
using StorageAggregate = StorageManagement.Domain.AggregatesModel.StorageAggregate.Storage;

namespace StorageManagement.MongoDB.MongoDB
{
    [DependsOn(
        typeof(AbpMongoDbModule), 
        typeof(StorageManagementDomainModule))]
    public class ScreenRecordingMongoDbModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddMongoDbContext<StorageManagementMongoDbContext>(options =>
            {
                options.AddRepository<StorageAggregate, StorageRepository>();
            });
        }
    }
}