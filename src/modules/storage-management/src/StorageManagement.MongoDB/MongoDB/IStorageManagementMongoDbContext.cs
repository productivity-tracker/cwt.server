﻿using MongoDB.Driver;
using StorageManagement.Domain;
using Volo.Abp.Data;
using Volo.Abp.MongoDB;
using StorageAggregate = StorageManagement.Domain.AggregatesModel.StorageAggregate.Storage;

namespace StorageManagement.MongoDB.MongoDB
{
    [ConnectionStringName(StorageManagementConnectionDbProperties.ConnectionStringName)]
    public interface IStorageManagementMongoDbContext : IAbpMongoDbContext
    {
        IMongoCollection<StorageAggregate> Storages { get; }
    }
}