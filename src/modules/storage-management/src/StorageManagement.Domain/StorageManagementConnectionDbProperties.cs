﻿namespace StorageManagement.Domain
{
    public class StorageManagementConnectionDbProperties
    {
        public static string DbTablePrefix { get; set; } = "StorageMgmt";

        public static string DbSchema { get; set; } = null;

        public const string ConnectionStringName = "ScorageManagementConnection";
    }
}