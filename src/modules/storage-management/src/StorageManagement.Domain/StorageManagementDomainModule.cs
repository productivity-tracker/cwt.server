﻿using StorageManagement.Domain.Shared;
using Volo.Abp.Domain;
using Volo.Abp.Modularity;

namespace StorageManagement.Domain
{
    [DependsOn(
        typeof(ScreenManagementDomainSharedModule),
        typeof(AbpDddDomainModule))]
    public class StorageManagementDomainModule : AbpModule
    {
        
    }
}