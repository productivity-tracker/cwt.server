﻿using System;
using Volo.Abp.Domain.Repositories;

namespace StorageManagement.Domain.AggregatesModel.StorageAggregate
{
    public interface IStorageRepository : IRepository<Storage, Guid>
    { }
}