﻿using System;
using StorageManagement.Domain.Shared;
using Volo.Abp.Domain.Entities.Auditing;

namespace StorageManagement.Domain.AggregatesModel.StorageAggregate
{
    public class Storage : FullAuditedAggregateRoot<Guid>, IEquatable<Storage>
    {
        /// <summary>
        ///     Id from identity service
        /// </summary>
        public string AdapterId { get; private set; }

        public StorageType Type { get; private set; }

        /// <summary>
        ///     The name of storage
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        ///     Adapter connection endpoint, like: https://127.0.0.1:5000
        /// </summary>
        public string Endpoint { get; private set; }

        public bool IsActive { get; private set; }

        /// <summary>
        ///     Available disk space
        /// </summary>
        public double Capacity { get; private set; }

        /// <summary>
        ///     Usage of disk space
        /// </summary>
        public double Usage { get; private set; }

        public Storage(Guid id,
                       string adapterId,
                       StorageType type,
                       string name,
                       string endpoint,
                       bool isActive,
                       double capacity,
                       double usage) : base(id)
        {
            AdapterId = adapterId;
            Type = type;
            Name = name;
            Endpoint = endpoint;
            IsActive = isActive;
            Capacity = capacity;
            Usage = usage;
        }

        protected Storage()
        { }

        #region IEquatable Support

        public bool Equals(Storage other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return AdapterId == other.AdapterId && Type == other.Type && Name == other.Name && Endpoint == other.Endpoint &&
                   IsActive == other.IsActive && Capacity.Equals(other.Capacity) && Usage.Equals(other.Usage);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return EntityEquals(obj) && Equals((Storage)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = AdapterId != null ? AdapterId.GetHashCode() : 0;
                hashCode = (hashCode * 397) ^ (int)Type;
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Endpoint != null ? Endpoint.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ IsActive.GetHashCode();
                hashCode = (hashCode * 397) ^ Capacity.GetHashCode();
                hashCode = (hashCode * 397) ^ Usage.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(Storage left, Storage right) => Equals(left, right);

        public static bool operator !=(Storage left, Storage right) => !Equals(left, right);

        #endregion
    }
}