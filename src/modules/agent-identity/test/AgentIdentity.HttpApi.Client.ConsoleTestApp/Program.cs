﻿using System;
using System.Threading.Tasks;
using AgentIdentity.Application.Contracts.Devices;
using AgentIdentity.Application.Contracts.Devices.Identity.DTO;
using AgentIdentity.Domain.Shared.Device;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp;

namespace AgentIdentity.HttpApi.Client.ConsoleTestApp
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            using var app = AbpApplicationFactory.Create<AgentApplicationModule>(options => options.UseAutofac());
            app.Initialize();

            //app.Shutdown();

            //Resolve a service and use it
            var deviceService = app.ServiceProvider.GetRequiredService<IDeviceService>();
            var request = new RegistrationRequest("jhkjgfdsdfg",
                                                  "Test1",
                                                  "Test",
                                                  "bl",
                                                  "win",
                                                  "1.0");
            await deviceService.RegisterAsync(request);

            deviceService.PutStateAsync("234234", DeviceState.Unknown);

            //var userAgentService = app.ServiceProvider.GetService<IUserAgentService>();
            //await userAgentService.LogOnAsync(new LogonRequest("denis", "bl", false, "Test1", "Denis S."));

            Console.WriteLine("Press ENTER to stop application...");
            Console.ReadLine();
        }
    }
}