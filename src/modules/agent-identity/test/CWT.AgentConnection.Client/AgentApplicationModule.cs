﻿using CWT.AgentConnection.HttpApi.Client;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Autofac;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;

namespace CWT.AgentConnection.Client
{
    [DependsOn(typeof(AbpAutofacModule), typeof(AgentConnectionClientAppModule))]
    public class AgentApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.Configure<AbpRemoteServiceOptions>(options =>
            {
                options.RemoteServices.Default = 
                    new RemoteServiceConfiguration("http://localhost:5000/");
            });
        }
    }
}