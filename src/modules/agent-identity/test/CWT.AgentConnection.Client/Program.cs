﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using CWT.AgentConnection.Application.Contracts.Devices;
using CWT.AgentConnection.Application.Contracts.Devices.DTO;
using CWT.AgentConnection.Domain.Shared.Device;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Volo.Abp;

namespace CWT.AgentConnection.Client
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using var app = AbpApplicationFactory.Create<AgentApplicationModule>(options => options.UseAutofac());
            app.Initialize();

            app.Shutdown();

            //Resolve a service and use it
            var deviceService = app.ServiceProvider.GetRequiredService<IDeviceService>();
            var request = new RegistrationRequest("jhkjgfdsdfg", "Test1", "Test", "bl", "win", "1.0");
            await deviceService.RegisterAsync(request);

            deviceService.SetState("234234", DeviceState.Unknown);

            //var userAgentService = app.ServiceProvider.GetService<IUserAgentService>();
            //await userAgentService.LogOnAsync(new LogonRequest("denis", "bl", false, "Test1", "Denis S."));

            Console.WriteLine("Press ENTER to stop application...");
            Console.ReadLine();
        }

        private static void AuthenticateUser()
        {
            var cookies = new CookieContainer();
            var handler = new HttpClientHandler
            {
                CookieContainer = cookies
            };

            var client = new HttpClient(handler);
            var response = client.GetAsync("http://google.com").Result;

            var uri = new Uri("http://google.com");
            var responseCookies = cookies.GetCookies(uri).Cast<Cookie>();
            foreach (var cookie in responseCookies)
                Console.WriteLine(cookie.Name + ": " + cookie.Value);

            Console.ReadLine();
        }
    }
}
