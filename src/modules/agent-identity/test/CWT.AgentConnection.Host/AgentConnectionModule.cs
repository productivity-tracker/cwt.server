﻿using CWT.AgentConnection.Application;
using CWT.AgentConnection.HttpApi;
using CWT.AgentConnection.HttpApi.Hubs;
using CWT.AgentConnection.MongoDB.MongoDB;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp;
using Volo.Abp.Autofac;
using Volo.Abp.Modularity;
using Microsoft.OpenApi.Models;

namespace CWT.AgentConnection.Host
{
    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(AgentConnectionHttpApiModule),
        typeof(AgentConnectionMongoDbModule),
        typeof(AgentConnectionApplicationModule))]
    public class AgentConnectionModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddSignalR();

            context.Services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo {Title = "Agent Service API", Version = "v1"});
                options.DocInclusionPredicate((docName, description) => true);
                options.CustomSchemaIds(type => type.FullName);
            });
        }

        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
            var app = context.GetApplicationBuilder();

            app.UseCorrelationId();
            app.UseRouting();
            app.UseEndpoints(builder => builder.MapHub<AgentHub>("/agent/connection"));

            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "Agent Service API");
            });

            app.UseMvcWithDefaultRouteAndArea();
        }
    }
}