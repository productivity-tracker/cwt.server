﻿using System;
using System.Threading.Tasks;
using AgentIdentity.Application.Contracts.Internal.Agents;
using AgentIdentity.Application.Contracts.Internal.Agents.RealTime;
using AgentIdentity.HttpApi.Extensions;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Volo.Abp.AspNetCore.SignalR;

namespace AgentIdentity.HttpApi.Hubs
{
    [HubRoute("/agents/connection")] 
    public class AgentsHub : Hub
    {
        private readonly IAgentService _agentService;
        private readonly ILogger _logger;

        public AgentsHub(ILogger<AgentsHub> logger, IAgentService agentService)
        {
            _agentService = agentService;
            _logger = logger;
        }

        public override Task OnConnectedAsync()
        {
            var client = new OnlineClient(Context.ConnectionId, Context.GetHttpContext()?.GetIp());
            _agentService.Add(client);
            _logger.LogDebug($"Client connected, connectionId: '{Context.ConnectionId}'");
            return base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            _agentService.Remove(Context.ConnectionId);
            _logger.LogDebug($"Client disconnected, connectionId: '{Context.ConnectionId}';");

            await base.OnDisconnectedAsync(exception);
        }
    }
}