﻿using AgentIdentity.Application.Contracts.Internal;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Modularity;

namespace AgentIdentity.HttpApi
{
    [DependsOn(typeof(AbpAspNetCoreMvcModule), typeof(AgentIdentityAppContractsInternalModule))]
    public class AgentIdentityHttpApiModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
            => PreConfigure<IMvcBuilder>(mvcBuilder => { mvcBuilder.AddApplicationPartIfNotExists(typeof(AgentIdentityHttpApiModule).Assembly); });
    }
}