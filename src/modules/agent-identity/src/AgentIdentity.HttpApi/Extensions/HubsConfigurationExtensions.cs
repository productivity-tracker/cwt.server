﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.SignalR;

namespace AgentIdentity.HttpApi.Extensions
{
    public static class HubsConfigurationExtensions
    {
        private static readonly MethodInfo _mapHubMethod = typeof(HubEndpointConventionBuilder).GetMethod("MapHub", new[] { typeof(PathString) });

        public static IEndpointRouteBuilder MapSignalR(this IEndpointRouteBuilder routeBuilder, params Assembly[] assemblies)
        {
            var targetAssemblies = assemblies.Any() ? assemblies : new[] { Assembly.GetExecutingAssembly() };

            foreach (var assembly in targetAssemblies)
            {
                var hubTypes = assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(Hub)) && !t.IsAbstract).ToList();

                foreach (var hubType in hubTypes)
                {
                    var path = $"/{hubType.Name}".RemovePostFix("Hub");
                    _mapHubMethod.MakeGenericMethod(hubType).Invoke(routeBuilder, new object[] { new PathString(path) });
                }
            }

            return routeBuilder;
        }
    }
}