﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using AgentIdentity.Domain.AggregatesModel.UserAgentAggregate;
using AgentIdentity.Domain.Services.Devices;
using Volo.Abp;
using Volo.Abp.DependencyInjection;

namespace AgentIdentity.Domain.Services.UserAgents
{
    public class UserAgentManager : ISingletonDependency
    {
        private readonly IUserAgentRepository _userAgentRepository;
        private readonly ConcurrentDictionary<string, User> _onlineUsers;
        public event EventHandler<UserAgentStateChangedEventArgs> UserLoggedOff;

        public event EventHandler<UserAgentStateChangedEventArgs> UserLoggedOn;

        public UserAgentManager(IUserAgentRepository userAgentRepository)
        {
            _userAgentRepository = userAgentRepository;
            _onlineUsers = new ConcurrentDictionary<string, User>();
        }

        public async Task<UserAgent> LogOnOrRegisterAsync(User user, DeviceIdentifier deviceIdentifier)
        {
            var userAgentId = CalculateUniqueName(user.UserName, user.Domain, user.IsLocalUser, deviceIdentifier);
            var userAgent = await _userAgentRepository.FindAsync(userAgentId);
            if (userAgent == null)
            {
                userAgent = await RegisterAsync(userAgentId, user);
            }
            else
            {
                var loggedOnUser = userAgent.LogOn();
                userAgent = await _userAgentRepository.UpdateAsync(loggedOnUser);
            }

            _onlineUsers.AddOrUpdate(userAgentId, user, (id, oldUser) => user);
            RaiseUserAgentStateChanged(userAgentId, deviceIdentifier.CalculateId(), isOnline: true);
            return userAgent;
        }

        public void LogOff(User user, DeviceIdentifier deviceIdentifier)
        {
            var userAgentId = CalculateUniqueName(user.UserName, user.Domain, user.IsLocalUser, deviceIdentifier);
            _onlineUsers.TryRemove(userAgentId, out _);
            RaiseUserAgentStateChanged(userAgentId, deviceIdentifier.CalculateId(), isOnline: false);
        }

        public bool IsOnline(string userAgentId)
        {
            Check.NotNullOrWhiteSpace(userAgentId, nameof(userAgentId));

            return _onlineUsers.ContainsKey(userAgentId);
        }

        private string CalculateUniqueName(string userName,
                                           string domain,
                                           bool isLocalUser,
                                           DeviceIdentifier deviceIdentifier)
        {
            var userLocation = !isLocalUser && !domain.IsNullOrWhiteSpace() ? domain : deviceIdentifier?.UniversallyUniqueId;
            return $"{userName}@{userLocation}".ToUpper();
        }

        private async Task<UserAgent> RegisterAsync(string uniqueUserName, User user)
        {
            var userAgent = new UserAgent(uniqueUserName, user);
            return await _userAgentRepository.InsertAsync(userAgent);
        }

        private void RaiseUserAgentStateChanged(string userAgentId, string deviceId, bool isOnline)
        {
            var arg = new UserAgentStateChangedEventArgs(userAgentId, deviceId);
            if (isOnline)
            {
                UserLoggedOn?.Invoke(this, arg);
            }
            else
            {
                UserLoggedOff?.Invoke(this, arg);
            }
        }
    }
}