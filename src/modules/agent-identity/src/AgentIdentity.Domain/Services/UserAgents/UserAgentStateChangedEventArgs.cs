﻿using System;

namespace AgentIdentity.Domain.Services.UserAgents
{
    public class UserAgentStateChangedEventArgs : EventArgs
    {
        public string UserAgentId { get; }
        public string DeviceId { get; }

        public UserAgentStateChangedEventArgs(string userAgentId, string deviceId)
        {
            UserAgentId = userAgentId;
            DeviceId = deviceId;
        }
    }
}