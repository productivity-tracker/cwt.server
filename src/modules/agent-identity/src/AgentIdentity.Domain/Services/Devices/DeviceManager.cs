﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using AgentIdentity.Domain.AggregatesModel.DeviceAggregate;
using AgentIdentity.Domain.Services.UserAgents;
using AgentIdentity.Domain.Shared.Device;
using Volo.Abp;
using Volo.Abp.DependencyInjection;

namespace AgentIdentity.Domain.Services.Devices
{
    public class DeviceManager : ISingletonDependency, IDisposable
    {
        private readonly IDeviceRepository _deviceRepository;
        private readonly UserAgentManager _userAgentManager;
        private readonly ConcurrentDictionary<string, DeviceState> _states;

        public event EventHandler<DeviceStateChangedEventArgs> StateChanged;

        public DeviceManager(IDeviceRepository deviceRepository, UserAgentManager userAgentManager)
        {
            _deviceRepository = deviceRepository;
            _userAgentManager = userAgentManager;
            _states = new ConcurrentDictionary<string, DeviceState>();

            _userAgentManager.UserLoggedOn += OnUserLoggedOn;
        }

        public void Dispose() => _userAgentManager.UserLoggedOn -= OnUserLoggedOn;

        public async Task<Device> RegisterAsync(DeviceIdentifier identifier,
                                                DeviceType deviceType,
                                                string agentVersion,
                                                SystemInfo systemInfo,
                                                DeviceState deviceState = DeviceState.Locked)
        {
            var deviceId = identifier.CalculateId();
            var device = await _deviceRepository.FindAsync(deviceId);
            if (device == null)
            {
                var newDevice = new Device(deviceId, deviceType, systemInfo, agentVersion);
                device = await _deviceRepository.InsertAsync(newDevice);
            }

            var connectedDevice = device.Connect(agentVersion, systemInfo);
            await _deviceRepository.UpdateAsync(connectedDevice);
            _states.AddOrUpdate(deviceId, deviceState, (id, state) => deviceState);

            RaiseStateChanged(deviceId, deviceState);

            return connectedDevice;
        }

        public void SetDeviceState(DeviceIdentifier identifier, DeviceState newState)
        {
            var deviceId = identifier.CalculateId();
            _states.AddOrUpdate(deviceId, newState, (id, state) => newState);
            RaiseStateChanged(deviceId, newState);
        }

        public bool IsOnline(string deviceId)
        {
            Check.NotNullOrWhiteSpace(deviceId, nameof(deviceId));

            return GetState(deviceId) != DeviceState.Unknown;
        }

        public DeviceState GetState(string deviceId)
        {
            Check.NotNullOrWhiteSpace(deviceId, nameof(deviceId));

            return _states.TryGetValue(deviceId, out var state) ? state : DeviceState.Unknown;
        }

        private async void OnUserLoggedOn(object sender, UserAgentStateChangedEventArgs arg)
        {
            var device = await _deviceRepository.FindAsync(arg.DeviceId);
            if (device == null || device.LastLoggedOnUserAgentId == arg.UserAgentId)
            {
                return;
            }

            var updatedDevice = device.SetLoggedUser(arg.UserAgentId);
            await _deviceRepository.UpdateAsync(updatedDevice);
        }

        private void RaiseStateChanged(string deviceId, DeviceState state)
            => StateChanged?.Invoke(this, new DeviceStateChangedEventArgs(deviceId, state));
    }
}