﻿using System;

namespace AgentIdentity.Domain.Services.Devices
{
    public class DeviceIdentifier : IEquatable<DeviceIdentifier>
    {
        public string UniversallyUniqueId { get; private set; }

        public DeviceIdentifier(string universallyUniqueId)
        {
            UniversallyUniqueId = universallyUniqueId;
        }

        internal string CalculateId() => UniversallyUniqueId;

        #region IEquatable Support

        public bool Equals(DeviceIdentifier other)
        {
            if (ReferenceEquals(objA: null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(UniversallyUniqueId, other.UniversallyUniqueId);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(objA: null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((DeviceIdentifier)obj);
        }

        public override int GetHashCode() => UniversallyUniqueId != null ? UniversallyUniqueId.GetHashCode() : 0;

        public static bool operator ==(DeviceIdentifier left, DeviceIdentifier right) => Equals(left, right);

        public static bool operator !=(DeviceIdentifier left, DeviceIdentifier right) => !Equals(left, right);

        #endregion
    }
}