﻿using System;
using AgentIdentity.Domain.Shared.Device;

namespace AgentIdentity.Domain.Services.Devices
{
    public class DeviceStateChangedEventArgs : EventArgs
    {
        public string DeviceId { get; }
        public DeviceState State { get; }

        public DeviceStateChangedEventArgs(string deviceId, DeviceState state)
        {
            DeviceId = deviceId;
            State = state;
        }
    }
}