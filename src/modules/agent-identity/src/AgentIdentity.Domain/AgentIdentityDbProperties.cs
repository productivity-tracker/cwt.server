﻿namespace AgentIdentity.Domain
{
    public class AgentIdentityDbProperties
    {
        public const string ConnectionStringName = "AgentIdentity";
        public static string DbTablePrefix { get; set; } = "AgentIdentity";

        public static string DbSchema { get; set; } = null;
    }
}