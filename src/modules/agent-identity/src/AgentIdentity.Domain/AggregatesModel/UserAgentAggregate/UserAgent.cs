﻿using System;
using Volo.Abp.Domain.Entities;

namespace AgentIdentity.Domain.AggregatesModel.UserAgentAggregate
{
    public class UserAgent : AggregateRoot<string>, IEquatable<UserAgent>
    {
        public User User { get; private set; }
        public DateTime LastLoggedOnDate { get; private set; }
        public Guid EmployeeId { get; private set; }

        internal UserAgent(string uniqueName, User user)
            : base(uniqueName)
        {
            User = user;
            LastLoggedOnDate = DateTime.UtcNow;
        }

        protected UserAgent()
        { }

        public UserAgent AssignEmployee(Guid employeeId)
        {
            EmployeeId = employeeId;
            return this;
        }

        internal UserAgent LogOn()
        {
            LastLoggedOnDate = DateTime.UtcNow;
            return this;
        }

        #region IEquatable Support

        public bool Equals(UserAgent other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id == other.Id && Equals(User, other.User) && LastLoggedOnDate.Equals(other.LastLoggedOnDate) &&
                   EmployeeId.Equals(other.EmployeeId);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return EntityEquals(obj) && Equals((UserAgent)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Id != null ? Id.GetHashCode() : 0;
                hashCode = (hashCode * 397) ^ (User != null ? User.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ LastLoggedOnDate.GetHashCode();
                hashCode = (hashCode * 397) ^ EmployeeId.GetHashCode();
                return hashCode;
            }
        }

        #endregion
    }
}