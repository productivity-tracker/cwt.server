﻿using System;

namespace AgentIdentity.Domain.AggregatesModel.UserAgentAggregate
{
    public class User : IEquatable<User>
    {
        public string DisplayName { get; internal set; }
        public string UserName { get; private set; }
        public string Domain { get; private set; }
        public bool IsLocalUser { get; private set; }

        private User()
        { }

        public User(string displayName,
                    string userName,
                    string domain,
                    bool isLocalUser)
        {
            DisplayName = displayName;
            UserName = userName;
            Domain = domain;
            IsLocalUser = isLocalUser;
        }

        #region IEquatable Support

        public bool Equals(User other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return DisplayName == other.DisplayName && UserName == other.UserName && Domain == other.Domain && IsLocalUser == other.IsLocalUser;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((User)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = DisplayName != null ? DisplayName.GetHashCode() : 0;
                hashCode = (hashCode * 397) ^ (UserName != null ? UserName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Domain != null ? Domain.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ IsLocalUser.GetHashCode();
                return hashCode;
            }
        }

        #endregion
    }
}