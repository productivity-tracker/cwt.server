﻿using Volo.Abp.Domain.Repositories;

namespace AgentIdentity.Domain.AggregatesModel.UserAgentAggregate
{
    public interface IUserAgentRepository : IRepository<UserAgent, string>
    { }
}