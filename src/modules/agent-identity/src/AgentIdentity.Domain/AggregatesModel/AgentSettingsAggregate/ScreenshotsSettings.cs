﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CWT.AgentConnection.Domain.AggregatesModel.AgentSettingsAggregate
{
    public class ScreenshotsSettings : IEquatable<ScreenshotsSettings>
    {
        public bool EnableCaptureDesktop { get; private set; }
        public TimeSpan ScreenshotsInterval { get; private set; }
        public bool EnableObserveProcess { get; private set; }
        public List<string> ObservedProcesses { get; private set; }
        public ImageFormat ImageFormat { get; private set; }
        public long Quality { get; private set; }
        public TimeSpan ScreenHistoryTtl { get; private set; }

        public ScreenshotsSettings(bool enableCaptureDesktop,
                                   TimeSpan screenshotsInterval,
                                   bool enableObserveProcess,
                                   IEnumerable<string> observedProcesses,
                                   ImageFormat imageFormat,
                                   long quality,
                                   TimeSpan screenHistoryTtl)
        {
            EnableCaptureDesktop = enableCaptureDesktop;
            ScreenshotsInterval = screenshotsInterval;
            EnableObserveProcess = enableObserveProcess;
            ObservedProcesses = observedProcesses.ToList();
            ImageFormat = imageFormat;
            Quality = quality;
            ScreenHistoryTtl = screenHistoryTtl;
        }

        private ScreenshotsSettings()
        { }

        #region IEquatable Support

        public bool Equals(ScreenshotsSettings other)
        {
            if (ReferenceEquals(objA: null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return EnableCaptureDesktop == other.EnableCaptureDesktop && ScreenshotsInterval.Equals(other.ScreenshotsInterval) &&
                   EnableObserveProcess == other.EnableObserveProcess && Equals(ObservedProcesses, other.ObservedProcesses) &&
                   string.Equals(ImageFormat, other.ImageFormat) && Quality == other.Quality &&
                   ScreenHistoryTtl.Equals(other.ScreenHistoryTtl);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(objA: null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != GetType())
            {
                return false;
            }

            return Equals((ScreenshotsSettings)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = EnableCaptureDesktop.GetHashCode();
                hashCode = (hashCode * 397) ^ ScreenshotsInterval.GetHashCode();
                hashCode = (hashCode * 397) ^ EnableObserveProcess.GetHashCode();
                hashCode = (hashCode * 397) ^ (ObservedProcesses != null ? ObservedProcesses.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (ImageFormat != null ? ImageFormat.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Quality.GetHashCode();
                hashCode = (hashCode * 397) ^ ScreenHistoryTtl.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(ScreenshotsSettings left, ScreenshotsSettings right) => Equals(left, right);

        public static bool operator !=(ScreenshotsSettings left, ScreenshotsSettings right) => !Equals(left, right);

        #endregion
    }
}