﻿using System;
using Volo.Abp.Domain.Repositories;

namespace CWT.AgentConnection.Domain.AggregatesModel.AgentSettingsAggregate
{
    public interface IAgentSettingsRepository : IRepository<AgentSettings, Guid>
    { }
}