﻿using System;
using Volo.Abp.Domain.Entities.Auditing;

namespace CWT.AgentConnection.Domain.AggregatesModel.AgentSettingsAggregate
{
    public class AgentSettings : FullAuditedAggregateRoot<Guid>, IEquatable<AgentSettings>
    {
        public string Name { get; private set; }
        public bool IsActive { get; private set; }
        public bool IsDefault { get; private set; }
        public ScreenshotsSettings ScreenshotsSettings { get; private set; }
        public Guid StorageAdapterId { get; private set; }

        protected AgentSettings()
        { }

        public AgentSettings(Guid id, string name, bool isActive,
                            bool isDefault,
                            ScreenshotsSettings screenshotsSettings,
                            Guid storageAdapterId)
            : base(id)
        {
            Name = name;
            IsActive = isActive;
            IsDefault = isDefault;
            ScreenshotsSettings = screenshotsSettings;
            StorageAdapterId = storageAdapterId;
        }

        public AgentSettings SetDefault(bool isDefault)
        {
            var clone = Clone();
            clone.IsDefault = isDefault;
            return clone;
        }

        private AgentSettings Clone() => new AgentSettings(Id, Name, IsActive, IsDefault, ScreenshotsSettings, StorageAdapterId);

        #region IEquatable Support

        public bool Equals(AgentSettings other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Name == other.Name && IsActive == other.IsActive && IsDefault == other.IsDefault &&
                   Equals(ScreenshotsSettings, other.ScreenshotsSettings) && StorageAdapterId.Equals(other.StorageAdapterId);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((AgentSettings)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Name != null ? Name.GetHashCode() : 0;
                hashCode = (hashCode * 397) ^ IsActive.GetHashCode();
                hashCode = (hashCode * 397) ^ IsDefault.GetHashCode();
                hashCode = (hashCode * 397) ^ (ScreenshotsSettings != null ? ScreenshotsSettings.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ StorageAdapterId.GetHashCode();
                return hashCode;
            }
        }

        #endregion
    }
}