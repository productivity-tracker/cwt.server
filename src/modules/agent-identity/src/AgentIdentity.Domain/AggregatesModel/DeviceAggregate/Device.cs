﻿using System;
using AgentIdentity.Domain.Shared.Device;
using Volo.Abp.Domain.Entities;

namespace AgentIdentity.Domain.AggregatesModel.DeviceAggregate
{
    public class Device : AggregateRoot<string>, IEquatable<Device>
    {
        public DeviceType Type { get; private set; }
        public SystemInfo System { get; private set; }
        public string CurrentAgentVersion { get; private set; }
        public DateTime LastConnectionDate { get; private set; }
        public string LastLoggedOnUserAgentId { get; private set; }

        internal Device(string id,
                        DeviceType type,
                        SystemInfo system,
                        string currentAgentVersion)
            : base(id)
        {
            Type = type;
            System = system;
            CurrentAgentVersion = currentAgentVersion;
        }

        protected Device()
        { }

        internal Device Connect(string agentVersion, SystemInfo about)
        {
            LastConnectionDate = DateTime.UtcNow;
            CurrentAgentVersion = agentVersion;
            System = about;
            return this;
        }

        internal Device SetLoggedUser(string userAgentId)
        {
            LastLoggedOnUserAgentId = userAgentId;
            return this;
        }

        #region IEquatable Support

        public bool Equals(Device other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Type == other.Type && Equals(System, other.System) && CurrentAgentVersion == other.CurrentAgentVersion &&
                   LastConnectionDate.Equals(other.LastConnectionDate) && LastLoggedOnUserAgentId.Equals(other.LastLoggedOnUserAgentId);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return EntityEquals(obj) && Equals((Device)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (int)Type;
                hashCode = (hashCode * 397) ^ (System != null ? System.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (CurrentAgentVersion != null ? CurrentAgentVersion.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ LastConnectionDate.GetHashCode();
                hashCode = (hashCode * 397) ^ LastLoggedOnUserAgentId.GetHashCode();
                return hashCode;
            }
        }

        #endregion
    }
}