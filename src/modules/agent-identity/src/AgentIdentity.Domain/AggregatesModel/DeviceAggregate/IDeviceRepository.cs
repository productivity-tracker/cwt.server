﻿using Volo.Abp.Domain.Repositories;

namespace AgentIdentity.Domain.AggregatesModel.DeviceAggregate
{
    public interface IDeviceRepository : IRepository<Device, string>
    { }
}