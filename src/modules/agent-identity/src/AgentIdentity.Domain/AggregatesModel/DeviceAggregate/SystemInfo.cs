﻿using System;

namespace AgentIdentity.Domain.AggregatesModel.DeviceAggregate
{
    public class SystemInfo : IEquatable<SystemInfo>
    {
        public string Domain { get; private set; }
        public string DeviceName { get; private set; }
        public string IpAddress { get; private set; }
        public string OperatingSystem { get; private set; }

        private SystemInfo()
        { }

        public SystemInfo(string domain,
                          string deviceName,
                          string ipAddress,
                          string operatingSystem)
        {
            Domain = domain;
            DeviceName = deviceName;
            IpAddress = ipAddress;
            OperatingSystem = operatingSystem;
        }

        #region IEquatable Support

        public bool Equals(SystemInfo other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Domain == other.Domain && DeviceName == other.DeviceName && IpAddress == other.IpAddress &&
                   OperatingSystem == other.OperatingSystem;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((SystemInfo)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Domain != null ? Domain.GetHashCode() : 0;
                hashCode = (hashCode * 397) ^ (DeviceName != null ? DeviceName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (IpAddress != null ? IpAddress.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (OperatingSystem != null ? OperatingSystem.GetHashCode() : 0);
                return hashCode;
            }
        }

        #endregion
    }
}