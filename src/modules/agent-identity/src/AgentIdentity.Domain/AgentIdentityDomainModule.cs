﻿using AgentIdentity.Domain.Shared;
using Volo.Abp.Domain;
using Volo.Abp.Modularity;

namespace AgentIdentity.Domain
{
    [DependsOn(typeof(AbpDddDomainModule), typeof(AgentIdentityDomainSharedModule))]
    public class AgentIdentityDomainModule : AbpModule
    { }
}