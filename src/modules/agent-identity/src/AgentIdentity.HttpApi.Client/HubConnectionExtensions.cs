﻿using System;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace AgentIdentity.HttpApi.Client
{
    public static class HubConnectionExtensions
    {
        public static void AddHubConnection(this IServiceCollection services, Uri url)
        {
            var connectionBuilder = new HubConnectionBuilder().ConfigureLogging(builder =>
                                                              {
#if DEBUG
                                                                  builder.SetMinimumLevel(LogLevel.Debug);
#elif RELEASE
                                                                  builder.SetMinimumLevel(LogLevel.Information);
#endif
                                                              })
                                                              .WithAutomaticReconnect()
                                                              .WithUrl(url);
            services.AddSingleton(connectionBuilder.Build());
        }
    }
}