﻿using System;
using AgentIdentity.Application.Contracts;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;

namespace AgentIdentity.HttpApi.Client
{
    [DependsOn(typeof(AbpHttpClientModule), typeof(AgentIdentityAppContractsModule))]
    public class AgentIdentityClientAppModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            //Create dynamic client proxies
            context.Services.AddHttpClientProxies(typeof(AgentIdentityAppContractsModule).Assembly);

            //context.Services.AddHubConnection(new Uri("http://localhost:5000/agents/connection"));
        }

        //public override void OnApplicationInitialization(ApplicationInitializationContext context)
        //{
        //    var hubConnection = context.ServiceProvider.GetRequiredService<HubConnection>();
        //    hubConnection.StartAsync().Wait();
        //}
    }
}