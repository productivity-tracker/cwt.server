﻿namespace AgentIdentity.Domain.Shared.UserAgent
{
    public enum UserAgentStatus
    {
        Discovered,
        Mapped
    }
}