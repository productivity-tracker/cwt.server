﻿namespace AgentIdentity.Domain.Shared.Device
{
    public enum DeviceType
    {
        Unknown = 0,
        PC,
        SmartPhone
    }
}