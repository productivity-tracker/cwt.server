﻿namespace AgentIdentity.Domain.Shared.Device
{
    public enum DeviceState
    {
        Unknown,
        Locked,
        Unlocked
    }
}