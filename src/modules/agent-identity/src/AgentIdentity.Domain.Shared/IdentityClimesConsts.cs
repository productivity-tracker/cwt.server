﻿namespace AgentIdentity.Domain.Shared
{
    public static class IdentityClimesConsts
    {
        public const string CONNECTION_ID = nameof(CONNECTION_ID);
        public const string DEVICE_ID = nameof(DEVICE_ID);
        public const string EMPLOYEE_ID = nameof(EMPLOYEE_ID);
        public const string USER_AGENT_ID = nameof(USER_AGENT_ID);
    }
}