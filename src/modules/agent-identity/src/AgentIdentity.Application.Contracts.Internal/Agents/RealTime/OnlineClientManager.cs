﻿using System;
using System.Collections.Generic;
using Volo.Abp.DependencyInjection;

namespace AgentIdentity.Application.Contracts.Internal.Agents.RealTime
{
    /// <summary>
    /// Implements <see cref="IOnlineClientManager" />.
    /// </summary>
    public class OnlineClientManager : IOnlineClientManager, ISingletonDependency
    {
        /// <summary>
        /// Online clients Store.
        /// </summary>
        protected IOnlineClientStore Store { get; }

        public event EventHandler<OnlineClientEventArgs> ClientConnected;
        public event EventHandler<OnlineClientEventArgs> ClientDisconnected;

        /// <summary>
        /// Initializes a new instance of the <see cref="OnlineClientManager" /> class.
        /// </summary>
        public OnlineClientManager(IOnlineClientStore store)
        {
            Store = store;
        }

        public virtual void Add(IOnlineClient client)
        {
            Store.Add(client);

            ClientConnected?.Invoke(this, new OnlineClientEventArgs(client));
        }

        public virtual bool Remove(string connectionId)
        {
            var result = Store.TryRemove(connectionId, out var client);
            if (result)
            {
                ClientDisconnected?.Invoke(this, new OnlineClientEventArgs(client));
            }

            return result;
        }

        public virtual IOnlineClient GetByConnectionIdOrNull(string connectionId)
        {
            if (Store.TryGet(connectionId, out var client))
            {
                return client;
            }

            return null;
        }

        public virtual IReadOnlyList<IOnlineClient> GetAllClients() => Store.GetAll();
    }
}