﻿using System;
using System.Collections.Generic;

namespace AgentIdentity.Application.Contracts.Internal.Agents.RealTime
{
    public class OnlineClient : IOnlineClient
    {
        private Dictionary<string, object> _properties;

        /// <summary>
        /// Unique connection Id for this client.
        /// </summary>
        public string ConnectionId { get; set; }

        /// <summary>
        /// IP address of this client.
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// Connection establishment time for this client.
        /// </summary>
        public DateTime ConnectedAt { get; set; }

        /// <summary>
        /// Shortcut to set/get <see cref="Properties" />.
        /// </summary>
        public object this[string key]
        {
            get => Properties[key];
            set => Properties[key] = value;
        }

        /// <summary>
        /// Can be used to add custom properties for this client.
        /// </summary>
        public Dictionary<string, object> Properties
        {
            get => _properties;
            set => _properties = value ?? throw new ArgumentNullException(nameof(value));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OnlineClient" /> class.
        /// </summary>
        /// <param name="connectionId">The connection identifier.</param>
        /// <param name="ipAddress">The ip address.</param>
        /// <param name="connectedAt">The time when the client was connected</param>
        public OnlineClient(string connectionId, string ipAddress, DateTime connectedAt)
        {
            ConnectionId = connectionId;
            IpAddress = ipAddress;
            ConnectedAt = connectedAt;

            Properties = new Dictionary<string, object>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OnlineClient" /> class.
        /// </summary>
        /// <param name="connectionId">The connection identifier.</param>
        /// <param name="ipAddress">The ip address.</param>
        public OnlineClient(string connectionId, string ipAddress)
            : this(connectionId, ipAddress, DateTime.UtcNow)
        {
        }
    }
}