﻿using System.Threading.Tasks;
using AgentIdentity.Application.Contracts.Internal.Agents.RealTime;
using Volo.Abp;
using Volo.Abp.Application.Services;

namespace AgentIdentity.Application.Contracts.Internal.Agents
{
    [RemoteService(IsEnabled = false)]
    public interface IAgentService : IApplicationService, IOnlineClientManager
    {
        void SetDeviceId(string connectionId, string deviceId);
        Task UpdateSettings(string employeeId);
    }
}