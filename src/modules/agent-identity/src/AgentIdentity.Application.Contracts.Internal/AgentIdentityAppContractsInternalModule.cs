﻿using Volo.Abp.Modularity;

namespace AgentIdentity.Application.Contracts.Internal
{
    [DependsOn(typeof(AgentIdentityAppContractsModule))]
    public class AgentIdentityAppContractsInternalModule : AbpModule
    { }
}