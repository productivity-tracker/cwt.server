﻿using System;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Linq;

namespace AgentIdentity.Application
{
    public abstract class AgentIdentityApplicationService<TEntity, TKey, TGetListOutputDto, TGetListInput, TRepository> : ApplicationService
        where TEntity : class, IEntity<TKey>
        where TGetListOutputDto : IEntityDto<TKey>
        where TRepository : IRepository<TEntity, TKey>
    {
        public IAsyncQueryableExecuter AsyncQueryableExecutor { get; set; }
        protected TRepository Repository { get; }

        protected AgentIdentityApplicationService(TRepository repository)
        {
            Repository = repository;
            AsyncQueryableExecutor = DefaultAsyncQueryableExecuter.Instance;
        }

        /// <summary>
        /// Maps <see cref="TEntity" /> to <see cref="TGetOutputDto" />.
        /// It uses <see cref="IObjectMapper" /> by default.
        /// It can be overriden for custom mapping.
        /// </summary>
        protected abstract TGetListOutputDto MapToGetListOutputDto(TEntity entity);

        public virtual async Task<PagedResultDto<TGetListOutputDto>> GetListAsync(TGetListInput input)
        {
            var query = CreateFilteredQuery(input);

            var totalCount = await AsyncQueryableExecutor.CountAsync(query);

            query = ApplySorting(query, input);
            query = ApplyPaging(query, input);

            var entities = await AsyncQueryableExecutor.ToListAsync(query);

            return new PagedResultDto<TGetListOutputDto>(totalCount, entities.Select(MapToGetListOutputDto).ToList());
        }

        /// <summary>
        /// Should apply sorting if needed.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="input">The input.</param>
        protected virtual IQueryable<TEntity> ApplySorting(IQueryable<TEntity> query, TGetListInput input)
        {
            //Try to sort query if available
            if (input is ISortedResultRequest sortInput)
            {
                if (!sortInput.Sorting.IsNullOrWhiteSpace())
                {
                    return query.OrderBy(sortInput.Sorting);
                }
            }

            //IQueryable.Task requires sorting, so we should sort if Take will be used.
            if (input is ILimitedResultRequest)
            {
                return query.OrderByDescending(e => e.Id);
            }

            //No sorting
            return query;
        }

        /// <summary>
        /// Should apply paging if needed.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="input">The input.</param>
        protected virtual IQueryable<TEntity> ApplyPaging(IQueryable<TEntity> query, TGetListInput input)
        {
            //Try to use paging if available
            if (input is IPagedResultRequest pagedInput)
            {
                return query.PageBy(pagedInput);
            }

            //Try to limit query result if available
            if (input is ILimitedResultRequest limitedInput)
            {
                return query.Take(limitedInput.MaxResultCount);
            }

            //No paging
            return query;
        }

        /// <summary>
        /// This method should create <see cref="IQueryable{TEntity}" /> based on given input.
        /// It should filter query if needed, but should not do sorting or paging.
        /// Sorting should be done in <see cref="ApplySorting" /> and paging should be done in <see cref="ApplyPaging" />
        /// methods.
        /// </summary>
        /// <param name="input">The input.</param>
        protected virtual IQueryable<TEntity> CreateFilteredQuery(TGetListInput input) => Repository;
    }
}