﻿using System;
using System.Threading.Tasks;
using AgentIdentity.Application.Contracts.Internal.Agents;
using AgentIdentity.Application.Contracts.Internal.Agents.RealTime;
using AgentIdentity.Domain.Shared;
using Volo.Abp;

namespace AgentIdentity.Application.Agents
{
    [RemoteService(IsEnabled = false)]
    public class AgentService : OnlineClientManager, IAgentService
    {
        public AgentService(IOnlineClientStore store)
            : base(store)
        { }

        public void SetDeviceId(string connectionId, string deviceId)
        {
            var client = GetByConnectionIdOrNull(connectionId);
            if (client != null)
            {
                client[IdentityClimesConsts.DEVICE_ID] = deviceId;
            }
        }

        public Task UpdateSettings(string employeeId) => throw new NotImplementedException();
    }
}