﻿using AgentIdentity.Application.Contracts.Devices.DTO;
using AgentIdentity.Application.Contracts.Internal;
using AgentIdentity.Application.Contracts.UserAgents.DTO;
using AgentIdentity.Domain;
using AgentIdentity.Domain.AggregatesModel.DeviceAggregate;
using AgentIdentity.Domain.AggregatesModel.UserAgentAggregate;
using Volo.Abp.AutoMapper;
using Volo.Abp.EventBus.Distributed;
using Volo.Abp.Modularity;

namespace AgentIdentity.Application
{
    [DependsOn(
        typeof(AbpAutoMapperModule),
        typeof(AgentIdentityDomainModule), 
        typeof(AgentIdentityAppContractsInternalModule))]
    public class AgentIdentityApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpAutoMapperOptions>(options =>
            {
                //Add all mappings defined in the assembly of the MyModule class
                options.AddMaps<AgentIdentityApplicationModule>(true);
                //options.AddProfile<DeviceApplicationAutoMapperProfile>(true);
            });

            Configure<AbpDistributedEventBusOptions>(options =>
            {
                options.EtoMappings.Add<Device, DeviceDto>();
                options.EtoMappings.Add<UserAgent, UserAgentDto>();
            });
        }
    }
}