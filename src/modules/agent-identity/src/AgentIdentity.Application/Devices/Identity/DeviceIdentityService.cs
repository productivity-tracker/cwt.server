﻿using System.Threading.Tasks;
using AgentIdentity.Application.Contracts.Devices.Identity;
using AgentIdentity.Application.Contracts.Devices.Identity.DTO;
using AgentIdentity.Domain.AggregatesModel.DeviceAggregate;
using AgentIdentity.Domain.Services.Devices;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp.Application.Services;

namespace AgentIdentity.Application.Devices.Identity
{
    [Authorize]
    public class DeviceIdentityService : ApplicationService, IDeviceIdentityService
    {
        private readonly DeviceManager _deviceManager;

        public DeviceIdentityService(DeviceManager deviceManager)
        {
            _deviceManager = deviceManager;
        }

        [AllowAnonymous]
        public async Task<string> RegisterAsync(RegistrationRequest request)
        {
            var systemInfo = new SystemInfo(request.Domain, request.DeviceName, request.IpAddress, request.OperatingSystem);
            var deviceIdentifier = new DeviceIdentifier(request.UniversallyUniqueId);
            var device = await _deviceManager.RegisterAsync(deviceIdentifier, request.Type, request.AgentVersion, systemInfo);
            return device.Id;
        }

        public async Task PutStateAsync(ChangeStateRequest changeState)
            => await Task.Run(() => _deviceManager.SetDeviceState(new DeviceIdentifier(changeState.UniversallyUniqueDeviceId),
                                                                  changeState.NewState));
    }
}