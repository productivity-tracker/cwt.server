﻿using AgentIdentity.Application.Contracts.Devices.DTO;
using AgentIdentity.Domain.AggregatesModel.DeviceAggregate;
using AutoMapper;

namespace AgentIdentity.Application.Devices
{
    public class DeviceApplicationAutoMapperProfile : Profile
    {
        public DeviceApplicationAutoMapperProfile()
        {
            CreateMap<SystemInfo, SystemInfoDto>(MemberList.Destination);
        }
    }
}