﻿using System.Threading.Tasks;
using AgentIdentity.Application.Contracts.Devices;
using AgentIdentity.Application.Contracts.Devices.DTO;
using AgentIdentity.Domain.AggregatesModel.DeviceAggregate;
using AgentIdentity.Domain.Services.Devices;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp.Application.Dtos;

namespace AgentIdentity.Application.Devices
{
    [Authorize]
    public class DeviceService : AgentIdentityApplicationService<Device, string, DeviceDto, PagedAndSortedResultRequestDto, IDeviceRepository>, IDeviceService
    {
        private readonly DeviceManager _deviceManager;

        public DeviceService(IDeviceRepository repository,
                             DeviceManager deviceManager) 
            : base(repository)
        {
            _deviceManager = deviceManager;
        }

        protected override DeviceDto MapToGetListOutputDto(Device entity) => new DeviceDto
        {
            Id = entity.Id,
            Type = entity.Type,
            IsOnline = _deviceManager.IsOnline(entity.Id),
            State = _deviceManager.GetState(entity.Id),
            System = ObjectMapper.Map<SystemInfo, SystemInfoDto>(entity.System),
            CurrentAgentVersion = entity.CurrentAgentVersion,
            LastLoggedOnUserAgentId = entity.LastLoggedOnUserAgentId,
            LastConnectionDate = entity.LastConnectionDate
        };

        public Task DeleteAsync(string deviceId) => Repository.DeleteAsync(deviceId);
    }
}