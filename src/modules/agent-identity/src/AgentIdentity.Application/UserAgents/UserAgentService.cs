﻿using System.Threading.Tasks;
using AgentIdentity.Application.Contracts.UserAgents;
using AgentIdentity.Application.Contracts.UserAgents.DTO;
using AgentIdentity.Domain.AggregatesModel.UserAgentAggregate;
using AgentIdentity.Domain.Services.UserAgents;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp.Application.Dtos;

namespace AgentIdentity.Application.UserAgents
{
    [Authorize]
    public class UserAgentService : AgentIdentityApplicationService<UserAgent, string, UserAgentDto, PagedAndSortedResultRequestDto, IUserAgentRepository>, IUserAgentService
    {
        private readonly UserAgentManager _userAgentManager;

        public UserAgentService(IUserAgentRepository repository, UserAgentManager manager)
            : base(repository)
        {
            _userAgentManager = manager;
        }

        protected override UserAgentDto MapToGetListOutputDto(UserAgent entity) => new UserAgentDto
        {
            Id = entity.Id,
            EmployeeId = entity.EmployeeId,
            IsOnline = _userAgentManager.IsOnline(entity.Id),
            LastLoggedOnDate = entity.LastLoggedOnDate,
            User = ObjectMapper.Map<User, UserDto>(entity.User)
        };

        public Task DeleteAsync(string id) => Repository.DeleteAsync(id);

        public async Task<UserAgentDto> GetAsync(string id) 
            => ObjectMapper.Map<UserAgent, UserAgentDto>(await Repository.GetAsync(id));

        public async Task AssignEmployeeAsync(AssignEmployeeRequest assignRequest)
        {
            var userAgent = await Repository.GetAsync(assignRequest.UserAgentId);
            await Repository.UpdateAsync(userAgent.AssignEmployee(assignRequest.EmployeeId));
        }
    }
}