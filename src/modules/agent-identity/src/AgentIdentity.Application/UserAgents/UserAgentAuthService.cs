﻿using System.Threading.Tasks;
using AgentIdentity.Application.Contracts.UserAgents.Auth;
using AgentIdentity.Application.Contracts.UserAgents.Auth.DTO;
using AgentIdentity.Domain.AggregatesModel.UserAgentAggregate;
using AgentIdentity.Domain.Services.Devices;
using AgentIdentity.Domain.Services.UserAgents;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp.Application.Services;

namespace AgentIdentity.Application.UserAgents
{
    //[Authorize]
    [AllowAnonymous]
    public class UserAgentAuthService : ApplicationService, IUserAgentAuthService
    {
        private readonly UserAgentManager _userAgentManager;

        public UserAgentAuthService(UserAgentManager userAgentManager)
        {
            _userAgentManager = userAgentManager;
        }

        public async Task<string> LogOnAsync(LogonRequest request)
        {
            var user = new User(request.DisplayName, request.UserName, request.Domain, request.IsLocalUser);
            var deviceIdentifier = new DeviceIdentifier(request.DeviceUUID);
            var userAgent = await _userAgentManager.LogOnOrRegisterAsync(user, deviceIdentifier);
            return userAgent.Id;
        }

        public async Task LogOffAsync(UserIdentity identity)
        {
            var user = new User(string.Empty, identity.UserName, identity.Domain, identity.IsLocalUser);
            var deviceIdentifier = new DeviceIdentifier(identity.DeviceUUID);
            await Task.Run(() => _userAgentManager.LogOff(user, deviceIdentifier));
        }
    }
}