﻿using AgentIdentity.Application.Contracts.UserAgents.DTO;
using AgentIdentity.Domain.AggregatesModel.UserAgentAggregate;
using AutoMapper;

namespace AgentIdentity.Application.UserAgents
{
    public class UserAgentApplicationAutoMapperProfile : Profile
    {
        public UserAgentApplicationAutoMapperProfile()
        {
            CreateMap<User, UserDto>(MemberList.Destination);
        }
    }
}