﻿using AgentIdentity.Domain.AggregatesModel.DeviceAggregate;
using AgentIdentity.MongoDB.MongoDB;
using Volo.Abp.Domain.Repositories.MongoDB;
using Volo.Abp.MongoDB;

namespace AgentIdentity.MongoDB.Devices
{
    public class DeviceRepository : MongoDbRepository<IAgentIdentityMongoDbContext, Device, string>, IDeviceRepository
    {
        public DeviceRepository(IMongoDbContextProvider<IAgentIdentityMongoDbContext> dbContextProvider)
            : base(dbContextProvider)
        { }
    }
}