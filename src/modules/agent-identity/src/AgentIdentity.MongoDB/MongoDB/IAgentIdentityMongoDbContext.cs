﻿using AgentIdentity.Domain;
using AgentIdentity.Domain.AggregatesModel.DeviceAggregate;
using AgentIdentity.Domain.AggregatesModel.UserAgentAggregate;
using MongoDB.Driver;
using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace AgentIdentity.MongoDB.MongoDB
{
    [ConnectionStringName(AgentIdentityDbProperties.ConnectionStringName)]
    public interface IAgentIdentityMongoDbContext : IAbpMongoDbContext
    {
        IMongoCollection<Device> Devices { get; }
        IMongoCollection<UserAgent> UserAgents { get; }
    }
}