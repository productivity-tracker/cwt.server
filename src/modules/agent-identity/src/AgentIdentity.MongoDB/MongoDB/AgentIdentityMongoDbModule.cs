﻿using AgentIdentity.Domain;
using AgentIdentity.Domain.AggregatesModel.DeviceAggregate;
using AgentIdentity.Domain.AggregatesModel.UserAgentAggregate;
using AgentIdentity.MongoDB.Devices;
using AgentIdentity.MongoDB.UserAgents;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;
using Volo.Abp.MongoDB;

namespace AgentIdentity.MongoDB.MongoDB
{
    [DependsOn(
        typeof(AbpMongoDbModule), 
        typeof(AgentIdentityDomainModule))]
    public class AgentIdentityMongoDbModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
            => context.Services.AddMongoDbContext<AgentIdentityMongoDbContext>(options =>
            {
                options.AddRepository<Device, DeviceRepository>();
                options.AddRepository<UserAgent, UserAgentRepository>();
            });
    }
}