﻿using AgentIdentity.Domain.AggregatesModel.DeviceAggregate;
using AgentIdentity.Domain.AggregatesModel.UserAgentAggregate;
using MongoDB.Driver;
using Volo.Abp.MongoDB;

namespace AgentIdentity.MongoDB.MongoDB
{
    public class AgentIdentityMongoDbContext : AbpMongoDbContext, IAgentIdentityMongoDbContext
    {
        [MongoCollection(nameof(Devices))]
        public IMongoCollection<Device> Devices => Collection<Device>();

        [MongoCollection(nameof(UserAgents))]
        public IMongoCollection<UserAgent> UserAgents => Collection<UserAgent>();
    }
}