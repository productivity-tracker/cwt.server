﻿using AgentIdentity.Domain.AggregatesModel.UserAgentAggregate;
using AgentIdentity.MongoDB.MongoDB;
using Volo.Abp.Domain.Repositories.MongoDB;
using Volo.Abp.MongoDB;

namespace AgentIdentity.MongoDB.UserAgents
{
    public class UserAgentRepository : MongoDbRepository<IAgentIdentityMongoDbContext, UserAgent, string>, IUserAgentRepository
    {
        public UserAgentRepository(IMongoDbContextProvider<IAgentIdentityMongoDbContext> dbContextProvider)
            : base(dbContextProvider)
        { }
    }
}