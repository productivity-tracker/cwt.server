﻿using System;
using AgentIdentity.Domain.Shared.Device;
using Volo.Abp.Application.Dtos;

namespace AgentIdentity.Application.Contracts.Devices.DTO
{
    [Serializable]
    public class DeviceDto : EntityDto<string>
    {
        public DeviceType Type { get; set; }
        public SystemInfoDto System { get; set; }
        public DeviceState State { get; set; }
        public bool IsOnline { get; set; }
        public string CurrentAgentVersion { get; set; }
        public DateTime LastConnectionDate { get; set; }
        public string LastLoggedOnUserAgentId { get; set; }
    }
}