﻿using System;

namespace AgentIdentity.Application.Contracts.Devices.DTO
{
    [Serializable]
    public class SystemInfoDto
    {
        public string Domain { get; set; }
        public string DeviceName { get; set; }
        public string IpAddress { get; set; }
        public string OperatingSystem { get; set; }
    }
}