﻿using System.Threading.Tasks;
using AgentIdentity.Application.Contracts.Devices.Identity.DTO;
using Volo.Abp.Application.Services;

namespace AgentIdentity.Application.Contracts.Devices.Identity
{
    public interface IDeviceIdentityService : IApplicationService
    {
        Task PutStateAsync(ChangeStateRequest changeState);
        Task<string> RegisterAsync(RegistrationRequest request);
    }
}