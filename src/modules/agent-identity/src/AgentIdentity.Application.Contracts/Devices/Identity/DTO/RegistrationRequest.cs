﻿using System;
using AgentIdentity.Domain.Shared.Device;

namespace AgentIdentity.Application.Contracts.Devices.Identity.DTO
{
    [Serializable]
    public class RegistrationRequest
    {
        public string AgentConnectionId { get; set; }
        public string UniversallyUniqueId { get; set; }
        public string DeviceName { get; set; }
        public string Domain { get; set; }
        public string OperatingSystem { get; set; }
        public string AgentVersion { get; set; }
        public DeviceType Type { get; set; }
        internal string IpAddress { get; set; }

        private RegistrationRequest()
        { }

        public RegistrationRequest(string agentConnectionId,
                                   string universallyUniqueId,
                                   string deviceName,
                                   string domain,
                                   string operatingSystem,
                                   string agentVersion,
                                   string ipAddress,
                                   DeviceType type)
        {
            AgentConnectionId = agentConnectionId;
            UniversallyUniqueId = universallyUniqueId;
            DeviceName = deviceName;
            Domain = domain;
            OperatingSystem = operatingSystem;
            AgentVersion = agentVersion;
            IpAddress = ipAddress;
            Type = type;
        }
    }
}