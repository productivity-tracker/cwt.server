﻿using AgentIdentity.Domain.Shared.Device;

namespace AgentIdentity.Application.Contracts.Devices.Identity.DTO
{
    public class ChangeStateRequest
    {
        public string UniversallyUniqueDeviceId { get; set; }
        public DeviceState NewState { get; set; }
    }
}