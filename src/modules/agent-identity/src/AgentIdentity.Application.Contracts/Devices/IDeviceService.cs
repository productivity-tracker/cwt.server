﻿using System.Threading.Tasks;
using AgentIdentity.Application.Contracts.Devices.DTO;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace AgentIdentity.Application.Contracts.Devices
{
    public interface IDeviceService : IApplicationService
    {
        Task DeleteAsync(string deviceId);
        Task<PagedResultDto<DeviceDto>> GetListAsync(PagedAndSortedResultRequestDto request);
    }
}