﻿using System.Threading.Tasks;
using AgentIdentity.Application.Contracts.UserAgents.DTO;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace AgentIdentity.Application.Contracts.UserAgents
{
    public interface IUserAgentService : IApplicationService
    {
        Task<UserAgentDto> GetAsync(string id);
        Task AssignEmployeeAsync(AssignEmployeeRequest assignRequest);
        Task DeleteAsync(string id);
        Task<PagedResultDto<UserAgentDto>> GetListAsync(PagedAndSortedResultRequestDto request);
    }
}