﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AgentIdentity.Application.Contracts.UserAgents.DTO
{
    [Serializable]
    public class AssignEmployeeRequest
    {
        [Required]
        public string UserAgentId { get; set; }

        [Required]
        public Guid EmployeeId { get; set; }
    }
}