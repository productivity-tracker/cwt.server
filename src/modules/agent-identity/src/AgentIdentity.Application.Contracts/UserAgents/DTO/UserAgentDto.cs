﻿using System;
using Volo.Abp.Application.Dtos;

namespace AgentIdentity.Application.Contracts.UserAgents.DTO
{
    [Serializable]
    public class UserAgentDto : EntityDto<string>
    {
        public UserDto User { get; set; }
        public string UniqueAgentName { get; set; }
        public Guid EmployeeId { get; set; }
        public bool IsOnline { get; set; }
        public DateTime LastLoggedOnDate { get; set; }
    }
}