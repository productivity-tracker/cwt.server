﻿using System;

namespace AgentIdentity.Application.Contracts.UserAgents.DTO
{
    [Serializable]
    public class UserDto
    {
        public string DisplayName { get; set; }
        public string UserName { get; set; }
        public string Domain { get; set; }
        public bool IsLocalUser { get; set; }
    }
}