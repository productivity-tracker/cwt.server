﻿using System;

namespace AgentIdentity.Application.Contracts.UserAgents.Auth.DTO
{
    [Serializable]
    public class LogonRequest : UserIdentity
    {
        public string DisplayName { get; set; }

        private LogonRequest()
        { }

        public LogonRequest(string userName,
                            string domain,
                            bool isLocalUser,
                            string deviceUuid,
                            string displayName)
            : base(userName, domain, isLocalUser, deviceUuid)
        {
            DisplayName = displayName;
        }
    }
}