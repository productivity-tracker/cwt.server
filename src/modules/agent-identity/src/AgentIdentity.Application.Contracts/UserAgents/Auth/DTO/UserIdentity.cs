﻿using System;

namespace AgentIdentity.Application.Contracts.UserAgents.Auth.DTO
{
    [Serializable]
    public class UserIdentity
    {
        public string UserName { get; set; }
        public string Domain { get; set; }
        public bool IsLocalUser { get; set; }
        public string DeviceUUID { get; set; }

        protected UserIdentity()
        { }

        public UserIdentity(string userName,
                            string domain,
                            bool isLocalUser,
                            string deviceUuid)
        {
            UserName = userName;
            Domain = domain;
            IsLocalUser = isLocalUser;
            DeviceUUID = deviceUuid;
        }
    }
}