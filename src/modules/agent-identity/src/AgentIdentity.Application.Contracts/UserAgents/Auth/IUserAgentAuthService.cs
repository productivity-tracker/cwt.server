﻿using System.Threading.Tasks;
using AgentIdentity.Application.Contracts.UserAgents.Auth.DTO;
using Volo.Abp.Application.Services;

namespace AgentIdentity.Application.Contracts.UserAgents.Auth
{
    public interface IUserAgentAuthService : IApplicationService
    {
        Task LogOffAsync(UserIdentity identity);
        Task<string> LogOnAsync(LogonRequest request);
    }
}