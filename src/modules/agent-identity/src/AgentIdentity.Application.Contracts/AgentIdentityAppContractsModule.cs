﻿using AgentIdentity.Domain.Shared;
using Volo.Abp.Modularity;

namespace AgentIdentity.Application.Contracts
{
    [DependsOn(typeof(AgentIdentityDomainSharedModule))]
    public class AgentIdentityAppContractsModule : AbpModule
    { }
}