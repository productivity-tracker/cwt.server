﻿using System;
using AgentIdentity.Application;
using AgentIdentity.Application.Devices;
using AgentIdentity.Application.Devices.Identity;
using AgentIdentity.Application.UserAgents;
using AgentIdentity.HttpApi;
using AgentIdentity.HttpApi.Hubs;
using AgentIdentity.MongoDB.MongoDB;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.AspNetCore.Mvc.Conventions;
using Volo.Abp.Autofac;
using Volo.Abp.Modularity;

namespace AgentIdentity.Host
{
    [DependsOn(typeof(AbpAutofacModule),
        typeof(AgentIdentityHttpApiModule),
        typeof(AgentIdentityMongoDbModule),
        typeof(AgentIdentityApplicationModule))]
    public class AgentIdentityModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            // Enable cookie authentication
            context.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie();
            context.Services.AddHttpContextAccessor();

            Configure<AbpAspNetCoreMvcOptions>(options =>
            {
                options.ConventionalControllers.Create(typeof(AgentIdentityApplicationModule).Assembly,
                                                       opts => opts.TypePredicate = t
                                                           => t.FullName == typeof(UserAgentService).FullName ||
                                                              t.FullName == typeof(DeviceService).FullName);
                options.ConventionalControllers.Create(typeof(AgentIdentityApplicationModule).Assembly,
                                                       ConfigureUserAgentAuthConventionalControllers);
                options.ConventionalControllers.Create(typeof(AgentIdentityApplicationModule).Assembly,
                                                       ConfigureDeviceIdentityConventionalControllers);
            });

            context.Services.AddSignalR();

            context.Services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo { Title = "Agent Identity Service API", Version = "v1" });
                options.DocInclusionPredicate((docName, description) => true);
                options.CustomSchemaIds(type => type.FullName);
            });
        }

        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
            var app = context.GetApplicationBuilder();

            // Add authentication to request pipeline
            app.UseCookiePolicy();
            app.UseAuthentication();

            app.UseCorrelationId();
            app.UseRouting();
            app.UseEndpoints(builder => builder.MapHub<AgentsHub>("/agents/connection"));

            app.UseSwagger();
            app.UseSwaggerUI(options => { options.SwaggerEndpoint("/swagger/v1/swagger.json", "Agent Identity Service API"); });

            app.UseMvcWithDefaultRouteAndArea();
        }

        private void ConfigureUserAgentAuthConventionalControllers(ConventionalControllerSetting opts)
        {
            opts.TypePredicate = t => t.FullName == typeof(UserAgentAuthService).FullName;
            opts.RootPath = "agents";
            opts.UrlControllerNameNormalizer = urlActionNameNormalizerContext
                => string.Equals(urlActionNameNormalizerContext.ControllerName, "userAgentAuth", StringComparison.OrdinalIgnoreCase)
                    ? "userAgent"
                    : urlActionNameNormalizerContext.ControllerName;
        }

        private void ConfigureDeviceIdentityConventionalControllers(ConventionalControllerSetting opts)
        {
            opts.TypePredicate = t => t.FullName == typeof(DeviceIdentityService).FullName;
            opts.RootPath = "agents";
            opts.UrlControllerNameNormalizer = urlActionNameNormalizerContext
                => string.Equals(urlActionNameNormalizerContext.ControllerName, "deviceIdentity", StringComparison.OrdinalIgnoreCase)
                    ? "device"
                    : urlActionNameNormalizerContext.ControllerName;
        }
    }
}