﻿namespace ScreenRecording.Domain.Shared
{
    public enum ImageFormat
    {
        Jpg,
        Png,
        Bmp
    }
}