﻿using System;

namespace ScreenRecording.Domain.Shared
{
    public static class ContentTypeMap
    {
        public static string Convert(ImageFormat format) => format switch
        {
            ImageFormat.Jpg => "image/jpeg",
            ImageFormat.Png => "image/png",
            ImageFormat.Bmp => "image/bmp",
            _ => throw new ArgumentOutOfRangeException(format.ToString("G"))
        };
    }
}