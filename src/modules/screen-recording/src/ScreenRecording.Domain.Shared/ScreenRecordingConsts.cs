﻿namespace ScreenRecording.Domain.Shared
{
    public class ScreenRecordingConsts
    {
        public const string DATE_FORMAT = "yyyy-MM-dd";
        public const string TIME_FORMAT = "HH-mm-ss";
    }
}