﻿using System;
using System.Globalization;

namespace ScreenRecording.Domain.Shared
{
    public class DateTimeToStringConverter
    {
        private readonly string _dateTimeFormat = $"{ScreenRecordingConsts.DATE_FORMAT}_{ScreenRecordingConsts.TIME_FORMAT}";

        public string Convert(DateTime dateTimeUtc) 
            => dateTimeUtc.ToString(_dateTimeFormat);

        public DateTime Convert(string dateTime)
        {
            if (!DateTime.TryParseExact(dateTime, 
                                        _dateTimeFormat, 
                                        provider: null, 
                                        DateTimeStyles.None, 
                                        out var parsedDate))
            {
                throw new ArgumentException($"Invalid date, must be like '{_dateTimeFormat}'");
            }

            return parsedDate;
        }
    }
}