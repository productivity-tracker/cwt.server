﻿using ScreenRecording.Domain.Shared;
using Volo.Abp.Domain;
using Volo.Abp.Modularity;

namespace ScreenRecording.Domain
{
    [DependsOn(
        typeof(ScreenRecordingDomainSharedModule),
        typeof(AbpDddDomainModule))]
    public class ScreenRecordingDomainModule : AbpModule
    {
        
    }
}