﻿using System;
using System.Collections.Concurrent;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using CWT.Timing;
using Volo.Abp;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Services;

namespace ScreenRecording.Domain.Services.ScreenshotRecording.RecordSetTokens
{
    public class ScreenRecordAliasManager : DomainService, ISingletonDependency
    {
        private readonly ConcurrentDictionary<Guid, RecordSetRequest> _tokens;

        private readonly ISubject<TouchInfo> _tokenTouch = new Subject<TouchInfo>();
        public IObservable<TouchInfo> ScreenRecordSeen => _tokenTouch;

        public Guid GetToken(RecordSetRequest request)
        {
            Check.NotNull(request, nameof(request));

            var token = GuidGenerator.Create();
            if (!_tokens.TryAdd(token, request))
            {
                throw new NotSupportedException();
            }

            return token;
        }

        public (string employeeId, Date date) Resolve(Guid token, string ipAddress)
        {
            if (!_tokens.TryGetValue(token, out var value))
            {
                throw new ArgumentException();
            }

            if (value.IpAddress != ipAddress)
            {
                throw new ArgumentException("Mismatch IpAddress");
            }

            _tokenTouch.OnNext(new TouchInfo { IpAddress = value.IpAddress, UserId = value.UserId });

            return (employeeId: value.EmployeeId, date: value.Date);
        }
    }
}