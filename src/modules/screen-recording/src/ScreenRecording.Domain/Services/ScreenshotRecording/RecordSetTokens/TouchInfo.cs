﻿using System;

namespace ScreenRecording.Domain.Services.ScreenshotRecording.RecordSetTokens
{
    public class TouchInfo
    {
        public Guid Token { get; set; }
        public string UserId { get; set; }
        public string IpAddress { get; set; }
        public DateTime Timestamp { get; private set; }
    }
}