﻿using System;
using CWT.Timing;
using JetBrains.Annotations;
using Volo.Abp;

namespace ScreenRecording.Domain.Services.ScreenshotRecording.RecordSetTokens
{
    public class RecordSetRequest : IEquatable<RecordSetRequest>
    {        
        public string IpAddress { get; }

        public string UserId { get; }
        
        public string EmployeeId { get; }

        public Date Date { get; }

        public RecordSetRequest(Date date,
                                [NotNull] string employeeId,
                                [NotNull] string ipAddress,
                                [NotNull] string userId)
        {
            Date = date;
            EmployeeId = Check.NotNullOrWhiteSpace(employeeId, nameof(employeeId));
            IpAddress = Check.NotNullOrWhiteSpace(ipAddress, nameof(ipAddress));
            UserId = Check.NotNullOrWhiteSpace(userId, nameof(userId));
        }

        #region IEquatable Support

        public bool Equals(RecordSetRequest other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return IpAddress == other.IpAddress && UserId == other.UserId && EmployeeId == other.EmployeeId && Date.Equals(other.Date);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((RecordSetRequest)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = IpAddress.GetHashCode();
                hashCode = (hashCode * 397) ^ UserId.GetHashCode();
                hashCode = (hashCode * 397) ^ EmployeeId.GetHashCode();
                hashCode = (hashCode * 397) ^ Date.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(RecordSetRequest left, RecordSetRequest right) => Equals(left, right);

        public static bool operator !=(RecordSetRequest left, RecordSetRequest right) => !Equals(left, right);

        #endregion
    }
}