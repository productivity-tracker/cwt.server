﻿using System;

namespace ScreenRecording.Domain.Services.ScreenshotRecording
{
    public class ScreenshotEventArg : EventArgs
    {
        public string EmployeeId { get; }
        public string BasePath { get; }
        public DateTime Timestamp { get; }

        public ScreenshotEventArg(string employeeId, string basePath, DateTime timestamp)
        {
            EmployeeId = employeeId;
            BasePath = basePath;
            Timestamp = timestamp;
        }
    }
}