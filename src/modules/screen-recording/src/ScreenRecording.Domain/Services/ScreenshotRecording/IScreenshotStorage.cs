﻿using CWT.Domain.Abstractions.Repository;
using ScreenRecording.Domain.AggregatesModel.ScreenRecordAggregate;
using Volo.Abp.DependencyInjection;

namespace ScreenRecording.Domain.Services.ScreenshotRecording
{
    public interface IScreenshotStorage : IBlobRepository<ScreenshotItem>, ITransientDependency
    {
        
    }
}