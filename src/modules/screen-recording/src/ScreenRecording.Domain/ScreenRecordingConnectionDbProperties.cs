﻿namespace ScreenRecording.Domain
{
    public class ScreenRecordingConnectionDbProperties
    {
        public static string DbTablePrefix { get; set; } = string.Empty;

        public static string DbSchema { get; set; } = null;

        public const string ConnectionStringName = "ScreenRecordingConnection";
    }
}