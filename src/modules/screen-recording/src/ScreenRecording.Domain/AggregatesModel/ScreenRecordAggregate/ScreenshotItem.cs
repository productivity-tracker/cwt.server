﻿using System;

namespace ScreenRecording.Domain.AggregatesModel.ScreenRecordAggregate
{
    public class ScreenshotItem
    {
        public ScreenshotKey Key { get; private set; }
        
        public ulong Size { get; private set; }
        
        public DateTime? LastModified { get; private set; }

        public ScreenshotItem(ScreenshotKey key,
                              ulong size,
                              DateTime? lastModified)
        {
            Key = key;
            Size = size;
            LastModified = lastModified;
        }

        protected ScreenshotItem()
        {
            
        }
    }
}