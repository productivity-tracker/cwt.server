﻿using System;
using System.IO;
using ScreenRecording.Domain.Shared;

namespace ScreenRecording.Domain.AggregatesModel.ScreenRecordAggregate
{
    public class ScreenshotKey : ScreenRecordKey, IEquatable<ScreenshotKey>
    {
        public ImageFormat ImageFormat { get; private set; }

        protected ScreenshotKey() { }

        /// <summary>
        /// Create <see cref="ScreenshotKey" /> from object path or key.
        /// </summary>
        /// <param name="path">The path must be match the pattern <see cref="ScreenRecordKey.DateTimePattern" />.</param>
        /// <exception cref="ArgumentException"></exception>
        public ScreenshotKey(string path)
            : base(path)
        {
            ImageFormat = GetExtension(path);
        }

        public ScreenshotKey(string employeeId, DateTime createdAt, ImageFormat imageFormat)
            : base(employeeId, createdAt)
        {
            ImageFormat = imageFormat;
        }
        
        protected override string CreateFileName(DateTime createTime) => $"{base.CreateFileName(createTime)}.{ImageFormat:G}";

        private ImageFormat GetExtension(string path)
        {
            var extension = Path.GetExtension(path)?.TrimStart('.');
            if (extension.IsNullOrWhiteSpace())
            {
                throw new ArgumentNullException(nameof(path), "Invalid path extension.");
            }

            return extension.ToEnum<ImageFormat>(true);
        }

        #region IEquatable Support

        public bool Equals(ScreenshotKey other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) && ImageFormat == other.ImageFormat;
        }

        public override bool Equals(object obj) => ReferenceEquals(this, obj) || obj is ScreenshotKey other && Equals(other);

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode() * 397) ^ (int)ImageFormat;
            }
        }

        public static bool operator ==(ScreenshotKey left, ScreenshotKey right) => Equals(left, right);

        public static bool operator !=(ScreenshotKey left, ScreenshotKey right) => !Equals(left, right);

        #endregion
    }
}