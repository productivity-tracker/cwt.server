﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using CWT.Timing;
using ScreenRecording.Domain.Shared;

namespace ScreenRecording.Domain.AggregatesModel.ScreenRecordAggregate
{
    public class ScreenRecordKey : IEquatable<ScreenRecordKey>
    {
        internal static string DateTimePattern => $"{ScreenRecordingConsts.DATE_FORMAT}/{ScreenRecordingConsts.TIME_FORMAT}";

        public string EmployeeId { get; protected set; }
        public DateTime CreatedAt { get; protected set; }

        /// <summary>
        /// Create <see cref="ScreenshotKey" /> from object path or key.
        /// </summary>
        /// <param name="path">The path must be match the pattern <see cref="DateTimePattern" />.</param>
        /// <exception cref="ArgumentException"></exception>
        public ScreenRecordKey(string path)
        {
            EmployeeId = GetEmployeeId(path);
            CreatedAt = GetCreatedTime(path);
        }

        public ScreenRecordKey(string employeeId, DateTime createdAt)
        {
            EmployeeId = employeeId;
            CreatedAt = createdAt;
        }

        protected ScreenRecordKey()
        { }

        public override string ToString() => GetRelativePath(EmployeeId, CreatedAt, CreateFileName(CreatedAt)).ToLower();

        protected virtual string CreateFileName(DateTime createTime) => $"{createTime.ToString(ScreenRecordingConsts.TIME_FORMAT)}";

        public static string GetRelativePath(string employeeId, Date date, string fileName) => $"{GetPrefix(employeeId, date)}/{fileName}";

        public static string GetPrefix(string employeeId, Date createdAt) => $"{employeeId}/{createdAt.ToString(ScreenRecordingConsts.DATE_FORMAT)}";

        protected DateTime GetCreatedTime(string path)
        {
            const string pattern = @"\d{4}-[01]\d-[0-3]\d[- /.][0-2]\d-[0-5]\d-[0-5]\d(?:\.\d+)?Z?";
            var datetimeStr = new Regex(pattern).Match(path).Value;
            if (!DateTime.TryParseExact(datetimeStr,
                                        $"{ScreenRecordingConsts.DATE_FORMAT}/{ScreenRecordingConsts.TIME_FORMAT}",
                                        null,
                                        DateTimeStyles.None,
                                        out var createdDateTime))
            {
                throw new ArgumentException($"The path does not match the pattern '{DateTimePattern}'. ");
            }

            return createdDateTime;
        }

        protected string GetEmployeeId(string path) => path.Substring(startIndex: 0, path.IndexOf(@"/", StringComparison.Ordinal));

        #region IEquatable Support

        public bool Equals(ScreenRecordKey other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return EmployeeId == other.EmployeeId && CreatedAt.Equals(other.CreatedAt);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ScreenRecordKey)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((EmployeeId != null ? EmployeeId.GetHashCode() : 0) * 397) ^ CreatedAt.GetHashCode();
            }
        }

        public static bool operator ==(ScreenRecordKey left, ScreenRecordKey right) => Equals(left, right);

        public static bool operator !=(ScreenRecordKey left, ScreenRecordKey right) => !Equals(left, right);

        #endregion
    }
}