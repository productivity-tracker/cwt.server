﻿using Microsoft.Extensions.DependencyInjection;
using ScreenRecording.Application.Contracts;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Modularity;

namespace ScreenRecording.HttpApi
{
    [DependsOn(typeof(ScreenRecordingApplicationContractsModule), typeof(AbpAspNetCoreMvcModule))]
    public class ScreenRecordingHttpApiModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            PreConfigure<IMvcBuilder>(mvcBuilder =>
            {
                mvcBuilder.AddApplicationPartIfNotExists(typeof(ScreenRecordingHttpApiModule).Assembly);
            });
        }
    }
}