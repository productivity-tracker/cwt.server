﻿using System;
using CWT.Timing;
using ScreenRecording.Application.Contracts.ScreenshotRecording;
using ScreenRecording.Application.Contracts.ScreenshotRecording.DTO;
using ScreenRecording.Domain.Shared;

namespace ScreenRecording.HttpApi.Controllers
{
    public class ScreenshotUrlFactory : IScreenshotUrlFactory
    {
        private readonly Uri _baseUrl;
        public ScreenshotUrlFactory(string baseUrl)
        {
            _baseUrl = new Uri(baseUrl);
        }

        public Uri Create(ScreenshotKeyDto key)
        {
            var date = new Date(key.Timestamp);
            return new Uri(_baseUrl,
                           "api/screenRecording/screenshots/" +
                           $"{key.EmployeeId}/{date.ToString(ScreenRecordingConsts.DATE_FORMAT)}/{GetFileName(key)}".ToLower());
        }

        private string GetFileName(ScreenshotKeyDto key)
            => $"{key.Timestamp.ToString(ScreenRecordingConsts.TIME_FORMAT)}.{key.ImageFormat}";
    }
}