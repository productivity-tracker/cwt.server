﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using CWT.Timing;
using Microsoft.AspNetCore.Mvc;
using ScreenRecording.Application.Contracts.ScreenshotRecording;
using ScreenRecording.Application.Contracts.ScreenshotRecording.DTO;
using ScreenRecording.Domain.Shared;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Validation;
using UploadStream;

namespace ScreenRecording.HttpApi.Controllers
{
    [RemoteService]
    [Area("screenRecording")]
    [Route("api/screenRecording/screenshots")]
    public class ScreenshotsController : AbpController
    {
        private readonly IScreenshotProvider _screenshotProvider;
        private readonly IScreenshotUploadService _screenshotUploadService;

        public ScreenshotsController(IScreenshotProvider screenshotProvider, IScreenshotUploadService screenshotUploadService)
        {
            _screenshotProvider = screenshotProvider;
            _screenshotUploadService = screenshotUploadService;
        }

        //[HttpPost]
        //public async Task<IActionResult> UploadFile([FromForm] ScreenshotDto screenshot)
        //{
        //    var uploadedFile = screenshot.File;
        //    if (uploadedFile == null || uploadedFile.Length == 0)
        //    {
        //        return BadRequest("File not found");
        //    }

        //    using var stream = uploadedFile.OpenReadStream();
        //    var screenshotKey = new ScreenshotKeyDto
        //    {
        //        UserAgentId = screenshot.UserAgentId,
        //        Timestamp = screenshot.Timestamp,
        //        ImageFormat = screenshot.ImageFormat
        //    };

        //    await _screenshotUploadService.UploadAsync(screenshotKey, stream, screenshot.Tags);

        //    return Ok("Upload successful");
        //}

        [HttpPost("")]
        [DisableFormModelBinding]
        public async Task<IActionResult> UploadStream(ScreenshotDto screenshot)
        {
            await this.StreamFiles<ScreenshotDto>(async x =>
            {
                using var stream = x.OpenReadStream();
                await _screenshotUploadService.UploadAsync(
                    screenshot.UserAgentId, 
                    screenshot.Timestamp, 
                    screenshot.ImageFormat, 
                    stream, 
                    screenshot.Tags);
            });

            return Ok("Upload successful");
        }

        [DisableValidation]
        [HttpGet("{employeeId}/{date}/{fileName}")]
        public async Task<IActionResult> DownloadImage(string employeeId, string date, string fileName)
        {
            var stream = new MemoryStream();
            var contentType = await _screenshotProvider.DownloadAsync(employeeId, ParseDate(date), fileName, stream);

            stream.Position = 0;
            return File(stream, contentType);
        }

        [HttpGet("{employeeId}/{date}")]
        public async Task<IList<ScreenshotInfoDto>> GetScreenshots(string employeeId, string date)
            => await _screenshotProvider.GetListAsync(employeeId, ParseDate(date));

        private Date ParseDate(string date)
        {
            if (!DateTime.TryParseExact(date,
                                        ScreenRecordingConsts.DATE_FORMAT,
                                        provider: null,
                                        DateTimeStyles.None,
                                        out var parsedDate))
            {
                throw new ArgumentException($"Invalid date, must be like '{ScreenRecordingConsts.DATE_FORMAT}'");
            }

            return parsedDate;
        }
    }
}
