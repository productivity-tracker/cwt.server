﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using ScreenRecording.Application.Contracts;
using Volo.Abp;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Modularity;

namespace ScreenRecording.Grpc
{
    [DependsOn(
        typeof(ScreenRecordingApplicationContractsModule),
        typeof(AbpAspNetCoreMvcModule))]
    public class ScreenRecordingGrpcModule : AbpModule
    {
        //public override void ConfigureServices(ServiceConfigurationContext context) => context.Services.AddGrpc();

        //public override void OnApplicationInitialization(ApplicationInitializationContext context)
        //{
        //    var app = context.GetApplicationBuilder();

        //    app.UseRouting();
        //    app.UseEndpoints(endpoints => endpoints.MapGrpcService<Services.ScreenshotUploadService>());
        //}
    }
}