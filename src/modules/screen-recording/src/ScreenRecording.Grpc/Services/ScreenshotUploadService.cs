﻿using System;
using System.Threading.Tasks;
using CWT.ScreenRecording;
using Google.Protobuf;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using ScreenRecording.Application.Contracts.ScreenshotRecording;
using ScreenRecording.Application.Contracts.ScreenshotRecording.DTO;
using ScreenRecording.Domain.Shared;
using UploadStatus = CWT.ScreenRecording.UploadStatus;

namespace ScreenRecording.Grpc.Services
{
    public class ScreenshotUploadService : CWT.ScreenRecording.ScreenshotUploadService.ScreenshotUploadServiceBase
    {
        private readonly IScreenshotUploadService _screenshotUploadService;
        private readonly ILogger _logger;

        private readonly DateTimeToStringConverter _dateTimeConverter;

        public ScreenshotUploadService(ILogger<ScreenshotUploadService>  logger, IScreenshotUploadService uploadService)
        {
            _logger = logger;
            _screenshotUploadService = uploadService;
            _dateTimeConverter = new DateTimeToStringConverter();
        }

        public override async Task<UploadStatus> Upload(ScreenshotRequest request, ServerCallContext context)
        {
            try
            {
                var key = Convert(request.Key);
                var dto = new ScreenshotDto
                {
                    Key = key,
                    Data = request.Key.ToByteArray()
                };

                await _screenshotUploadService.UploadAsync(dto);
                return new UploadStatus { Code = UploudStatusCode.Ok };
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Error occurred while {nameof(Upload)} screenshot");
                return new UploadStatus { Code = UploudStatusCode.Failed };
            }
        }

        private ScreenshotKeyDto Convert(ScreenshotKey key)
        {
            return new ScreenshotKeyDto
            {
                EmployeeId = key.EmployeeId,
                ImageFormat = key.ImageFormat.ToEnum<ImageFormat>(),
                CreatedAt = _dateTimeConverter.Convert(key.CreatedAt)
            };
        }
    }
}