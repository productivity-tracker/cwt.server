﻿using System;
using ScreenRecording.Domain.Shared;

namespace ScreenRecording.Application.Contracts.RecordSettings.DTO
{
    [Serializable]
    public class ScreenshotSettingsDto
    {
        public ImageFormat ImageFormat { get; set; }
        public long Quality { get; set; }
    }
}