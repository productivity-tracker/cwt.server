﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ScreenRecordingAdministration.ScreenRecordSettings;

namespace ScreenRecording.Application.Contracts.RecordSettings.DTO
{
    [Serializable]
    public class ScreenRecordAgentSettingsDto
    {
        public bool EnableRecording { get; set; }
        public RecordMode RecordMode { get; set; }
        public RecordType RecordType { get; set; }
        public ScreenshotSettingsDto ScreenshotSettings { get; set; }
        public ISet<string> ObservableProcesses { get; set; }
        public ISet<string> ObservableWebSites { get; set; }
        public TimeSpan FrameFrequency { get; set; }
        public string StorageAdapterAddress { get; set; }
    } 
}