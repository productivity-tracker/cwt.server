﻿using System.IO;
using System.Threading.Tasks;
using ScreenRecording.Application.Contracts.ScreenshotRecording.DTO;
using ScreenRecording.Domain.Shared;
using Volo.Abp.Application.Services;

namespace ScreenRecording.Application.Contracts.ScreenshotRecording
{
    public interface IScreenshotUploadService : IApplicationService
    {
        Task<UploadStatus> UploadAsync(string userAgentId, long timestamp, ImageFormat imgFormat, Stream data, string[] tags = null);
    }
}