﻿using System;
using ScreenRecording.Application.Contracts.ScreenshotRecording.DTO;

namespace ScreenRecording.Application.Contracts.ScreenshotRecording
{
    public interface IScreenshotUrlFactory
    {
        Uri Create(ScreenshotKeyDto key);
    }
}