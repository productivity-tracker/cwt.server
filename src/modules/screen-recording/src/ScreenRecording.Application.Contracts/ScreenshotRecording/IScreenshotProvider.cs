﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CWT.Timing;
using ScreenRecording.Application.Contracts.ScreenshotRecording.DTO;
using Volo.Abp.Application.Services;

namespace ScreenRecording.Application.Contracts.ScreenshotRecording
{
    public interface IScreenshotProvider : IApplicationService
    {
        /// <summary>
        ///     Load screenshot data to stream
        /// </summary>
        /// <returns>Return content type</returns>
        Task<string> DownloadAsync(ScreenshotKeyDto screenshotKey, Stream stream);

        /// <summary>
        ///     See summary on <see cref="DownloadAsync(ScreenshotKeyDto, Stream)" />
        /// </summary>
        Task<string> DownloadAsync(string employeeId,
                                   Date date,
                                   string fileName, Stream stream);

        Task<IList<ScreenshotInfoDto>> GetListAsync(string employeeId, Date date);
    }
}