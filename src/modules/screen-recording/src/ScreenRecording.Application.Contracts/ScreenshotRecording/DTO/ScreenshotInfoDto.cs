﻿using System;

namespace ScreenRecording.Application.Contracts.ScreenshotRecording.DTO
{
    [Serializable]
    public class ScreenshotInfoDto
    {
        public ScreenshotKeyDto Key { get; set; }
        public Uri Url { get; set; }
        public ulong Size { get; set; }
        public DateTime? LastModified { get; set; }
    }
}