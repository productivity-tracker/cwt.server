﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using ScreenRecording.Domain.Shared;

namespace ScreenRecording.Application.Contracts.ScreenshotRecording.DTO
{
    public class ScreenshotDto
    {
        [Required]
        public string UserAgentId { get; set; }

        /// <summary>
        /// UnixTimeSeconds
        /// </summary>
        [Required]
        public long Timestamp { get; set; }

        [Required]
        public ImageFormat ImageFormat { get; set; }
        
        public IFormFile File { get; set; }

        public string[] Tags  { get; set; }
    }
}