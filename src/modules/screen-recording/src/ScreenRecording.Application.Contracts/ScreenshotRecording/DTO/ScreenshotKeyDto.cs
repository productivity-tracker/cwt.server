﻿using System;
using System.ComponentModel.DataAnnotations;
using ScreenRecording.Domain.Shared;

namespace ScreenRecording.Application.Contracts.ScreenshotRecording.DTO
{
    [Serializable]
    public class ScreenshotKeyDto
    {
        [Required]
        public string EmployeeId { get; set; }

        /// <summary>
        /// UnixTimeSeconds
        /// </summary>
        [Required]
        public long Timestamp { get; set; }

        [Required]
        public ImageFormat ImageFormat { get; set; }
    }
}