﻿namespace ScreenRecording.Application.Contracts.ScreenshotRecording.DTO
{
    public enum UploadStatusCode : byte
    {
        Unknown = 0,
        Ok = 1,
        Failed = 2
    }
}