﻿namespace ScreenRecording.Application.Contracts.ScreenshotRecording.DTO
{
    public class UploadStatus
    {
        public UploadStatusCode Status { get; }
        public string Message { get; }

        public UploadStatus(string message, byte status)
        {
            Message = message;
            Status = (UploadStatusCode)status;
        }
    }
}