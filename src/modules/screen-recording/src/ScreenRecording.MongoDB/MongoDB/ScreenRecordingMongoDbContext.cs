﻿using MongoDB.Driver;
using Volo.Abp.MongoDB;

namespace ScreenRecording.MongoDB.MongoDB
{
    public class ScreenRecordingMongoDbContext : AbpMongoDbContext, IScreenRecordingMongoDbContext
    {
        [MongoCollection(nameof(ScreenHistories))]
        public IMongoCollection<Domain.AggregatesModel.ScreenHistoryAggregate.ScreenHistory> ScreenHistories 
            => Collection<Domain.AggregatesModel.ScreenHistoryAggregate.ScreenHistory>();
    }
}