﻿using MongoDB.Driver;
using ScreenRecording.Domain;
using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace ScreenRecording.MongoDB.MongoDB
{
    [ConnectionStringName(ScreenRecordingConnectionDbProperties.ConnectionStringName)]
    public interface IScreenRecordingMongoDbContext : IAbpMongoDbContext
    {
        IMongoCollection<Domain.AggregatesModel.ScreenHistoryAggregate.ScreenHistory> ScreenHistories { get; }
    }
}