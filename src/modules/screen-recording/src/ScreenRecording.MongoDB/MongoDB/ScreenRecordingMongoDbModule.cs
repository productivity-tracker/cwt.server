﻿using Microsoft.Extensions.DependencyInjection;
using ScreenRecording.Domain;
using ScreenRecording.MongoDB.ScreenHistories;
using Volo.Abp.Modularity;
using Volo.Abp.MongoDB;

namespace ScreenRecording.MongoDB.MongoDB
{
    [DependsOn(typeof(AbpMongoDbModule), typeof(ScreenRecordingDomainModule))]
    public class ScreenRecordingMongoDbModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddMongoDbContext<ScreenRecordingMongoDbContext>(options =>
            {
                options.AddRepository<Domain.AggregatesModel.ScreenHistoryAggregate.ScreenHistory, ScreenHistoryRepository>();
            });
        }
    }
}