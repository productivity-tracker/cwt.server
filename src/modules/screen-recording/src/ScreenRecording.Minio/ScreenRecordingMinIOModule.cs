﻿using CWT.Data.Minio.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ScreenRecording.MinIO.ScreenshotRecording;
using Volo.Abp.Modularity;

namespace ScreenRecording.MinIO
{
    public class ScreenRecordingMinIOModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddTransient<IScreenshotItemMapper, ScreenshotItemMapper>();
            var configuration = context.Services.GetConfiguration();
            var minioSettings = configuration.GetSection(nameof(MinioSettings)).Get<MinioSettings>();
            context.Services.AddSingleton(minioSettings);
        }
    }
}