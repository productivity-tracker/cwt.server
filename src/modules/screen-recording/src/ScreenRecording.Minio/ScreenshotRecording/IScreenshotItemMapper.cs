﻿using CWT.Data.Minio;
using ScreenRecording.Domain.AggregatesModel.ScreenRecordAggregate;

namespace ScreenRecording.MinIO.ScreenshotRecording
{
    public interface IScreenshotItemMapper : IObjectMapper<ScreenshotItem>
    {
        
    }
}