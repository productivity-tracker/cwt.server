﻿using CWT.Data.Minio;
using CWT.Data.Minio.Configuration;
using Microsoft.Extensions.Logging;
using ScreenRecording.Domain.AggregatesModel.ScreenRecordAggregate;
using ScreenRecording.Domain.Services.ScreenshotRecording;

namespace ScreenRecording.MinIO.ScreenshotRecording
{
    public class ScreenshotStorage : MinioBlobRepository<ScreenshotItem>, IScreenshotStorage
    {
        public ScreenshotStorage(ILoggerFactory logeFactory, MinioSettings minioSettings, IScreenshotItemMapper mapper)
            : base(logeFactory, minioSettings, mapper)
        { }
    }
}