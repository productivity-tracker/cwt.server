﻿using System;
using Microsoft.Extensions.Logging;
using Minio.DataModel;
using ScreenRecording.Domain.AggregatesModel.ScreenRecordAggregate;

namespace ScreenRecording.MinIO.ScreenshotRecording
{
    public class ScreenshotItemMapper : IScreenshotItemMapper
    {
        private readonly ILogger _logger;
        public ScreenshotItemMapper(ILogger<ScreenshotItemMapper> logger)
        {
            _logger = logger;
        }

        public ScreenshotItem Map(Item item)
        {
            try
            {
                var key = new ScreenshotKey(item.Key);
                return new ScreenshotItem(key, item.Size, item.LastModifiedDateTime);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Error occured while {nameof(Map)}: {nameof(Item)} -> {nameof(ScreenshotItem)}");
                throw;
            }
        }
    }
}