﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ScreenRecording.Application.Contracts;
using ScreenRecording.Application.Contracts.ScreenshotRecording;
using ScreenRecording.Grpc.Client.ScreenshotUploading;
using Volo.Abp.Modularity;

namespace ScreenRecording.Grpc.Client
{
    [DependsOn(typeof(ScreenRecordingApplicationContractsModule))]
    public class ScreenRecordingGrpcClientModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            var configuration = context.Services.GetConfiguration();
            var grpcServer = configuration.GetConnectionString("ScreenRecordingGrpc");
            var uploadServiceProxy = new ScreenshotUploadServiceProxy(new Uri(grpcServer));
            context.Services.AddSingleton<IScreenshotUploadService>(uploadServiceProxy);
        }
    }
}