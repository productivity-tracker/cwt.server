﻿using System;
using System.Threading.Tasks;
using CWT.ScreenRecording;
using Google.Protobuf;
using Grpc.Net.Client;
using ScreenRecording.Application.Contracts.ScreenshotRecording;
using ScreenRecording.Application.Contracts.ScreenshotRecording.DTO;
using ScreenRecording.Domain.Shared;
using UploadStatus = CWT.ScreenRecording.UploadStatus;
using UploadStatusInternal = ScreenRecording.Application.Contracts.ScreenshotRecording.DTO.UploadStatus;

namespace ScreenRecording.Grpc.Client.ScreenshotUploading
{
    public class ScreenshotUploadServiceProxy : IScreenshotUploadService
    {
        private readonly ScreenshotUploadService.ScreenshotUploadServiceClient _client;
        private readonly DateTimeToStringConverter _dateTimeConverter;

        public ScreenshotUploadServiceProxy(Uri url)
        {
            var channel = GrpcChannel.ForAddress(url);
            _client = new ScreenshotUploadService.ScreenshotUploadServiceClient(channel);

            _dateTimeConverter = new DateTimeToStringConverter();
        }

        public async Task<UploadStatusInternal> UploadAsync(ScreenshotDto screenshot)
        {
            var request = new ScreenshotRequest
            {
                Key = Convert(screenshot.Key),
                Data = ByteString.Empty
            };

            var uploadStatus = await _client.UploadAsync(request);
            return Convert(uploadStatus);
        }

        private ScreenshotKey Convert(ScreenshotKeyDto key)
            => new ScreenshotKey
            {
                EmployeeId = key.EmployeeId,
                CreatedAt = _dateTimeConverter.Convert(key.CreatedAt),
                ImageFormat = key.ImageFormat.ToString("G")
            };

        private UploadStatusInternal Convert(UploadStatus status) 
            => new UploadStatusInternal(status.Message, (byte)status.Code);
    }
}