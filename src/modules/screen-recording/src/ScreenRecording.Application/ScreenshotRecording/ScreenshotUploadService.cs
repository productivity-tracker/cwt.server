﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using AgentIdentity.Application.Contracts.UserAgents;
using AgentIdentity.Application.Contracts.UserAgents.DTO;
using ScreenRecording.Application.Contracts.ScreenshotRecording;
using ScreenRecording.Application.Contracts.ScreenshotRecording.DTO;
using ScreenRecording.Domain.AggregatesModel.ScreenRecordAggregate;
using ScreenRecording.Domain.Services.ScreenshotRecording;
using ScreenRecording.Domain.Shared;
using Volo.Abp;
using Volo.Abp.Application.Services;
using Microsoft.Extensions.Logging;

namespace ScreenRecording.Application.ScreenshotRecording
{
    public class ScreenshotUploadService : ApplicationService, IScreenshotUploadService, IDisposable
    {
        private readonly ILogger _logger;

        private readonly IScreenshotStorage _screenshotStorage;
        private readonly IDictionary<string, string> _userAgentToEmployeeMap = new ConcurrentDictionary<string, string>();
        private readonly IUserAgentService _userAgentService;

        private readonly IDisposable _subscription;

        public ScreenshotUploadService(
            ILogger<ScreenshotUploadService> logger,
            IScreenshotStorage screenshotStorage,
            IObservable<UserAgentDto> userAgentChanged,
            IUserAgentService userAgentService)
        {
            _logger = logger;

            _screenshotStorage = screenshotStorage;
            _userAgentService = userAgentService;

            _subscription = userAgentChanged.Subscribe(
                x => _userAgentToEmployeeMap.Add(x.Id, x.EmployeeId.ToString("N")));
        }

        public void Dispose() => _subscription?.Dispose();

        public async Task<UploadStatus> UploadAsync(string userAgentId, long timestamp, ImageFormat imgFormat, Stream data, string[] tags = null)
        {
            Check.NotNull(userAgentId, nameof(userAgentId));

            if (!_userAgentToEmployeeMap.TryGetValue(userAgentId, out var employeeId))
            {
                employeeId = (await _userAgentService.GetAsync(userAgentId)).Id;
                if (employeeId.IsNullOrWhiteSpace())
                {
                    _logger.LogWarning($"Not found {nameof(employeeId)} for {nameof(userAgentId)} '{userAgentId}'");
                    return null;
                }

                _userAgentToEmployeeMap.Add(userAgentId, employeeId);
            }

            var domainKey = new ScreenshotKey(employeeId, DateTimeOffset.FromUnixTimeSeconds(timestamp).DateTime, imgFormat);
            await _screenshotStorage.PutAsync(domainKey.ToString(), data, ContentTypeMap.Convert(domainKey.ImageFormat));

            return new UploadStatus(string.Empty, (byte)UploadStatusCode.Ok);
        }
    }
}