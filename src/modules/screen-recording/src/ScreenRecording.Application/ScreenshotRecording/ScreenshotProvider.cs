﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using CWT.Timing;
using Microsoft.Extensions.Logging;
using ScreenRecording.Application.Contracts.ScreenshotRecording;
using ScreenRecording.Application.Contracts.ScreenshotRecording.DTO;
using ScreenRecording.Domain.AggregatesModel.ScreenRecordAggregate;
using ScreenRecording.Domain.Services.ScreenshotRecording;
using Volo.Abp;
using Volo.Abp.Application.Services;
using Volo.Abp.Validation;

namespace ScreenRecording.Application.ScreenshotRecording
{
    public class ScreenshotProvider : ApplicationService, IScreenshotProvider
    {
        private readonly IScreenshotStorage _screenshotStorage;
        private readonly IScreenshotUrlFactory _urlFactory;
        private readonly ILogger _logger;

        public ScreenshotProvider(ILogger<ScreenshotProvider> logger, IScreenshotStorage screenshotStorage, IScreenshotUrlFactory urlFactory)
        {
            _screenshotStorage = screenshotStorage;
            _urlFactory = urlFactory;
            _logger = logger;
        }

        public Task<string> DownloadAsync(ScreenshotKeyDto screenshotKey, Stream stream)
        {
            Check.NotNull(screenshotKey, nameof(screenshotKey));

            var key = ObjectMapper.Map<ScreenshotKeyDto, ScreenshotKey>(screenshotKey);
            return _screenshotStorage.GetAsync(key.ToString(), stream);
        }

        [DisableValidation]
        public Task<string> DownloadAsync(string employeeId,
                                          Date date,
                                          string fileName,
                                          Stream stream)
            => _screenshotStorage.GetAsync(ScreenRecordKey.GetRelativePath(employeeId, date, fileName), stream);

        public async Task<IList<ScreenshotInfoDto>> GetListAsync(string employeeId, Date date)
        {
            var cts = new CancellationTokenSource(TimeSpan.FromSeconds(60));
            var result = new List<ScreenshotInfoDto>();
            var taskCompletionSource = new TaskCompletionSource<object>();

            var prefix = ScreenRecordKey.GetPrefix(employeeId, date);
            _screenshotStorage.GetListObjectsAsync(prefix, recursive: true, cts.Token)
                              .Subscribe(
                                  item =>
                                  {
                                      var dto = Convert(item);
                                      dto.Url = _urlFactory?.Create(dto.Key);
                                      result.Add(dto);
                                  },
                                  ex => _logger.LogError($"Error occurred while {nameof(GetListAsync)} (EmployeeId: {employeeId}, Date: {date})", ex),
                                  () =>
                                  {
                                      _logger.LogDebug($"Completed action {nameof(GetListAsync)} (EmployeeId: {employeeId}, Date: {date}) ");
                                      taskCompletionSource.SetResult(null);
                                  },
                                  cts.Token);

            await taskCompletionSource.Task;
            return result;
        }

        private ScreenshotInfoDto Convert(ScreenshotItem screenshotItem)
            => new ScreenshotInfoDto
            {
                Key = ObjectMapper.Map<ScreenshotKey, ScreenshotKeyDto>(screenshotItem.Key),
                LastModified = screenshotItem.LastModified,
                Size = screenshotItem.Size
            };
    }
}