﻿using ScreenRecording.Application.Contracts;
using ScreenRecording.Domain;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;

namespace ScreenRecording.Application.ScreenshotRecording
{
    [DependsOn(
        typeof(ScreenRecordingDomainModule),
        typeof(ScreenRecordingApplicationContractsModule),
        typeof(AbpAutoMapperModule))]
    public class ScreenshotRecordingApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {

        }
    }
}