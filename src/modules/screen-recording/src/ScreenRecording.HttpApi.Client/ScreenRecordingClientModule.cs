﻿using Microsoft.Extensions.DependencyInjection;
using ScreenRecording.Application.Contracts;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;

namespace ScreenRecording.HttpApi.Client
{
    [DependsOn(
        typeof(ScreenRecordingApplicationContractsModule), 
        typeof(AbpHttpClientModule))]
    public class ScreenRecordingClientModule : AbpModule
    {
        public const string REMOTE_SERVICE_NAME = "ScreenRecording";

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddHttpClientProxies(typeof(ScreenRecordingApplicationContractsModule).Assembly,
                                                  REMOTE_SERVICE_NAME);
        }
    }
}