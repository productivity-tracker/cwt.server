﻿using Volo.Abp.Modularity;

namespace TimelineTracking
{
    [DependsOn(
        typeof(TimelineTrackingApplicationModule),
        typeof(TimelineTrackingDomainTestModule)
        )]
    public class TimelineTrackingApplicationTestModule : AbpModule
    {

    }
}
