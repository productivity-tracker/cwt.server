﻿using TimelineTracking.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace TimelineTracking
{
    /* Domain tests are configured to use the EF Core provider.
     * You can switch to MongoDB, however your domain tests should be
     * database independent anyway.
     */
    [DependsOn(
        typeof(TimelineTrackingEntityFrameworkCoreTestModule)
        )]
    public class TimelineTrackingDomainTestModule : AbpModule
    {
        
    }
}
