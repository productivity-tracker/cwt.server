﻿namespace TimelineTracking
{
    /* Inherit from this class for your domain layer tests.
     * See SampleManager_Tests for example.
     */
    public abstract class TimelineTrackingDomainTestBase : TimelineTrackingTestBase<TimelineTrackingDomainTestModule>
    {

    }
}