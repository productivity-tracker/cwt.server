﻿using Volo.Abp.Http.Client.IdentityModel;
using Volo.Abp.Modularity;

namespace TimelineTracking
{
    [DependsOn(
        typeof(TimelineTrackingHttpApiClientModule),
        typeof(AbpHttpClientIdentityModelModule)
        )]
    public class TimelineTrackingConsoleApiClientModule : AbpModule
    {
        
    }
}
