﻿using System.Linq;
using System.Threading.Tasks;
using CWT.Timing;
using Microsoft.AspNetCore.Authorization;
using TimelineTracking.AggregatesModel.TimelineAggregate;
using TimelineTracking.Timelines.DTO;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace TimelineTracking.Timelines
{
    [Authorize]
    public class TimelineService : ApplicationService, ITimelineService
    {
        private readonly ITimelineRepository _repository;

        public TimelineService(ITimelineRepository repository)
        {
            _repository = repository;
        }
        
        public async Task<PagedResultDto<TimelineDto>> GetRangeAsync(DateTimeRangeRequest range)
        {
            var entities = await _repository.GetRangeAsync(new DateTimeRange(range.StartTime, range.EndTime));
            return new PagedResultDto<TimelineDto>(entities.Count,
                                                   entities.Select(e => ObjectMapper.Map<Timeline, TimelineDto>(e)).ToList());
        }
    }
}