﻿using AutoMapper;
using TimelineTracking.AggregatesModel.TimelineAggregate;
using TimelineTracking.Timelines.DTO;

namespace TimelineTracking.Timelines
{
    public class TimelineApplicationAutoMapperProfile : Profile
    {
        public TimelineApplicationAutoMapperProfile()
        {
            CreateMap<Timeline, TimelineDto>(MemberList.Destination).ReverseMap();
            CreateMap<TimelineKey, TimelineKeyDto>(MemberList.Destination).ReverseMap();
        }
    }
}