﻿using TimelineTracking.AggregatesModel.TimelineAggregate;
using TimelineTracking.Timelines.DTO;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;
using Volo.Abp.Application;
using Volo.Abp.EventBus.Distributed;

namespace TimelineTracking
{
    [DependsOn(
        typeof(TimelineTrackingDomainModule),
        typeof(TimelineTrackingApplicationContractsModule),
        typeof(AbpDddApplicationModule),
        typeof(AbpAutoMapperModule))]
    public class TimelineTrackingApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpAutoMapperOptions>(options =>
            {
                options.AddMaps<TimelineTrackingApplicationModule>(validate: true);
            });

            Configure<AbpDistributedEventBusOptions>(options =>
            {
                options.EtoMappings.Add<Timeline, TimelineDto>();
            });
        }
    }
}
