﻿using System.Collections.Generic;
using CWT.Timing;

namespace TimelineTracking
{
    public class DateTimeRangeComparer : IComparer<DateTimeRange>
    {
        public int Compare(DateTimeRange x, DateTimeRange y)
        {
            if (x == null && y != null) 
                return -1;
            if (x != null && y == null) 
                return 1;
            if (x?.StartTime == y?.StartTime) 
                return 0;
            if (x.StartTime < y.StartTime) 
                return -1;

            return 1;
        }
    }
}