﻿using Volo.Abp.Modularity;
using Volo.Abp.Localization;

namespace TimelineTracking
{
    [DependsOn(typeof(AbpLocalizationModule))]
    public class TimelineTrackingDomainSharedModule : AbpModule
    {
    }
}
