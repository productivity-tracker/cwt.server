﻿namespace TimelineTracking.Timelines
{
    public enum TimelineSource
    {
        Device,
        UserAgent,
        Employee
    }
}