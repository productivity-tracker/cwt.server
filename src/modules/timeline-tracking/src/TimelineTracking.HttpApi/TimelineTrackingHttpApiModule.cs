﻿using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Modularity;
using Microsoft.Extensions.DependencyInjection;

namespace TimelineTracking
{
    [DependsOn(
        typeof(TimelineTrackingApplicationContractsModule),
        typeof(AbpAspNetCoreMvcModule))]
    public class TimelineTrackingHttpApiModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            PreConfigure<IMvcBuilder>(mvcBuilder =>
            {
                mvcBuilder.AddApplicationPartIfNotExists(typeof(TimelineTrackingHttpApiModule).Assembly);
            });
        }
    }
}
