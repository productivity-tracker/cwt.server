﻿using Volo.Abp.Settings;

namespace TimelineTracking.Settings
{
    public class TimelineTrackingSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            /* Define module settings here.
             * Use names from TimelineTrackingSettings class.
             */
        }
    }
}