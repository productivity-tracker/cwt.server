﻿namespace TimelineTracking
{
    public static class TimelineTrackingDbProperties
    {
        public static string DbTablePrefix { get; set; } = "TimelineTrk"; 

        public static string DbSchema { get; set; } = null;

        public const string ConnectionStringName = "TimelineTracking";
    }
}
