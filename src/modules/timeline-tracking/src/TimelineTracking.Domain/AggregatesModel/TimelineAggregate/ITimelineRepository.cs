﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CWT.Timing;
using Volo.Abp.Domain.Repositories;

namespace TimelineTracking.AggregatesModel.TimelineAggregate
{
    public interface ITimelineRepository : IReadOnlyBasicRepository<Timeline, Guid>
    {
        Task<IList<Timeline>> GetRangeAsync(DateTimeRange range, CancellationToken cancellationToken = default);
    }
}