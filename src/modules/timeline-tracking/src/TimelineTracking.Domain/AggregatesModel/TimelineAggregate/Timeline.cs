﻿using System;
using System.Collections.Generic;
using System.Linq;
using CWT.Timing;
using Volo.Abp.Domain.Entities;

namespace TimelineTracking.AggregatesModel.TimelineAggregate
{
    public class Timeline : AggregateRoot<Guid>, IEquatable<Timeline>
    {
        internal const int DELAY_BEFORE_NEW_RANGE_IN_SECONDS = 30;

        private readonly DateTimeRangeComparer _sessionComparer = new DateTimeRangeComparer();
        public TimelineKey Key { get; private set; }

        public SortedSet<DateTimeRange> Sessions { get; private set; }

        protected Timeline()
        { }

        public Timeline(Guid id, TimelineKey key, IEnumerable<DateTimeRange> sessions)
            : base(id)
        {
            Key = key;
            Sessions = new SortedSet<DateTimeRange>(sessions, _sessionComparer);
        }

        public Timeline UpdateSession(DateTime lastActivityTime)
        {
            if (Sessions.Any())
            {
                var lastSession = Sessions.Last();
                if (lastSession.EndTime.AddSeconds(DELAY_BEFORE_NEW_RANGE_IN_SECONDS) > lastActivityTime)
                {
                    lastSession.EndTime = lastActivityTime;
                    return this;
                }
            }

            Sessions.Add(new DateTimeRange(lastActivityTime, lastActivityTime));
            return this;
        }

        #region IEquatable Support

        public bool Equals(Timeline other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(Key, other.Key) && Equals(Sessions, other.Sessions);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return EntityEquals(obj) && Equals((Timeline)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Key != null ? Key.GetHashCode() : 0) * 397) ^ (Sessions != null ? Sessions.GetHashCode() : 0);
            }
        }

        public static bool operator ==(Timeline left, Timeline right) => Equals(left, right);

        public static bool operator !=(Timeline left, Timeline right) => !Equals(left, right);

        #endregion
    }
}