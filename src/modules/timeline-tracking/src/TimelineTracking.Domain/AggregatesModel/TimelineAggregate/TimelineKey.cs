﻿using System;
using TimelineTracking.Timelines;

namespace TimelineTracking.AggregatesModel.TimelineAggregate
{
    public class TimelineKey : IEquatable<TimelineKey>, IComparable<TimelineKey>, IComparable
    {
        public string Date { get; private set; }

        public TimelineSource Source { get; private set; }

        public string SourceId { get; private set; }

        protected TimelineKey()
        { }

        public TimelineKey(string date, TimelineSource source, string sourceId)
        {
            Date = date;
            Source = source;
            SourceId = sourceId;
        }

        public int CompareTo(object obj)
        {
            if (ReferenceEquals(objA: null, obj)) return 1;
            if (ReferenceEquals(this, obj)) return 0;
            return obj is TimelineKey other ? CompareTo(other) : throw new ArgumentException($"Object must be of type {nameof(TimelineKey)}");
        }

        public int CompareTo(TimelineKey other)
        {
            if (ReferenceEquals(this, other)) return 0;
            if (ReferenceEquals(objA: null, other)) return 1;
            var dateComparison = string.Compare(Date, other.Date, StringComparison.Ordinal);
            if (dateComparison != 0) return dateComparison;
            var sourceComparison = Source.CompareTo(other.Source);
            if (sourceComparison != 0) return sourceComparison;
            return string.Compare(SourceId, other.SourceId, StringComparison.Ordinal);
        }

        #region IEquatable Support

        public bool Equals(TimelineKey other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Date == other.Date && Source == other.Source && SourceId == other.SourceId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((TimelineKey)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Date != null ? Date.GetHashCode() : 0;
                hashCode = (hashCode * 397) ^ (int)Source;
                hashCode = (hashCode * 397) ^ (SourceId != null ? SourceId.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(TimelineKey left, TimelineKey right) => Equals(left, right);

        public static bool operator !=(TimelineKey left, TimelineKey right) => !Equals(left, right);

        #endregion
    }
}