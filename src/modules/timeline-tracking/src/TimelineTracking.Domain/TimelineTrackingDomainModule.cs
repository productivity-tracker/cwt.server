﻿using Volo.Abp.Modularity;

namespace TimelineTracking
{
    [DependsOn(
        typeof(TimelineTrackingDomainSharedModule)
        )]
    public class TimelineTrackingDomainModule : AbpModule
    {

    }
}
