﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;

namespace TimelineTracking
{
    [DependsOn(
        typeof(TimelineTrackingApplicationContractsModule),
        typeof(AbpHttpClientModule))]
    public class TimelineTrackingHttpApiClientModule : AbpModule
    {
        public const string RemoteServiceName = "TimelineTracking";

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddHttpClientProxies(
                typeof(TimelineTrackingApplicationContractsModule).Assembly,
                RemoteServiceName
            );
        }
    }
}
