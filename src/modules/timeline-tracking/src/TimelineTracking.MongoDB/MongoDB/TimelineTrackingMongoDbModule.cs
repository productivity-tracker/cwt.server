﻿using Microsoft.Extensions.DependencyInjection;
using TimelineTracking.AggregatesModel.TimelineAggregate;
using TimelineTracking.Timelines;
using Volo.Abp.Modularity;
using Volo.Abp.MongoDB;

namespace TimelineTracking.MongoDB
{
    [DependsOn(
        typeof(TimelineTrackingDomainModule),
        typeof(AbpMongoDbModule))]
    public class TimelineTrackingMongoDbModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddMongoDbContext<TimelineTrackingMongoDbContext>(options =>
            {
                options.AddRepository<Timeline, TimelineRepository>();
            });
        }
    }
}
