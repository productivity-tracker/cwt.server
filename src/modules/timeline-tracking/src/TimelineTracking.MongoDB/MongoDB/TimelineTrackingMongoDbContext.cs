﻿using MongoDB.Driver;
using TimelineTracking.AggregatesModel.TimelineAggregate;
using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace TimelineTracking.MongoDB
{
    [ConnectionStringName(TimelineTrackingDbProperties.ConnectionStringName)]
    public class TimelineTrackingMongoDbContext : AbpMongoDbContext, ITimelineTrackingMongoDbContext
    {
        [MongoCollection(nameof(Timelines))]
        public IMongoCollection<Timeline> Timelines => Collection<Timeline>();
    }
}