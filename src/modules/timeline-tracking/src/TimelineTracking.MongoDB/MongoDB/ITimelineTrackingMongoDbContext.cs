﻿using MongoDB.Driver;
using TimelineTracking.AggregatesModel.TimelineAggregate;
using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace TimelineTracking.MongoDB
{
    [ConnectionStringName(TimelineTrackingDbProperties.ConnectionStringName)]
    public interface ITimelineTrackingMongoDbContext : IAbpMongoDbContext
    {
        IMongoCollection<Timeline> Timelines { get; }
    }
}
