﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CWT.Timing;
using MongoDB.Driver;
using TimelineTracking.AggregatesModel.TimelineAggregate;
using TimelineTracking.MongoDB;
using Volo.Abp.Domain.Repositories.MongoDB;
using Volo.Abp.MongoDB;

namespace TimelineTracking.Timelines
{
    internal class TimelineRepository : MongoDbRepository<ITimelineTrackingMongoDbContext, Timeline, Guid>, 
                                        ITimelineRepository
    {
        public TimelineRepository(IMongoDbContextProvider<ITimelineTrackingMongoDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
            EnsureHashedIndexOnDate(Collection);
        }

        public async Task<IList<Timeline>> GetRangeAsync(DateTimeRange range, CancellationToken cancellationToken = default)
        {
            var allDates = range.GetDates();
            var filter = new FilterDefinitionBuilder<Timeline>().In(e => e.Key.Date, allDates.Select(d => d.ToString()));
            var result = await Collection.FindAsync(filter, cancellationToken: cancellationToken);
            return await result.ToListAsync(cancellationToken: cancellationToken);
        }

        private void EnsureHashedIndexOnDate(IMongoCollection<Timeline> collection)
        {
            var model = new CreateIndexModel<Timeline>(Builders<Timeline>.IndexKeys.Hashed(timeline => timeline.Key.Date), 
                                                       new CreateIndexOptions { Background = true });
            collection.Indexes.CreateOneAsync(model);
        }
    }
}