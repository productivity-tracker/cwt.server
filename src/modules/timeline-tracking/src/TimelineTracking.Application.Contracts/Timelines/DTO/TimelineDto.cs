﻿using System;
using System.Collections.Generic;
using CWT.Timing;
using Volo.Abp.Application.Dtos;

namespace TimelineTracking.Timelines.DTO
{
    public class TimelineDto : EntityDto<Guid>
    {
        public TimelineKeyDto Key { get; set; }

        public SortedSet<DateTimeRange> Sessions { get; set; }
    }
}