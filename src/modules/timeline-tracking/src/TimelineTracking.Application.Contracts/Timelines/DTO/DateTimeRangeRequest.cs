﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TimelineTracking.Timelines.DTO
{
    [Serializable]
    public class DateTimeRangeRequest
    {
        /// <summary>
        ///     Start time of the datetime range.
        /// </summary>
        [Required]
        public DateTime StartTime { get; set; }

        
        /// <summary>
        ///     End time of the datetime range.
        /// </summary>
        [Required]
        public DateTime EndTime { get; set; }
    }
}