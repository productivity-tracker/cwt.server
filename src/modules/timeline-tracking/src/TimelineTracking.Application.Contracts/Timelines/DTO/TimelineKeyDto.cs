﻿using System;

namespace TimelineTracking.Timelines.DTO
{
    [Serializable]
    public class TimelineKeyDto
    {
        public string Date { get; set; }

        public TimelineSource Source { get; set; }

        public string SourceId { get; set; }
    }
}