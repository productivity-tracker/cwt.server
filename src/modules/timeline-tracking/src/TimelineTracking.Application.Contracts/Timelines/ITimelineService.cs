﻿using System.Threading.Tasks;
using TimelineTracking.Timelines.DTO;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace TimelineTracking.Timelines
{
    public interface ITimelineService : IApplicationService
    {
        Task<PagedResultDto<TimelineDto>> GetRangeAsync(DateTimeRangeRequest range);
    }
}