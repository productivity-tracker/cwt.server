﻿using Volo.Abp.Localization;

namespace ScreenRecordingAdministration.Localization
{
    [LocalizationResourceName("ScreenRecordingAdministration")]
    public class ScreenRecordingAdministrationResource
    {
        
    }
}
