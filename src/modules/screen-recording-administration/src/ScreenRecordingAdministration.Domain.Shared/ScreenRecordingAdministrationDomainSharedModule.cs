﻿using Volo.Abp.Modularity;
using Volo.Abp.Localization;

namespace ScreenRecordingAdministration
{
    [DependsOn(
        typeof(AbpLocalizationModule)
    )]
    public class ScreenRecordingAdministrationDomainSharedModule : AbpModule
    {
    }
}
