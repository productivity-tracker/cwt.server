﻿using System;

namespace ScreenRecordingAdministration.ScreenRecordSettings
{
    [Flags]
    public enum RecordMode
    {
        Always = 1,
        OnUserAction = 2, 
        ProcessMonitoring = 4,
        WebSiteMonitoring = 5
    }
}