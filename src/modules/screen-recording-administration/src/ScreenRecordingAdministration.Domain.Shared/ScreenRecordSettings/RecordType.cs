﻿namespace ScreenRecordingAdministration.ScreenRecordSettings
{
    public enum RecordType
    {
        Screenshot = 1,
        DesktopDuplicationStream,
        Auto
    }
}