﻿using System;
using System.Collections.Generic;
using ScreenRecordingAdministration.ScreenRecordSettings;

namespace ScreenRecordingAdministration.Agent.Contracts.RecordSettings.DTO
{
    [Serializable]
    public class ScreenRecordAgentSettingsDto
    {
        public bool EnableRecording { get; set; }
        public RecordMode RecordMode { get; set; }
        public RecordType RecordType { get; set; }
        public ScreenshotSettingsDto ScreenshotSettings { get; set; }
        public ISet<string> ObserveProcesses { get; set; }
        public string StorageAdapterAddress { get; set; }
    }
}