﻿using System;
using ScreenRecording.Domain.Shared;

namespace ScreenRecordingAdministration.Agent.Contracts.RecordSettings.DTO
{
    [Serializable]
    public class ScreenshotSettingsDto
    {
        public TimeSpan ScreenshotsInterval { get; set; }
        public ImageFormat ImageFormat { get; set; }
        public long Quality { get; set; }
    }
}