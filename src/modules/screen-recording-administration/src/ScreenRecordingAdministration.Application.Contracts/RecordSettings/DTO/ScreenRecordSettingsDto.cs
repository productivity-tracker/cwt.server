﻿using System;
using System.Collections.Generic;
using ScreenRecordingAdministration.ScreenRecordSettings;
using Volo.Abp.Application.Dtos;

namespace ScreenRecordingAdministration.RecordSettings.DTO
{
    [Serializable]
    public class ScreenRecordSettingsDto : FullAuditedEntityDto<Guid>
    {
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public bool IsDefault { get; set; }
        public RecordMode RecordMode { get; set; }
        public RecordType RecordType { get; set; }
        public TimeSpan FrameFrequency { get; set; }
        public ScreenshotSettingsDto ScreenshotSettings { get; set; }
        public ISet<string> ProcessSetMonitoring { get; set; }
        public TimeSpan ScreenRecordTTL { get; set; }
        public Guid StorageAdapterId { get; set; }
    }
}