﻿using System;
using ScreenRecordingAdministration.RecordSettings.DTO;
using Volo.Abp.Application.Services;

namespace ScreenRecordingAdministration.RecordSettings
{
    public interface IScreenRecordSettingsService : ICrudAppService<ScreenRecordSettingsDto, Guid>
    {
        
    }
}