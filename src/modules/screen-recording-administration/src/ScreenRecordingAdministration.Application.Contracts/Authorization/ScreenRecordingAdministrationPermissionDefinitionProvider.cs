﻿using ScreenRecordingAdministration.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace ScreenRecordingAdministration.Authorization
{
    public class ScreenRecordingAdministrationPermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            //var moduleGroup = context.AddGroup(ScreenRecordingAdministrationPermissions.GroupName, L("Permission:ScreenRecordingAdministration"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<ScreenRecordingAdministrationResource>(name);
        }
    }
}