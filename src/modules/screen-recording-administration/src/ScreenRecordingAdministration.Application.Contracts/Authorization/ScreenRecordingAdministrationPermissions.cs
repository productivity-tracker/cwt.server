﻿using Volo.Abp.Reflection;

namespace ScreenRecordingAdministration.Authorization
{
    public class ScreenRecordingAdministrationPermissions
    {
        public const string GroupName = "ScreenRecordingAdministration";

        public static string[] GetAll()
        {
            return ReflectionHelper.GetPublicConstantsRecursively(typeof(ScreenRecordingAdministrationPermissions));
        }
    }
}