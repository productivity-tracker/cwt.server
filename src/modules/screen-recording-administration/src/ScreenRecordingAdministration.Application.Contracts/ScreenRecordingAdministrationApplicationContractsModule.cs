﻿using Volo.Abp.Application;
using Volo.Abp.Modularity;
using Volo.Abp.VirtualFileSystem;
using Volo.Abp.Authorization;

namespace ScreenRecordingAdministration
{
    [DependsOn(
        typeof(ScreenRecordingAdministrationDomainSharedModule),
        typeof(AbpDddApplicationContractsModule),
        typeof(AbpAuthorizationModule)
        )]
    public class ScreenRecordingAdministrationApplicationContractsModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpVirtualFileSystemOptions>(options =>
            {
                options.FileSets.AddEmbedded<ScreenRecordingAdministrationApplicationContractsModule>("ScreenRecordingAdministration");
            });
        }
    }
}
