﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;

namespace ScreenRecordingAdministration
{
    [DependsOn(
        typeof(ScreenRecordingAdministrationApplicationContractsModule),
        typeof(AbpHttpClientModule))]
    public class ScreenRecordingAdministrationHttpApiClientModule : AbpModule
    {
        public const string RemoteServiceName = "ScreenRecordingAdministration";

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddHttpClientProxies(
                typeof(ScreenRecordingAdministrationApplicationContractsModule).Assembly,
                RemoteServiceName
            );
        }
    }
}
