﻿using ScreenRecordingAdministration.RecordSettings.DTO;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;
using Volo.Abp.Application;
using Volo.Abp.EventBus.Distributed;

namespace ScreenRecordingAdministration
{
    [DependsOn(
        typeof(ScreenRecordingAdministrationDomainModule),
        typeof(ScreenRecordingAdministrationApplicationContractsModule),
        typeof(AbpDddApplicationModule),
        typeof(AbpAutoMapperModule)
        )]
    public class ScreenRecordingAdministrationApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpAutoMapperOptions>(options =>
            {
                options.AddMaps<ScreenRecordingAdministrationApplicationModule>(validate: true);
            });

            Configure<AbpDistributedEventBusOptions>(options =>
            {
                options.EtoMappings.Add<AggregatesModel.RecordSettingsAggregate.ScreenRecordSettings, ScreenRecordSettingsDto>();
            });
        }
    }
}
