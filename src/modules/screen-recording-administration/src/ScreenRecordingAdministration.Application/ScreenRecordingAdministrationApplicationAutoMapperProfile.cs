﻿using AutoMapper;

namespace ScreenRecordingAdministration
{
    public class ScreenRecordingAdministrationApplicationAutoMapperProfile : Profile
    {
        public ScreenRecordingAdministrationApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}