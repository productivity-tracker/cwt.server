﻿using System;
using ScreenRecordingAdministration.RecordSettings;
using ScreenRecordingAdministration.RecordSettings.DTO;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace ScreenRecordingAdministration
{
    public class ScreenRecordingAdmSettingsService
        : CrudAppService<AggregatesModel.RecordSettingsAggregate.ScreenRecordSettings, ScreenRecordSettingsDto, Guid>,
          IScreenRecordSettingsService
    {
        public ScreenRecordingAdmSettingsService(IRepository<AggregatesModel.RecordSettingsAggregate.ScreenRecordSettings, Guid> repository)
            : base(repository)
        { }
    }
}