﻿using ScreenRecordingAdministration.Localization;
using Volo.Abp.Application.Services;

namespace ScreenRecordingAdministration
{
    public abstract class ScreenRecordingAdministrationAppService : ApplicationService
    {
        protected ScreenRecordingAdministrationAppService()
        {
            LocalizationResource = typeof(ScreenRecordingAdministrationResource);
        }
    }
}
