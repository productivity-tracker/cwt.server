﻿namespace ScreenRecordingAdministration
{
    public static class ScreenRecordingAdministrationDbProperties
    {
        public static string DbTablePrefix { get; set; } = "ScreenRecordingAdm";

        public static string DbSchema { get; set; } = null;

        public const string ConnectionStringName = "ScreenRecordingAdministration";
    }
}
