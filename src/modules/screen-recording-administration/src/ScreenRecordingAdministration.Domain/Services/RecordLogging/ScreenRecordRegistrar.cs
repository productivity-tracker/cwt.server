﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CWT.Timing;
using ScreenRecordingAdministration.AggregatesModel.RecordLogAggregate;
using Volo.Abp;
using Volo.Abp.Guids;

namespace ScreenRecordingAdministration.Services.RecordLogging
{
    public class ScreenRecordRegistrar : IScreenRecordRegistrar
    {
        internal const int DELAY_BEFORE_NEW_RANGE_IN_SECONDS = 30;

        private readonly ConcurrentDictionary<RecordSetKey, ScreenRecordingTrace> _registry;
        private readonly IScreenRecordLogRepository _repository;
        private readonly IGuidGenerator _guidGenerator;

        public ScreenRecordRegistrar(IScreenRecordLogRepository screenRecordLogRepository, IGuidGenerator guidGenerator)
        {
            _registry = new ConcurrentDictionary<RecordSetKey, ScreenRecordingTrace>();
            _repository = screenRecordLogRepository;
            _guidGenerator = guidGenerator;
        }

        public async Task RegisterAsync(IDictionary<RecordSetKey, ScreenRecordingTrace> newRecords)
        {
            var intervalsForUpdate = new Dictionary<RecordSetKey, DateTimeRange>();
            var recordsForInsert = new Dictionary<RecordSetKey, ScreenRecordingTrace>();
            var historiesForInsert = new List<ScreenRecordLog>();

            foreach (var recordPair in newRecords)
            {
                var key = recordPair.Key;
                var newRecord = recordPair.Value;

                Check.NotNull(key, nameof(key));
                Check.NotNull(newRecord, nameof(newRecord));

                if (!_registry.TryGetValue(key, out var currentRecord))
                {
                    var history = await _repository.FindByKeyAsync(key);
                    if (history != null)
                    {
                        currentRecord = history.RecordsRegistry?.LastOrDefault();
                    }
                    else
                    {
                        history = new ScreenRecordLog(_guidGenerator.Create(), key, new List<ScreenRecordingTrace> { newRecord });
                        historiesForInsert.Add(history);
                        AddToCache(key, newRecord);

                        continue;
                    }
                }

                if (currentRecord != null && currentRecord.Type == newRecord.Type && currentRecord.RecordLocation == newRecord.RecordLocation
                 && currentRecord.Interval.EndTime.AddSeconds(DELAY_BEFORE_NEW_RANGE_IN_SECONDS) > newRecord.Interval.StartTime)
                {
                    var newInterval = new DateTimeRange(currentRecord.Interval.StartTime, newRecord.Interval.EndTime);
                    intervalsForUpdate.Add(key, newInterval);
                    newRecord = currentRecord.UpdateInterval(newInterval.EndTime);
                }
                else
                {
                    recordsForInsert.Add(key, newRecord);
                }

                AddToCache(key, newRecord);
            }

            var tasks = new List<Task>();

            if (historiesForInsert.Any())
            {
                tasks.Add(_repository.BulkInsertAsync(historiesForInsert));
            }

            if (recordsForInsert.Any())
            {
                tasks.Add(_repository.BulkInsertScreenRecordsAsync(recordsForInsert));
            }

            if (intervalsForUpdate.Any())
            {
                tasks.Add(_repository.BulkUpdateLastIntervalsAsync(intervalsForUpdate));
            }

            await Task.WhenAll(tasks);
        }

        private void AddToCache(RecordSetKey setKey, ScreenRecordingTrace newRecordingTrace)
            => _registry.AddOrUpdate(setKey, newRecordingTrace, (historyKey, record) => newRecordingTrace);
    }
}