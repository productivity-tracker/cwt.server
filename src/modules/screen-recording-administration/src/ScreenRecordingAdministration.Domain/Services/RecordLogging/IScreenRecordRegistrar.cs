﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ScreenRecordingAdministration.AggregatesModel.RecordLogAggregate;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Services;

namespace ScreenRecordingAdministration.Services.RecordLogging
{
    public interface IScreenRecordRegistrar : IDomainService, ISingletonDependency
    {
        Task RegisterAsync(IDictionary<RecordSetKey, ScreenRecordingTrace> newRecords);
    }
}