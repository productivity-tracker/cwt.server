﻿using System;
using System.Threading.Tasks;
using ScreenRecordingAdministration.AggregatesModel.RecordSettingsBindingAggregate;
using Volo.Abp.Guids;

namespace ScreenRecordingAdministration.Services.RecordSettingsBinding
{
    public class RecordSettingsBindingManager
    {
        private readonly IScreenRecordSettingsBindingRepository _bindingRepository;
        private readonly IGuidGenerator _guidGenerator;

        public RecordSettingsBindingManager(IGuidGenerator guidGenerator, IScreenRecordSettingsBindingRepository bindingRepository)
        {
            _guidGenerator = guidGenerator;
            _bindingRepository = bindingRepository;
        }

        public async Task<ScreenRecordSettingsBinding> CreateBinding(Guid employeeId, Guid settingId, BindingType type)
        {
            var binding = new ScreenRecordSettingsBinding(_guidGenerator.Create(), 
                                                          employeeId, 
                                                          settingId, 
                                                          type);

            return await _bindingRepository.InsertAsync(binding);
        }
        
        public async Task DeleteBinding(Guid id)
        {
            await _bindingRepository.DeleteAsync(binding => binding.Id == id);
        }
    }
}