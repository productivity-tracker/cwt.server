﻿using Volo.Abp.Modularity;

namespace ScreenRecordingAdministration
{
    [DependsOn(
        typeof(ScreenRecordingAdministrationDomainSharedModule)
        )]
    public class ScreenRecordingAdministrationDomainModule : AbpModule
    {

    }
}
