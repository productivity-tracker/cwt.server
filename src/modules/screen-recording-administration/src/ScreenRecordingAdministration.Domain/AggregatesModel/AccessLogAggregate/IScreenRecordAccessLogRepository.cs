﻿using Volo.Abp.Domain.Repositories;

namespace ScreenRecordingAdministration.AggregatesModel.AccessLogAggregate
{
    public interface IScreenRecordAccessLogRepository : IRepository<ScreenRecordAccessLog, long>
    {
        
    }
}