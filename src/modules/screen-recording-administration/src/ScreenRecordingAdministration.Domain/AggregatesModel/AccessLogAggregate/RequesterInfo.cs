﻿using System;

namespace ScreenRecordingAdministration.AggregatesModel.AccessLogAggregate
{
    public class RequesterInfo : IEquatable<RequesterInfo>
    {
        public string UserId { get; private set; }
        public string UserName { get; private set; }
        public string ClientIpAddress { get; private set; }
        public string ClientName { get; private set; }
        public string BrowserInfo { get; private set; }

        public RequesterInfo(string browserInfo,
                             string clientIpAddress,
                             string clientName,
                             string userId,
                             string userName)
        {
            BrowserInfo = browserInfo;
            ClientIpAddress = clientIpAddress;
            ClientName = clientName;
            UserId = userId;
            UserName = userName;
        }

        #region IEquatable Support

        public bool Equals(RequesterInfo other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return UserId == other.UserId && UserName == other.UserName && ClientIpAddress == other.ClientIpAddress && ClientName == other.ClientName
                && BrowserInfo == other.BrowserInfo;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((RequesterInfo)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = UserId != null ? UserId.GetHashCode() : 0;
                hashCode = (hashCode * 397) ^ (UserName != null ? UserName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (ClientIpAddress != null ? ClientIpAddress.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (ClientName != null ? ClientName.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (BrowserInfo != null ? BrowserInfo.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(RequesterInfo left, RequesterInfo right) => Equals(left, right);

        public static bool operator !=(RequesterInfo left, RequesterInfo right) => !Equals(left, right);

        #endregion
    }
}