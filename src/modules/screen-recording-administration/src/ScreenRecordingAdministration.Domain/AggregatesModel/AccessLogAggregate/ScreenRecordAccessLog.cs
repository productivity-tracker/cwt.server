﻿using System;
using Volo.Abp.Domain.Entities;

namespace ScreenRecordingAdministration.AggregatesModel.AccessLogAggregate
{
    public class ScreenRecordAccessLog : AggregateRoot<long>, IEquatable<ScreenRecordAccessLog>
    {
        public RequesterInfo Requester { get; private set; }
        public ScreenRecord RequestedScreenRecord { get; private set; }
        public string ScreenRecordSetAlias { get; private set; }
        public DateTime Timestamp { get; private set; }
        public Guid StorageId { get; private set; }

        public ScreenRecordAccessLog(ScreenRecord requestedScreenRecord,
                                     RequesterInfo requester,
                                     Guid storageId,
                                     DateTime timestamp,
                                     string screenRecordSetAlias)
            : base(DateTime.UtcNow.Ticks)
        {
            RequestedScreenRecord = requestedScreenRecord;
            Requester = requester;
            StorageId = storageId;
            Timestamp = timestamp;
            ScreenRecordSetAlias = screenRecordSetAlias;
        }

        #region IEquatable Support

        public bool Equals(ScreenRecordAccessLog other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(Requester, other.Requester) && Equals(RequestedScreenRecord, other.RequestedScreenRecord)
                                                      && ScreenRecordSetAlias == other.ScreenRecordSetAlias && Timestamp.Equals(other.Timestamp)
                                                      && StorageId.Equals(other.StorageId);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return EntityEquals(obj) && Equals((ScreenRecordAccessLog)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Requester != null ? Requester.GetHashCode() : 0;
                hashCode = (hashCode * 397) ^ (RequestedScreenRecord != null ? RequestedScreenRecord.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (ScreenRecordSetAlias != null ? ScreenRecordSetAlias.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Timestamp.GetHashCode();
                hashCode = (hashCode * 397) ^ StorageId.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(ScreenRecordAccessLog left, ScreenRecordAccessLog right) => Equals(left, right);

        public static bool operator !=(ScreenRecordAccessLog left, ScreenRecordAccessLog right) => !Equals(left, right);

        #endregion
    }
}