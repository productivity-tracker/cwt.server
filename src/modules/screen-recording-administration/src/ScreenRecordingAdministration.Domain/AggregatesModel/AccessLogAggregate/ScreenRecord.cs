﻿using System;
using CWT.Timing;

namespace ScreenRecordingAdministration.AggregatesModel.AccessLogAggregate
{
    public class ScreenRecord : IEquatable<ScreenRecord>
    {
        public string EmployeeId { get; private set; }
        public Date Date { get; private set; }
        public TimeSpan TimeOfDay { get; private set; }

        public ScreenRecord(string employeeId, DateTime requestedDateTime)
        {
            EmployeeId = employeeId;
            Date = requestedDateTime.Date;
            TimeOfDay = requestedDateTime.TimeOfDay;
        }

        #region IEquatable Support

        public bool Equals(ScreenRecord other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return EmployeeId == other.EmployeeId && Date.Equals(other.Date) && TimeOfDay.Equals(other.TimeOfDay);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ScreenRecord)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = EmployeeId != null ? EmployeeId.GetHashCode() : 0;
                hashCode = (hashCode * 397) ^ Date.GetHashCode();
                hashCode = (hashCode * 397) ^ TimeOfDay.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(ScreenRecord left, ScreenRecord right) => Equals(left, right);

        public static bool operator !=(ScreenRecord left, ScreenRecord right) => !Equals(left, right);

        #endregion
    }
}