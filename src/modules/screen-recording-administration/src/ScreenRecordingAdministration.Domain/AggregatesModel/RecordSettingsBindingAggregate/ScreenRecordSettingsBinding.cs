﻿using System;
using Volo.Abp.Domain.Entities.Auditing;

namespace ScreenRecordingAdministration.AggregatesModel.RecordSettingsBindingAggregate
{
    public class ScreenRecordSettingsBinding : AuditedAggregateRoot<Guid>, IEquatable<ScreenRecordSettingsBinding>
    {
        public Guid ScreenRecordingSettingsId { get; private set; }
        public BindingType Type { get; private set; }
        public Guid TargetId { get; private set; }

        protected ScreenRecordSettingsBinding()
        { }

        public ScreenRecordSettingsBinding(Guid id,
                                           Guid targetId,
                                           Guid screenRecordingSettingsId,
                                           BindingType type)
            : base(id)
        {
            TargetId = targetId;
            ScreenRecordingSettingsId = screenRecordingSettingsId;
            Type = type;
        }

        #region IEquatable Support

        public bool Equals(ScreenRecordSettingsBinding other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return ScreenRecordingSettingsId.Equals(other.ScreenRecordingSettingsId) && Type == other.Type &&
                   TargetId.Equals(other.TargetId);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return EntityEquals(obj) && Equals((ScreenRecordSettingsBinding)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = ScreenRecordingSettingsId.GetHashCode();
                hashCode = (hashCode * 397) ^ (int)Type;
                hashCode = (hashCode * 397) ^ TargetId.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(ScreenRecordSettingsBinding left, ScreenRecordSettingsBinding right) => Equals(left, right);

        public static bool operator !=(ScreenRecordSettingsBinding left, ScreenRecordSettingsBinding right) => !Equals(left, right);

        #endregion
    }
}