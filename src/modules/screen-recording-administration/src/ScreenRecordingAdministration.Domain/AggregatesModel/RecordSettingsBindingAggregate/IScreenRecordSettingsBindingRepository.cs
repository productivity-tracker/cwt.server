﻿using Volo.Abp.Domain.Repositories;

namespace ScreenRecordingAdministration.AggregatesModel.RecordSettingsBindingAggregate
{
    public interface IScreenRecordSettingsBindingRepository : IRepository<ScreenRecordSettingsBinding>
    {

    }
}