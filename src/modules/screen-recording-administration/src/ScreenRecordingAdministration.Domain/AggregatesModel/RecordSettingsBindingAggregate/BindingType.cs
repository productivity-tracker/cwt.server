﻿namespace ScreenRecordingAdministration.AggregatesModel.RecordSettingsBindingAggregate
{
    public enum BindingType
    {
        Group,
        Personal
    }
}