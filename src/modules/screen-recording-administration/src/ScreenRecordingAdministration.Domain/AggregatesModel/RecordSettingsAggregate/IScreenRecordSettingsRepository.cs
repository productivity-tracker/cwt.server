﻿using System;
using Volo.Abp.Domain.Repositories;

namespace ScreenRecordingAdministration.AggregatesModel.RecordSettingsAggregate
{
    public interface IScreenRecordSettingsRepository : IRepository<ScreenRecordSettings, Guid>
    {
        
    }
}