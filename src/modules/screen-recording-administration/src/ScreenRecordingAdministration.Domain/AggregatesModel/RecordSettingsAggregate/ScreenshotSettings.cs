﻿using System;
using ScreenRecording.Domain.Shared;

namespace ScreenRecordingAdministration.AggregatesModel.RecordSettingsAggregate
{
    public class ScreenshotSettings : IEquatable<ScreenshotSettings>
    {
        public ImageFormat ImageFormat { get; private set; }
        public long Quality { get; private set; }

        protected ScreenshotSettings()
        { }

        public ScreenshotSettings(ImageFormat imageFormat, long quality)
        {
            ImageFormat = imageFormat;
            Quality = quality;
        }

        #region IEquatable Support

        public bool Equals(ScreenshotSettings other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return ImageFormat == other.ImageFormat && Quality == other.Quality;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ScreenshotSettings)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = ImageFormat.GetHashCode();
                hashCode = (hashCode * 397) ^ Quality.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(ScreenshotSettings left, ScreenshotSettings right) => Equals(left, right);

        public static bool operator !=(ScreenshotSettings left, ScreenshotSettings right) => !Equals(left, right);

        #endregion
    }
}