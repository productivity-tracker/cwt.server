﻿using System;
using System.Collections.Generic;
using ScreenRecordingAdministration.ScreenRecordSettings;
using Volo.Abp.Domain.Entities.Auditing;

namespace ScreenRecordingAdministration.AggregatesModel.RecordSettingsAggregate
{
    public class ScreenRecordSettings : FullAuditedAggregateRoot<Guid>
    {
        public string Name { get; private set; }
        public bool IsActive { get; private set; }
        public bool IsDefault { get; private set; }
        public RecordMode RecordMode { get; private set; }
        public RecordType RecordType { get; private set; }
        public TimeSpan FrameFrequency { get; private set; }
        public ScreenshotSettings ScreenshotSettings { get; private set; }
        public ISet<string> ObservableProcesses { get; private set; }
        public ISet<string> ObservableWebSites { get; private set; }
        public TimeSpan ScreenRecordTTL { get; private set; }
        public Guid StorageAdapterId { get; private set; }

        protected ScreenRecordSettings(TimeSpan frameFrequency)
        {
            FrameFrequency = frameFrequency;
        }

        public ScreenRecordSettings(Guid id,
                                    bool isActive,
                                    string name,
                                    IEnumerable<string> observableProcesses,
                                    IEnumerable<string> observableWebSites,
                                    RecordType recordType,
                                    TimeSpan screenRecordTtl,
                                    ScreenshotSettings screenshotSettings,
                                    Guid storageAdapterId,
                                    RecordMode recordMode,
                                    TimeSpan frameFrequency)
            : base(id)
        {
            IsActive = isActive;
            Name = name;
            ObservableProcesses = new HashSet<string>(observableProcesses);
            ObservableWebSites = new HashSet<string>(observableWebSites);
            RecordType = recordType;
            ScreenRecordTTL = screenRecordTtl;
            ScreenshotSettings = screenshotSettings;
            StorageAdapterId = storageAdapterId;
            RecordMode = recordMode;
            FrameFrequency = frameFrequency;
        }

        public ScreenRecordSettings SetAsDefault()
        {
            IsDefault = true;
            return this;
        }
    }
}