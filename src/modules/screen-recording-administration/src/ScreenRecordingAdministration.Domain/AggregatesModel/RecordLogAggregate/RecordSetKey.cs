﻿using System;

namespace ScreenRecordingAdministration.AggregatesModel.RecordLogAggregate
{
    public class RecordSetKey : IEquatable<RecordSetKey>
    {
        public string Date { get; private set; }

        public Guid EmployeeId { get; private set; }

        protected RecordSetKey()
        { }

        public RecordSetKey(string date, Guid employeeId)
        {
            Date = date;
            EmployeeId = employeeId;
        }

        #region IEquatable Support

        public bool Equals(RecordSetKey other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Date == other.Date && EmployeeId.Equals(other.EmployeeId);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((RecordSetKey)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Date != null ? Date.GetHashCode() : 0) * 397) ^ EmployeeId.GetHashCode();
            }
        }

        public static bool operator ==(RecordSetKey left, RecordSetKey right) => Equals(left, right);

        public static bool operator !=(RecordSetKey left, RecordSetKey right) => !Equals(left, right);

        #endregion
    }
}