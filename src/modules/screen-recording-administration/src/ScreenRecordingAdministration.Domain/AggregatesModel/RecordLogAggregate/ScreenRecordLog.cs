﻿using System;
using System.Collections.Generic;
using System.Linq;
using Volo.Abp;
using Volo.Abp.Domain.Entities;

namespace ScreenRecordingAdministration.AggregatesModel.RecordLogAggregate
{
    public class ScreenRecordLog : AggregateRoot<Guid>, IEquatable<ScreenRecordLog>, ISoftDelete
    {
        public bool IsDeleted { get; set; }
        public RecordSetKey Key { get; private set; }
        public ScreenRecordingTrace[] RecordsRegistry { get; private set; }

        public ScreenRecordLog(Guid id, RecordSetKey key,
                               IEnumerable<ScreenRecordingTrace> records)
            : base(id)
        {
            Key = key;
            RecordsRegistry = new SortedSet<ScreenRecordingTrace>(records, ScreenRecordingTrace.IntervalComparer).ToArray();;
        }

        #region IEquatable Support

        public bool Equals(ScreenRecordLog other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return IsDeleted == other.IsDeleted && Equals(Key, other.Key) && Equals(RecordsRegistry, other.RecordsRegistry);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return EntityEquals(obj) && Equals((ScreenRecordLog)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = IsDeleted.GetHashCode();
                hashCode = (hashCode * 397) ^ (Key != null ? Key.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (RecordsRegistry != null ? RecordsRegistry.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(ScreenRecordLog left, ScreenRecordLog right) => Equals(left, right);

        public static bool operator !=(ScreenRecordLog left, ScreenRecordLog right) => !Equals(left, right);

        #endregion
    }
}