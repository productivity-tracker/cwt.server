﻿using System;

namespace ScreenRecordingAdministration.AggregatesModel.RecordLogAggregate
{
    public class RecordLocationDetails : IEquatable<RecordLocationDetails>
    {
        public Guid StorageId { get; private set; }
        public string BasePath { get; private set; }

        protected RecordLocationDetails()
        { }

        public RecordLocationDetails(Guid storageId, string basePath)
        {
            StorageId = storageId;
            BasePath = basePath;
        }

        #region IEquatable Support

        public bool Equals(RecordLocationDetails other)
        {
            if (ReferenceEquals(objA: null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(StorageId, other.StorageId) && string.Equals(BasePath, other.BasePath);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(objA: null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((RecordLocationDetails)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((StorageId != null ? StorageId.GetHashCode() : 0) * 397) ^ (BasePath != null ? BasePath.GetHashCode() : 0);
            }
        }

        public static bool operator ==(RecordLocationDetails left, RecordLocationDetails right) => Equals(left, right);

        public static bool operator !=(RecordLocationDetails left, RecordLocationDetails right) => !Equals(left, right);

        #endregion
    }
}