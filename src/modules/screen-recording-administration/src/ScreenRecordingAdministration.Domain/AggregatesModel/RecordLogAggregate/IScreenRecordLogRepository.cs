﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CWT.Timing;
using JetBrains.Annotations;
using Volo.Abp.Domain.Repositories;

namespace ScreenRecordingAdministration.AggregatesModel.RecordLogAggregate
{
    public interface IScreenRecordLogRepository : IReadOnlyBasicRepository<ScreenRecordLog, Guid>
    {
        Task<IList<ScreenRecordLog>> GetAsync(DateTimeRange range, CancellationToken cancellationToken = default);

        Task<ScreenRecordLog> FindByKeyAsync(RecordSetKey setKey, CancellationToken cancellationToken = default);

        Task BulkUpdateLastIntervalsAsync(IDictionary<RecordSetKey, DateTimeRange> updatedIntervals, CancellationToken cancellationToken = default);
        Task BulkInsertScreenRecordsAsync(IDictionary<RecordSetKey, ScreenRecordingTrace> newIntervals, CancellationToken cancellationToken = default);
        Task BulkInsertAsync([NotNull]IEnumerable<ScreenRecordLog> histories, CancellationToken cancellationToken = default);
    }
}