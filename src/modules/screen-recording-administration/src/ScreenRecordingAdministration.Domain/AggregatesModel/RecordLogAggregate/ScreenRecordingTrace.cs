﻿using System;
using System.Collections.Generic;
using CWT.Timing;
using ScreenRecordingAdministration.ScreenRecordLogging;
using ScreenRecordingAdministration.ScreenRecordSettings;
using Volo.Abp.Domain.Entities;

namespace ScreenRecordingAdministration.AggregatesModel.RecordLogAggregate
{
    public class ScreenRecordingTrace : Entity<Guid>, IEquatable<ScreenRecordingTrace>
    {
        private class OptimisticIntervalComparer : IComparer<ScreenRecordingTrace>
        {
            public int Compare(ScreenRecordingTrace x, ScreenRecordingTrace y)
            {
                var comparer = new DateTimeRangeComparer();
                return comparer.Compare(x?.Interval, y?.Interval);
            }
        }

        internal static IComparer<ScreenRecordingTrace> IntervalComparer => new OptimisticIntervalComparer();

        public DateTimeRange Interval { get; private set; }
        public RecordType Type { get; private set; }
        public RecordLocationDetails RecordLocation { get; private set; }

        protected ScreenRecordingTrace()
        { }

        public ScreenRecordingTrace(DateTimeRange interval, RecordType type, RecordLocationDetails recordLocation)
        {
            Interval = interval;
            Type = type;
            RecordLocation = recordLocation;
        }

        public ScreenRecordingTrace UpdateInterval(DateTime endTime)
        {
            Interval.EndTime = endTime;
            return this;
        }

        #region IEquatable Support

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((ScreenRecordingTrace)obj);
        }

        public bool Equals(ScreenRecordingTrace other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(Interval, other.Interval) && Type == other.Type && Equals(RecordLocation, other.RecordLocation);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Interval != null ? Interval.GetHashCode() : 0;
                hashCode = (hashCode * 397) ^ (int)Type;
                hashCode = (hashCode * 397) ^ (RecordLocation != null ? RecordLocation.GetHashCode() : 0);
                return hashCode;
            }
        }

        #endregion
    }
}