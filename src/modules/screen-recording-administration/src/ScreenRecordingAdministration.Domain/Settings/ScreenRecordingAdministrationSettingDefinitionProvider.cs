﻿using Volo.Abp.Settings;

namespace ScreenRecordingAdministration.Settings
{
    public class ScreenRecordingAdministrationSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            /* Define module settings here.
             * Use names from ScreenRecordingAdministrationSettings class.
             */
        }
    }
}