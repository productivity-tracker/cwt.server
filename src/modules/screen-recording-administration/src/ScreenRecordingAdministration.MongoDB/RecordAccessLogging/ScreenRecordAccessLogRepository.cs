﻿using MongoDB.Driver;
using ScreenRecordingAdministration.AggregatesModel.AccessLogAggregate;
using ScreenRecordingAdministration.MongoDB;
using Volo.Abp.Domain.Repositories.MongoDB;
using Volo.Abp.MongoDB;

namespace ScreenRecordingAdministration.RecordAccessLogging
{
    public class ScreenRecordAccessLogRepository
        : MongoDbRepository<IScreenRecordingAdministrationMongoDbContext, ScreenRecordAccessLog, long>, IScreenRecordAccessLogRepository
    {
        public ScreenRecordAccessLogRepository(IMongoDbContextProvider<IScreenRecordingAdministrationMongoDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
            EnsureHashedIndexOnUserId(Collection);
            EnsureHashedIndexOnStorageId(Collection);
            EnsureHashedIndexOnRecordSetToken(Collection);
            EnsureCompoundIndexOnRequestedDateAndEmployeeId(Collection);
            EnsureHashedIndexOnRequesterIP(Collection);
        }

        private void EnsureHashedIndexOnUserId(IMongoCollection<ScreenRecordAccessLog> collection)
        {
            var model = new CreateIndexModel<ScreenRecordAccessLog>(
                Builders<ScreenRecordAccessLog>.IndexKeys.Hashed(x => x.Requester.UserId),
                new CreateIndexOptions { Background = true });
            collection.Indexes.CreateOneAsync(model);
        }

        private void EnsureHashedIndexOnRequesterIP(IMongoCollection<ScreenRecordAccessLog> collection)
        {
            var model = new CreateIndexModel<ScreenRecordAccessLog>(
                Builders<ScreenRecordAccessLog>.IndexKeys.Hashed(x => x.Requester.ClientIpAddress),
                new CreateIndexOptions { Background = true });
            collection.Indexes.CreateOneAsync(model);
        }

        private void EnsureHashedIndexOnStorageId(IMongoCollection<ScreenRecordAccessLog> collection)
        {
            var model = new CreateIndexModel<ScreenRecordAccessLog>(
                Builders<ScreenRecordAccessLog>.IndexKeys.Hashed(x => x.StorageId),
                new CreateIndexOptions { Background = true });
            collection.Indexes.CreateOneAsync(model);
        }

        private void EnsureHashedIndexOnRecordSetToken(IMongoCollection<ScreenRecordAccessLog> collection)
        {
            var model = new CreateIndexModel<ScreenRecordAccessLog>(
                Builders<ScreenRecordAccessLog>.IndexKeys.Hashed(x => x.ScreenRecordSetAlias),
                new CreateIndexOptions { Background = true });
            collection.Indexes.CreateOneAsync(model);
        }
        
        private void EnsureCompoundIndexOnRequestedDateAndEmployeeId(IMongoCollection<ScreenRecordAccessLog> collection)
        {
            var model = new CreateIndexModel<ScreenRecordAccessLog>(
                Builders<ScreenRecordAccessLog>.IndexKeys.Combine(
                    Builders<ScreenRecordAccessLog>.IndexKeys.Ascending(x => x.RequestedScreenRecord.Date),
                    Builders<ScreenRecordAccessLog>.IndexKeys.Ascending(x => x.RequestedScreenRecord.EmployeeId),
                    Builders<ScreenRecordAccessLog>.IndexKeys.Ascending(x => x.RequestedScreenRecord.TimeOfDay)),
                new CreateIndexOptions { Background = true });
            collection.Indexes.CreateOneAsync(model);
        }
    }
}