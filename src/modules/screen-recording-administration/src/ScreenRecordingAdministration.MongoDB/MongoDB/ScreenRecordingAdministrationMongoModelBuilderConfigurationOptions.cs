﻿using JetBrains.Annotations;
using Volo.Abp.MongoDB;

namespace ScreenRecordingAdministration.MongoDB
{
    public class ScreenRecordingAdministrationMongoModelBuilderConfigurationOptions : AbpMongoModelBuilderConfigurationOptions
    {
        public ScreenRecordingAdministrationMongoModelBuilderConfigurationOptions(
            [NotNull] string collectionPrefix = "")
            : base(collectionPrefix)
        {
        }
    }
}