﻿using Microsoft.Extensions.DependencyInjection;
using ScreenRecordingAdministration.AggregatesModel.RecordLogAggregate;
using ScreenRecordingAdministration.RecordLogging;
using ScreenRecordingAdministration.RecordSettings;
using ScreenRecordingAdministration.ScreenRecordLogging;
using ScreenRecordingAdministration.ScreenRecordSettings;
using Volo.Abp.Modularity;
using Volo.Abp.MongoDB;
using ScreenRecordSettingsAggregate = ScreenRecordingAdministration.AggregatesModel.RecordSettingsAggregate.ScreenRecordSettings;

namespace ScreenRecordingAdministration.MongoDB
{
    [DependsOn(
        typeof(ScreenRecordingAdministrationDomainModule),
        typeof(AbpMongoDbModule)
        )]
    public class ScreenRecordingAdministrationMongoDbModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddMongoDbContext<ScreenRecordingAdministrationMongoDbContext>(options =>
            {
                 options.AddRepository<ScreenRecordLog, ScreenRecordLogRepository>();
                 options.AddRepository<ScreenRecordSettingsAggregate, ScreenRecordSettingsRepository>();
            });
        }
    }
}
