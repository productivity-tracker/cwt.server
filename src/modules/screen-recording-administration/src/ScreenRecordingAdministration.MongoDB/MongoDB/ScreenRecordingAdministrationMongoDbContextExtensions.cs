﻿using System;
using Volo.Abp;
using Volo.Abp.MongoDB;

namespace ScreenRecordingAdministration.MongoDB
{
    public static class ScreenRecordingAdministrationMongoDbContextExtensions
    {
        public static void ConfigureScreenRecordingAdministration(
            this IMongoModelBuilder builder,
            Action<AbpMongoModelBuilderConfigurationOptions> optionsAction = null)
        {
            Check.NotNull(builder, nameof(builder));

            var options = new ScreenRecordingAdministrationMongoModelBuilderConfigurationOptions(
                ScreenRecordingAdministrationDbProperties.DbTablePrefix
            );

            optionsAction?.Invoke(options);
        }
    }
}