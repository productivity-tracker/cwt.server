﻿using MongoDB.Driver;
using ScreenRecordingAdministration.AggregatesModel.AccessLogAggregate;
using ScreenRecordingAdministration.AggregatesModel.RecordLogAggregate;
using ScreenRecordingAdministration.AggregatesModel.RecordSettingsBindingAggregate;
using Volo.Abp.Data;
using Volo.Abp.MongoDB;
using ScreenRecordSettingsAggregate = ScreenRecordingAdministration.AggregatesModel.RecordSettingsAggregate.ScreenRecordSettings;

namespace ScreenRecordingAdministration.MongoDB
{
    [ConnectionStringName(ScreenRecordingAdministrationDbProperties.ConnectionStringName)]
    public class ScreenRecordingAdministrationMongoDbContext : AbpMongoDbContext, IScreenRecordingAdministrationMongoDbContext
    {
        public IMongoCollection<ScreenRecordLog> RecordLog => Collection<ScreenRecordLog>();

        public IMongoCollection<ScreenRecordAccessLog> RecordAccessLog => Collection<ScreenRecordAccessLog>();

        public IMongoCollection<ScreenRecordSettingsAggregate> RecordSettings => Collection<ScreenRecordSettingsAggregate>();

        public IMongoCollection<ScreenRecordSettingsBinding> RecordSettingsBindings => Collection<ScreenRecordSettingsBinding>();

        protected override void CreateModel(IMongoModelBuilder modelBuilder)
        {
            base.CreateModel(modelBuilder);

            modelBuilder.ConfigureScreenRecordingAdministration();
        }
    }
}