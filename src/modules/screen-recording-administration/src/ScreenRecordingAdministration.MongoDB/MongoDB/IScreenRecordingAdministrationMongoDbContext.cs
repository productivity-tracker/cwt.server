﻿using MongoDB.Driver;
using ScreenRecordingAdministration.AggregatesModel.AccessLogAggregate;
using ScreenRecordingAdministration.AggregatesModel.RecordLogAggregate;
using ScreenRecordingAdministration.AggregatesModel.RecordSettingsBindingAggregate;
using Volo.Abp.Data;
using Volo.Abp.MongoDB;
using ScreenRecordSettingsAggregate = ScreenRecordingAdministration.AggregatesModel.RecordSettingsAggregate.ScreenRecordSettings;

namespace ScreenRecordingAdministration.MongoDB
{
    [ConnectionStringName(ScreenRecordingAdministrationDbProperties.ConnectionStringName)]
    public interface IScreenRecordingAdministrationMongoDbContext : IAbpMongoDbContext
    {
        [MongoCollection(nameof(RecordLog))]
        IMongoCollection<ScreenRecordLog> RecordLog { get; }

        [MongoCollection(nameof(RecordAccessLog))]
        IMongoCollection<ScreenRecordAccessLog> RecordAccessLog { get; }

        [MongoCollection(nameof(RecordSettings))]
        IMongoCollection<ScreenRecordSettingsAggregate> RecordSettings { get; }

        [MongoCollection(nameof(RecordSettingsBindings))]
        IMongoCollection<ScreenRecordSettingsBinding> RecordSettingsBindings { get; }
    }
}