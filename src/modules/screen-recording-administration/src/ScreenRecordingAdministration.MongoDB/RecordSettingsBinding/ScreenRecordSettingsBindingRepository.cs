﻿using System;
using MongoDB.Driver;
using ScreenRecordingAdministration.AggregatesModel.RecordSettingsBindingAggregate;
using ScreenRecordingAdministration.MongoDB;
using Volo.Abp.Domain.Repositories.MongoDB;
using Volo.Abp.MongoDB;

namespace ScreenRecordingAdministration.RecordSettingsBinding
{
    public class ScreenRecordSettingsBindingRepository 
        : MongoDbRepository<IScreenRecordingAdministrationMongoDbContext, ScreenRecordSettingsBinding, Guid>
    {
        public ScreenRecordSettingsBindingRepository(IMongoDbContextProvider<IScreenRecordingAdministrationMongoDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
            EnsureHashedIndexOnSettingsId(Collection);
            EnsureCompoundUniqueIndexOnTypeAndTargetId(Collection);
        }

        private void EnsureHashedIndexOnSettingsId(IMongoCollection<ScreenRecordSettingsBinding> collection)
        {
            var model = new CreateIndexModel<ScreenRecordSettingsBinding>(
                Builders<ScreenRecordSettingsBinding>.IndexKeys.Hashed(x => x.ScreenRecordingSettingsId),
                new CreateIndexOptions { Background = true });
            collection.Indexes.CreateOneAsync(model);
        }

        private void EnsureCompoundUniqueIndexOnTypeAndTargetId(IMongoCollection<ScreenRecordSettingsBinding> collection)
        {
            var model = new CreateIndexModel<ScreenRecordSettingsBinding>(
                Builders<ScreenRecordSettingsBinding>.IndexKeys.Combine(
                    Builders<ScreenRecordSettingsBinding>.IndexKeys.Ascending(x => x.Type),
                    Builders<ScreenRecordSettingsBinding>.IndexKeys.Ascending(x => x.TargetId)),
                new CreateIndexOptions { Background = true, Unique = true });
            collection.Indexes.CreateOneAsync(model);
        }
    }
}