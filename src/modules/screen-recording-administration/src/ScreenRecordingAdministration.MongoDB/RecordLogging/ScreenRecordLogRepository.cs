﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CWT.Timing;
using MongoDB.Driver;
using ScreenRecordingAdministration.AggregatesModel.RecordLogAggregate;
using ScreenRecordingAdministration.MongoDB;
using Volo.Abp.Domain.Repositories.MongoDB;
using Volo.Abp.MongoDB;

namespace ScreenRecordingAdministration.RecordLogging
{
    internal class ScreenRecordLogRepository : MongoDbRepository<IScreenRecordingAdministrationMongoDbContext, ScreenRecordLog, Guid>,
                                               IScreenRecordLogRepository
    {
        internal ScreenRecordLogRepository(IMongoDbContextProvider<IScreenRecordingAdministrationMongoDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
            EnsureCompoundIndexOnKey(Collection);
        }

        public async Task<IList<ScreenRecordLog>> GetAsync(DateTimeRange range, CancellationToken cancellationToken = default)
        {
            var allDates = range.GetDates();
            var filter = new FilterDefinitionBuilder<ScreenRecordLog>().In(e => e.Key.Date, allDates.Select(d => d.ToString()));
            var result = await Collection.FindAsync(filter, cancellationToken: cancellationToken);
            return await result.ToListAsync(cancellationToken: cancellationToken);
        }

        public async Task<ScreenRecordLog> FindByKeyAsync(RecordSetKey setKey, CancellationToken cancellationToken = default)
        {
            var filter = GetKeyFilterDefinition(setKey);
            var result = await Collection.FindAsync(filter, cancellationToken: cancellationToken);
            return await result.SingleOrDefaultAsync(cancellationToken: cancellationToken);
        }

        public async Task BulkUpdateLastIntervalsAsync(IDictionary<RecordSetKey, DateTimeRange> updatedIntervals,
                                                       CancellationToken cancellationToken = default)
        {
            var requests = (
                from intervalPair in updatedIntervals
                let filter = GetKeyFilterDefinition(intervalPair.Key)
                let update = Builders<ScreenRecordLog>.Update.Set(x => x.RecordsRegistry[-1].Interval, intervalPair.Value)
                select new UpdateOneModel<ScreenRecordLog>(filter, update)).Cast<WriteModel<ScreenRecordLog>>();

            await Collection.BulkWriteAsync(requests, cancellationToken: cancellationToken).ConfigureAwait(false);
        }

        public async Task BulkInsertScreenRecordsAsync(IDictionary<RecordSetKey, ScreenRecordingTrace> newIntervals,
                                                       CancellationToken cancellationToken = default)
        {
            var requests = (
                from intervalPair in newIntervals
                let filter = GetKeyFilterDefinition(intervalPair.Key)
                let update = Builders<ScreenRecordLog>.Update.Push(x => x.RecordsRegistry, intervalPair.Value)
                select new UpdateOneModel<ScreenRecordLog>(filter, update)).Cast<WriteModel<ScreenRecordLog>>();

            await Collection.BulkWriteAsync(requests, cancellationToken: cancellationToken).ConfigureAwait(false);
        }

        public async Task BulkInsertAsync(IEnumerable<ScreenRecordLog> histories, CancellationToken cancellationToken = default)
        {
            var requests = histories.Select(history => new InsertOneModel<ScreenRecordLog>(history));
            await Collection.BulkWriteAsync(requests, cancellationToken: cancellationToken).ConfigureAwait(false);
        }

        private void EnsureCompoundIndexOnKey(IMongoCollection<ScreenRecordLog> collection)
        {
            var model = new CreateIndexModel<ScreenRecordLog>(
                Builders<ScreenRecordLog>.IndexKeys.Combine(Builders<ScreenRecordLog>.IndexKeys.Ascending(x => x.Key.EmployeeId),
                                                            Builders<ScreenRecordLog>.IndexKeys.Ascending(x => x.Key.Date)),
                new CreateIndexOptions { Background = true });
            collection.Indexes.CreateOneAsync(model);
        }

        private FilterDefinition<ScreenRecordLog> GetKeyFilterDefinition(RecordSetKey setKey)
            => Builders<ScreenRecordLog>.Filter.Where(x => x.Key.EmployeeId == setKey.EmployeeId && x.Key.Date == setKey.Date);
    }
}