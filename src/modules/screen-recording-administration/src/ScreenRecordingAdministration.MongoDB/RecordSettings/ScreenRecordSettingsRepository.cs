﻿using System;
using MongoDB.Driver;
using ScreenRecordingAdministration.AggregatesModel.RecordSettingsAggregate;
using ScreenRecordingAdministration.MongoDB;
using Volo.Abp.Domain.Repositories.MongoDB;
using Volo.Abp.MongoDB;
using ScreenRecordSettingsAggregate = ScreenRecordingAdministration.AggregatesModel.RecordSettingsAggregate.ScreenRecordSettings;

namespace ScreenRecordingAdministration.RecordSettings
{
    public class ScreenRecordSettingsRepository
        : MongoDbRepository<IScreenRecordingAdministrationMongoDbContext, ScreenRecordSettingsAggregate, Guid>, IScreenRecordSettingsRepository
    {
        public ScreenRecordSettingsRepository(IMongoDbContextProvider<IScreenRecordingAdministrationMongoDbContext> dbContextProvider)
            : base(dbContextProvider)
        {
            EnsureHashedIndexOnDefaultField(Collection);
        }

        private void EnsureHashedIndexOnDefaultField(IMongoCollection<ScreenRecordSettingsAggregate> collection)
        {
            var model = new CreateIndexModel<ScreenRecordSettingsAggregate>(
                Builders<ScreenRecordSettingsAggregate>.IndexKeys.Hashed(x => x.IsDefault),
                new CreateIndexOptions { Background = true });
            collection.Indexes.CreateOneAsync(model);
        }
    }
}