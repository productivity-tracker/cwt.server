﻿using Localization.Resources.AbpUi;
using ScreenRecordingAdministration.Localization;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Microsoft.Extensions.DependencyInjection;

namespace ScreenRecordingAdministration
{
    [DependsOn(
        typeof(ScreenRecordingAdministrationApplicationContractsModule),
        typeof(AbpAspNetCoreMvcModule))]
    public class ScreenRecordingAdministrationHttpApiModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            PreConfigure<IMvcBuilder>(mvcBuilder =>
            {
                mvcBuilder.AddApplicationPartIfNotExists(typeof(ScreenRecordingAdministrationHttpApiModule).Assembly);
            });
        }
    }
}
