﻿using Volo.Abp.Http.Client.IdentityModel;
using Volo.Abp.Modularity;

namespace ScreenRecordingAdministration
{
    [DependsOn(
        typeof(ScreenRecordingAdministrationHttpApiClientModule),
        typeof(AbpHttpClientIdentityModelModule)
        )]
    public class ScreenRecordingAdministrationConsoleApiClientModule : AbpModule
    {
        
    }
}
