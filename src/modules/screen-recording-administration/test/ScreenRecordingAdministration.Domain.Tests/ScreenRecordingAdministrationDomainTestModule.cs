﻿using ScreenRecordingAdministration.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace ScreenRecordingAdministration
{
    /* Domain tests are configured to use the EF Core provider.
     * You can switch to MongoDB, however your domain tests should be
     * database independent anyway.
     */
    [DependsOn(
        typeof(ScreenRecordingAdministrationEntityFrameworkCoreTestModule)
        )]
    public class ScreenRecordingAdministrationDomainTestModule : AbpModule
    {
        
    }
}
