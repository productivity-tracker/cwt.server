﻿using ScreenRecordingAdministration.Samples;

namespace ScreenRecordingAdministration.MongoDB.Samples
{
    public class SampleRepository_Tests : SampleRepository_Tests<ScreenRecordingAdministrationMongoDbTestModule>
    {
        /* Don't write custom repository tests here, instead write to
         * the base class.
         * One exception can be some specific tests related to MongoDB.
         */
    }
}
