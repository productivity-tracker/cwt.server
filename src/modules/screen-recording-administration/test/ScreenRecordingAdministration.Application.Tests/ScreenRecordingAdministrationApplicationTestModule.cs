﻿using Volo.Abp.Modularity;

namespace ScreenRecordingAdministration
{
    [DependsOn(
        typeof(ScreenRecordingAdministrationApplicationModule),
        typeof(ScreenRecordingAdministrationDomainTestModule)
        )]
    public class ScreenRecordingAdministrationApplicationTestModule : AbpModule
    {

    }
}
