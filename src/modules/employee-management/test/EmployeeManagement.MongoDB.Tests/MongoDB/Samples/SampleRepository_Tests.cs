﻿using EmployeeManagement.Samples;

namespace EmployeeManagement.MongoDB.Samples
{
    public class SampleRepository_Tests : SampleRepository_Tests<EmployeeManagementMongoDbTestModule>
    {
        /* Don't write custom repository tests here, instead write to
         * the base class.
         * One exception can be some specific tests related to MongoDB.
         */
    }
}
