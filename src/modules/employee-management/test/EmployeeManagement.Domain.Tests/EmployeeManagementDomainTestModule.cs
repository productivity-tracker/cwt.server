﻿using EmployeeManagement.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace EmployeeManagement
{
    /* Domain tests are configured to use the EF Core provider.
     * You can switch to MongoDB, however your domain tests should be
     * database independent anyway.
     */
    [DependsOn(
        typeof(EmployeeManagementEntityFrameworkCoreTestModule)
        )]
    public class EmployeeManagementDomainTestModule : AbpModule
    {
        
    }
}
