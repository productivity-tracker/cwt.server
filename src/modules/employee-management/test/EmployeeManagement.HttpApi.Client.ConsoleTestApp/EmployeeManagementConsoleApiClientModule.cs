﻿using Volo.Abp.Http.Client.IdentityModel;
using Volo.Abp.Modularity;

namespace EmployeeManagement
{
    [DependsOn(
        typeof(EmployeeManagementHttpApiClientModule),
        typeof(AbpHttpClientIdentityModelModule)
        )]
    public class EmployeeManagementConsoleApiClientModule : AbpModule
    {
        
    }
}
