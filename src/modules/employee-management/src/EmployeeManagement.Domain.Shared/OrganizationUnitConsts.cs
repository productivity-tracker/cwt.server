﻿namespace EmployeeManagement
{
    public static class OrganizationUnitConsts
    {
        /// <summary>
        /// Maximum length of the <see cref="DisplayName" /> property.
        /// </summary>
        public const int MAX_DISPLAY_NAME_LENGTH = 128;

        /// <summary>
        /// Maximum depth of an UO hierarchy.
        /// </summary>
        public const int MAX_DEPTH = 16;

        /// <summary>
        /// Length of a code unit between dots.
        /// </summary>
        public const int CODE_UNIT_LENGTH = 5;

        /// <summary>
        /// Maximum length of the <see cref="Code" /> property.
        /// </summary>
        public const int MAX_CODE_LENGTH = MAX_DEPTH * (CODE_UNIT_LENGTH + 1) - 1;
    }
}