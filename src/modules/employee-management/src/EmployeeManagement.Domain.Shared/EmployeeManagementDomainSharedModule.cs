﻿using Volo.Abp.Modularity;
using Volo.Abp.Localization;

namespace EmployeeManagement
{
    [DependsOn(
        typeof(AbpLocalizationModule)
    )]
    public class EmployeeManagementDomainSharedModule : AbpModule
    {
    }
}
