﻿using System.Threading.Tasks;
using EmployeeManagement.OrganizationUnits.DTO;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace EmployeeManagement.OrganizationUnits
{
    public interface IOrganizationUnitService : IApplicationService
    {
        Task CreateAsync(CreateOuRequest request);
        Task AddSubAsync(CreateSubOuRequest request);
        Task DeleteAsync(string organizationUnitId);
        Task<PagedResultDto<OrganizationUnitDto>> GetAllAsync();
    }
}