﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Volo.Abp.Application.Dtos;

namespace EmployeeManagement.OrganizationUnits.DTO
{
    [Serializable]
    public class OrganizationUnitDto : FullAuditedEntityDto<string>
    {
        ///// <summary>
        ///// Hierarchical Code of this organization unit.
        ///// Example: "00001.00042.00005".
        ///// This is a unique code for a Tenant.
        ///// It's changeable if OU hierarch is changed.
        ///// </summary>
        //[Required]
        //[StringLength(OrganizationUnitConsts.MAX_CODE_LENGTH)]
        //public string Code { get; set; }

        /// <summary>
        /// Display name of this OU.
        /// </summary>
        public string DisplayName { get; set; }

        public ISet<OrganizationUnitDto> Children { get; set; }

        public Guid? ScreenRecordingSettingsId { get; set; }
    }
}