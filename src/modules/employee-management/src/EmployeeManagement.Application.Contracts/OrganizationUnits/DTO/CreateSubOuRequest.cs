﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EmployeeManagement.OrganizationUnits.DTO
{
    [Serializable]
    public class CreateSubOuRequest
    {
        [Required]
        public string ParentOuId { get; set; }

        /// <summary>
        /// Display name of new OU.
        /// </summary>
        [Required]
        [StringLength(OrganizationUnitConsts.MAX_DISPLAY_NAME_LENGTH)]
        public string DisplayName { get; set; }
    }
}