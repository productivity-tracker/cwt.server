﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EmployeeManagement.OrganizationUnits.DTO
{
    [Serializable]
    public class CreateOuRequest
    {
        /// <summary>
        /// Display name of new OU.
        /// </summary>
        [Required]
        [StringLength(OrganizationUnitConsts.MAX_DISPLAY_NAME_LENGTH)]
        public string DisplayName { get; set; }
    }
}