﻿using System;
using System.Collections.Generic;
using Volo.Abp.Application.Dtos;

namespace EmployeeManagement.Employees.DTO
{
    [Serializable]
    public class EmployeeDto : FullAuditedEntityDto<Guid>
    {
        public string DisplayName { get; set; }
        public string JobTitle { get; set; }
        public DateTime? LastLoggedOnDate { get; set; }
        public Guid? ScreenRecordingSettingsId { get; set; }
        public ISet<string> OrganizationUnitsIds { get; set; }
        public string Notes { get; set; }
    }
}