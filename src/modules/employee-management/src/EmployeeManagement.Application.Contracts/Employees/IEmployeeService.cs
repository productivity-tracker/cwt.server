﻿using System;
using EmployeeManagement.Employees.DTO;
using Volo.Abp.Application.Services;

namespace EmployeeManagement.Employees
{
    public interface IEmployeeService : ICrudAppService<EmployeeDto, Guid>
    {
        
    }
}