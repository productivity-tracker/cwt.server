﻿using Volo.Abp.Application;
using Volo.Abp.Modularity;
using Volo.Abp.Authorization;

namespace EmployeeManagement
{
    [DependsOn(
        typeof(EmployeeManagementDomainSharedModule),
        typeof(AbpDddApplicationContractsModule),
        typeof(AbpAuthorizationModule))]
    public class EmployeeManagementApplicationContractsModule : AbpModule
    {
    }
}
