﻿using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Modularity;
using Microsoft.Extensions.DependencyInjection;

namespace EmployeeManagement
{
    [DependsOn(
        typeof(EmployeeManagementApplicationContractsModule),
        typeof(AbpAspNetCoreMvcModule))]
    public class EmployeeManagementHttpApiModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            PreConfigure<IMvcBuilder>(mvcBuilder =>
            {
                mvcBuilder.AddApplicationPartIfNotExists(typeof(EmployeeManagementHttpApiModule).Assembly);
            });
        }
    }
}
