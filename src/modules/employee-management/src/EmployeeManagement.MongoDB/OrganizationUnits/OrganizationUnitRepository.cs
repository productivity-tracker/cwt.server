﻿using System;
using EmployeeManagement.AggregatesModel.EmployeeAggregate;
using EmployeeManagement.AggregatesModel.OrganizationUnitAggregate;
using EmployeeManagement.MongoDB;
using Volo.Abp.Domain.Repositories.MongoDB;
using Volo.Abp.MongoDB;

namespace EmployeeManagement.OrganizationUnits
{
    public class OrganizationUnitRepository : MongoDbRepository<IEmployeeManagementMongoDbContext, OrganizationUnit, string>, IOrganizationUnitRepository
    {
        public OrganizationUnitRepository(IMongoDbContextProvider<IEmployeeManagementMongoDbContext> dbContextProvider)
            : base(dbContextProvider)
        { }
    }
}