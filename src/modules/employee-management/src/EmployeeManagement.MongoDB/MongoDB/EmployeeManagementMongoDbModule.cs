﻿using EmployeeManagement.AggregatesModel.EmployeeAggregate;
using EmployeeManagement.AggregatesModel.OrganizationUnitAggregate;
using EmployeeManagement.Employees;
using EmployeeManagement.OrganizationUnits;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;
using Volo.Abp.MongoDB;

namespace EmployeeManagement.MongoDB
{
    [DependsOn(
        typeof(EmployeeManagementDomainModule),
        typeof(AbpMongoDbModule)
        )]
    public class EmployeeManagementMongoDbModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddMongoDbContext<EmployeeManagementMongoDbContext>(options =>
            {
                options.AddRepository<Employee, EmployeeRepository>();
                options.AddRepository<OrganizationUnit, OrganizationUnitRepository>();
            });
        }
    }
}
