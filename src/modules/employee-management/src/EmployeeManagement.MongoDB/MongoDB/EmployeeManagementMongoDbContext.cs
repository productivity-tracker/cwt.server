﻿using EmployeeManagement.AggregatesModel.EmployeeAggregate;
using EmployeeManagement.AggregatesModel.OrganizationUnitAggregate;
using MongoDB.Driver;
using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace EmployeeManagement.MongoDB
{
    [ConnectionStringName(EmployeeManagementDbProperties.ConnectionStringName)]
    public class EmployeeManagementMongoDbContext : AbpMongoDbContext, IEmployeeManagementMongoDbContext
    {
        [MongoCollection(nameof(Employees))]
        public IMongoCollection<Employee> Employees => Collection<Employee>();

        [MongoCollection(nameof(OrganizationUnits))]
        public IMongoCollection<OrganizationUnit> OrganizationUnits => Collection<OrganizationUnit>();
    }
}