﻿using EmployeeManagement.AggregatesModel.EmployeeAggregate;
using EmployeeManagement.AggregatesModel.OrganizationUnitAggregate;
using MongoDB.Driver;
using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace EmployeeManagement.MongoDB
{
    [ConnectionStringName(EmployeeManagementDbProperties.ConnectionStringName)]
    public interface IEmployeeManagementMongoDbContext : IAbpMongoDbContext
    {
        IMongoCollection<Employee> Employees { get; }
        IMongoCollection<OrganizationUnit> OrganizationUnits { get; }
    }
}
