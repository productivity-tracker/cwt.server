﻿using System;
using EmployeeManagement.AggregatesModel.EmployeeAggregate;
using EmployeeManagement.MongoDB;
using Volo.Abp.Domain.Repositories.MongoDB;
using Volo.Abp.MongoDB;

namespace EmployeeManagement.Employees
{
    public class EmployeeRepository : MongoDbRepository<IEmployeeManagementMongoDbContext, Employee, Guid>, IEmployeeRepository
    {
        public EmployeeRepository(IMongoDbContextProvider<IEmployeeManagementMongoDbContext> dbContextProvider)
            : base(dbContextProvider)
        { }
    }
}