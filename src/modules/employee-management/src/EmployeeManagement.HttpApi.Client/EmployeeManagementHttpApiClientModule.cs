﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;

namespace EmployeeManagement
{
    [DependsOn(
        typeof(EmployeeManagementApplicationContractsModule),
        typeof(AbpHttpClientModule))]
    public class EmployeeManagementHttpApiClientModule : AbpModule
    {
        public const string RemoteServiceName = "EmployeeManagement";

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddHttpClientProxies(
                typeof(EmployeeManagementApplicationContractsModule).Assembly,
                RemoteServiceName
            );
        }
    }
}
