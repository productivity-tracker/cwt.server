﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeeManagement.AggregatesModel.OrganizationUnitAggregate;
using Volo.Abp.Domain.Entities;
using Volo.Abp.Domain.Services;
using Volo.Abp.Guids;

namespace EmployeeManagement.Services.Organizations
{
    public class OrganizationUnitManager : IDomainService
    {
        private const char ID_SEPARATOR = '.';

        private readonly IOrganizationUnitRepository _repository;
        private readonly IGuidGenerator _guidGenerator;

        public OrganizationUnitManager(IGuidGenerator guidGenerator,
                                       IOrganizationUnitRepository repository)
        {
            _repository = repository;
            _guidGenerator = guidGenerator;
        }

        public async Task<OrganizationUnit> CreateRootAsync(string name)
        {
            var ou = new OrganizationUnit(_guidGenerator.Create().ToString(), name, new List<OrganizationUnit>());
            return await _repository.UpdateAsync(ou);
        }

        /// <summary>
        /// Create the new subsidiary OU in parent OU.
        /// </summary>
        /// <param name="parentId">The id of parent OU</param>
        /// <param name="name">The name of new subsidiary OU.</param>
        /// <returns>Return the updated root OU with addition of a new subsidiary OU.</returns>
        /// <exception cref="EntityNotFoundException"></exception>
        public async Task<OrganizationUnit> CreateSubAsync(string parentId, string name)
        {
            var rootId = GetRootId(parentId);
            var rootOu = await _repository.GetAsync(rootId);

            var targetOu = GetSubsidiary(rootOu, parentId);
            if (targetOu == null)
            {
                throw new EntityNotFoundException(typeof(OrganizationUnit), parentId);
            }

            var newSubsidiaryId = CreateNewSubsidiaryId(parentId, targetOu);
            var newOu = new OrganizationUnit(newSubsidiaryId, name, new List<OrganizationUnit>());
            targetOu.Children.Add(newOu);

            return await _repository.UpdateAsync(targetOu);
        }

        public async Task DeleteAsync(string organizationUnitId)
        {
            if (IsRootId(organizationUnitId))
            {
                await _repository.DeleteAsync(organizationUnitId);
            }
            else
            {
                var rootId = GetRootId(organizationUnitId);
                var rootOU = await _repository.GetAsync(rootId);
                var parentOU = GetSubsidiary(rootOU, GetParentId(organizationUnitId));
                if (parentOU == null)
                {
                    throw new EntityNotFoundException(typeof(OrganizationUnit), organizationUnitId);
                }

                var targetOu = parentOU.Children.Single(x => x.Id == organizationUnitId);
                parentOU.Children.Remove(targetOu);

                await _repository.UpdateAsync(parentOU);
            }
        }

        private static string CreateNewSubsidiaryId(string parentId, OrganizationUnit sourceOu)
        {
            var childrenCount = sourceOu?.Children?.Count ?? 0;
            return $"{parentId}{ID_SEPARATOR}{childrenCount}";
        }

        private string GetRootId(string organizationUnitId)
        {
            if (IsRootId(organizationUnitId))
            {
                return organizationUnitId;
            }

            return !organizationUnitId.IsNullOrWhiteSpace()
                ? organizationUnitId.Substring(startIndex: 0, organizationUnitId.IndexOf(ID_SEPARATOR))
                : _guidGenerator.Create().ToString("N");
        }

        private bool IsRootId(string ouId) => !ouId.Contains(ID_SEPARATOR.ToString());

        private static OrganizationUnit GetSubsidiary(OrganizationUnit sourceOu, string subsidiaryId)
        {
            if (sourceOu?.Children == null || !(bool)sourceOu.Children?.Any() || sourceOu.Id.Equals(subsidiaryId))
            {
                return sourceOu;
            }

            foreach (var ou in sourceOu.Children)
            {
                if (!subsidiaryId.Contains(ou.Id))
                {
                    continue;
                }

                return subsidiaryId.Equals(ou.Id) ? ou : GetSubsidiary(ou, subsidiaryId);
            }

            return null;
        }

        private string GetParentId(string organizationUnitId) => IsRootId(organizationUnitId)
            ? organizationUnitId
            : organizationUnitId.Substring(startIndex: 0, organizationUnitId.LastIndexOf(ID_SEPARATOR) + 1);

        //public virtual async Task<string> GetNextChildCodeAsync(long? parentId)
        //{
        //    var lastChild = await GetLastChildOrNullAsync(parentId);
        //    if (lastChild == null)
        //    {
        //        var parentCode = parentId != null ? await GetCodeAsync(parentId.Value) : null;
        //        return OrganizationUnit.AppendCode(parentCode, OrganizationUnit.CreateCode(1));
        //    }

        //    return OrganizationUnit.CalculateNextCode(lastChild.Code);
        //}
        //public virtual async Task<OrganizationUnit> GetLastChildOrNullAsync(long? parentId)
        //{
        //    var children = await OrganizationUnitRepository.GetAllListAsync(ou => ou.ParentId == parentId);
        //    return children.OrderBy(c => c.Code).LastOrDefault();
        //}

        //public virtual async Task<string> GetCodeAsync(Guid id)
        //{
        //    return (await _repository.GetAsync(id)).Code;
        //}

        //public async Task<List<OrganizationUnit>> FindChildrenAsync(long? parentId, bool recursive = false)
        //{
        //    if (!recursive)
        //    {
        //        return await _repository.GetAllListAsync(ou => ou.ParentId == parentId);
        //    }

        //    if (!parentId.HasValue)
        //    {
        //        return await _repository.GetAllListAsync();
        //    }

        //    var code = await GetCodeAsync(parentId.Value);

        //    return await _repository.GetAllListAsync(ou => ou.Code.StartsWith(code) && ou.Id != parentId.Value);
        //}

    }
}