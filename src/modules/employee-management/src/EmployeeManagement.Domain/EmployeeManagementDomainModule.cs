﻿using Volo.Abp.Modularity;

namespace EmployeeManagement
{
    [DependsOn(
        typeof(EmployeeManagementDomainSharedModule)
        )]
    public class EmployeeManagementDomainModule : AbpModule
    {

    }
}
