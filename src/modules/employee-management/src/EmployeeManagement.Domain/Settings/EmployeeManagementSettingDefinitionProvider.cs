﻿using Volo.Abp.Settings;

namespace EmployeeManagement.Settings
{
    public class EmployeeManagementSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            /* Define module settings here.
             * Use names from EmployeeManagementSettings class.
             */
        }
    }
}