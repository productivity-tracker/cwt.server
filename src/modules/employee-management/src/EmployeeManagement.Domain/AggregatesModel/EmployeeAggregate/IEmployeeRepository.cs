﻿using System;
using Volo.Abp.Domain.Repositories;

namespace EmployeeManagement.AggregatesModel.EmployeeAggregate
{
    public interface IEmployeeRepository : IRepository<Employee, Guid>
    { }
}