﻿using System;
using System.Collections.Generic;
using Volo.Abp.Domain.Entities.Auditing;

namespace EmployeeManagement.AggregatesModel.EmployeeAggregate
{
    public class Employee : FullAuditedAggregateRoot<Guid>, IEquatable<Employee>
    {
        public string DisplayName { get; private set; }
        public string JobTitle { get; private set; }
        public DateTime? LastLoggedOnDate { get; private set; }
        public ISet<string> OrganizationUnitsIds { get; private set; }
        public string Notes { get; private set; }

        protected Employee()
        { }

        public Employee(Guid id,
                        string displayName,
                        string jobTitle,
                        IEnumerable<string> organizationUnitsIds,
                        string notes)
            : base(id)
        {
            DisplayName = displayName;
            JobTitle = jobTitle;
            OrganizationUnitsIds = new HashSet<string>(organizationUnitsIds);
            Notes = notes;
        }

        public Employee SetLasLoggedOn(DateTime dateTime)
        {
            LastLoggedOnDate = dateTime;
            return this;
        }

        #region IEquatable Support

        public bool Equals(Employee other)
        {
            if (ReferenceEquals(null, other))
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            return DisplayName == other.DisplayName 
                && JobTitle == other.JobTitle 
                && Nullable.Equals(LastLoggedOnDate, other.LastLoggedOnDate) 
                && Equals(OrganizationUnitsIds, other.OrganizationUnitsIds) 
                && Notes == other.Notes;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != GetType())
            {
                return false;
            }

            return EntityEquals(obj) && Equals((Employee)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = DisplayName != null ? DisplayName.GetHashCode() : 0;
                hashCode = (hashCode * 397) ^ (JobTitle != null ? JobTitle.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ LastLoggedOnDate.GetHashCode();
                hashCode = (hashCode * 397) ^ (OrganizationUnitsIds != null ? OrganizationUnitsIds.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Notes != null ? Notes.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(Employee left, Employee right) => Equals(left, right);

        public static bool operator !=(Employee left, Employee right) => !Equals(left, right);

        #endregion
    }
}