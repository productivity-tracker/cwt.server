﻿using Volo.Abp.Domain.Repositories;

namespace EmployeeManagement.AggregatesModel.OrganizationUnitAggregate
{
    public interface IOrganizationUnitRepository : IBasicRepository<OrganizationUnit, string>
    {
    }
}