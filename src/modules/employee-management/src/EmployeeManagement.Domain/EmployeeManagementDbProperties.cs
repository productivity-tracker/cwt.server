﻿namespace EmployeeManagement
{
    public static class EmployeeManagementDbProperties
    {
        public static string DbTablePrefix { get; set; } = "EmployeeMgmt";

        public static string DbSchema { get; set; } = null;

        public const string ConnectionStringName = "EmployeeManagement";
    }
}
