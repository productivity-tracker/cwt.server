﻿using AutoMapper;
using EmployeeManagement.AggregatesModel.OrganizationUnitAggregate;
using EmployeeManagement.OrganizationUnits.DTO;

namespace EmployeeManagement.OrganizationUnits
{
    public class OrganizationUnitApplicationAutoMapperProfile : Profile
    {
        public OrganizationUnitApplicationAutoMapperProfile()
        {
            CreateMap<OrganizationUnit, OrganizationUnitDto>(MemberList.None);
        }
    }
}