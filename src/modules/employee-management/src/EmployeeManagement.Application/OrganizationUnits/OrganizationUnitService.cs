﻿using System.Linq;
using System.Threading.Tasks;
using EmployeeManagement.AggregatesModel.OrganizationUnitAggregate;
using EmployeeManagement.OrganizationUnits.DTO;
using EmployeeManagement.Services.Organizations;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;

namespace EmployeeManagement.OrganizationUnits
{
    [Authorize]
    public class OrganizationUnitService : ApplicationService, IOrganizationUnitService
    {
        private readonly OrganizationUnitManager _manager;
        private readonly IOrganizationUnitRepository _repository;

        public OrganizationUnitService(OrganizationUnitManager manager, IOrganizationUnitRepository repository)
        {
            _manager = manager;
            _repository = repository;
        }

        public async Task CreateAsync(CreateOuRequest request) => await _manager.CreateRootAsync(request.DisplayName);

        public async Task AddSubAsync(CreateSubOuRequest request) => await _manager.CreateSubAsync(request.ParentOuId, request.DisplayName);

        public async Task DeleteAsync(string organizationUnitId) => await _manager.DeleteAsync(organizationUnitId);

        public async Task<PagedResultDto<OrganizationUnitDto>> GetAllAsync()
        {
            var entities = await _repository.GetListAsync();
            return new PagedResultDto<OrganizationUnitDto>(entities.Count,
                                                           entities.Select(e => ObjectMapper.Map<OrganizationUnit, OrganizationUnitDto>(e)).ToList());
        }
    }
}