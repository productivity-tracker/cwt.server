﻿using AutoMapper;
using EmployeeManagement.AggregatesModel.EmployeeAggregate;
using EmployeeManagement.Employees.DTO;

namespace EmployeeManagement.Employees
{
    public class EmployeeApplicationAutoMapperProfile : Profile
    {
        public EmployeeApplicationAutoMapperProfile()
        {
            CreateMap<Employee, EmployeeDto>(MemberList.None)
                .ReverseMap()
                .ConstructUsing(dto => new Employee(dto.Id, dto.DisplayName, dto.JobTitle, dto.OrganizationUnitsIds, dto.Notes));
        }
    }
}