﻿using System;
using EmployeeManagement.AggregatesModel.EmployeeAggregate;
using EmployeeManagement.Employees.DTO;
using Microsoft.AspNetCore.Authorization;
using Volo.Abp.Application.Services;

namespace EmployeeManagement.Employees
{
    [Authorize]
    public class EmployeeService : CrudAppService<Employee, EmployeeDto, Guid>, IEmployeeService
    {
        public EmployeeService(IEmployeeRepository repository)
            : base(repository)
        { }
    }
}