﻿using EmployeeManagement.AggregatesModel.EmployeeAggregate;
using EmployeeManagement.AggregatesModel.OrganizationUnitAggregate;
using EmployeeManagement.Employees.DTO;
using EmployeeManagement.OrganizationUnits.DTO;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;
using Volo.Abp.Application;
using Volo.Abp.EventBus.Distributed;

namespace EmployeeManagement
{
    [DependsOn(
        typeof(EmployeeManagementDomainModule),
        typeof(EmployeeManagementApplicationContractsModule),
        typeof(AbpDddApplicationModule),
        typeof(AbpAutoMapperModule)
        )]
    public class EmployeeManagementApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpAutoMapperOptions>(options =>
            {
                options.AddMaps<EmployeeManagementApplicationModule>(validate: true);
            });

            Configure<AbpDistributedEventBusOptions>(options =>
            {
                options.EtoMappings.Add<Employee, EmployeeDto>();
                options.EtoMappings.Add<OrganizationUnit, OrganizationUnitDto>();
            });
        }
    }
}
