﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using JetBrains.Annotations;
using Volo.Abp.DependencyInjection;

namespace RealTime.Domain.OnlineTracking
{
    /// <summary>
    /// Implements <see cref="IOnlineClientManager" />.
    /// </summary>
    internal class OnlineClientManager : IOnlineClientManager, ISingletonDependency
    {
        /// <summary>
        /// Online clients Store.
        /// </summary>
        protected IOnlineClientStore Store { get; }

        public event EventHandler<OnlineClientEventArgs> ClientConnected;
        public event EventHandler<OnlineClientEventArgs> ClientDisconnected;
        public event EventHandler<OnlineUserEventArgs> UserConnected;
        public event EventHandler<OnlineUserEventArgs> UserDisconnected;

        /// <summary>
        /// Initializes a new instance of the <see cref="OnlineClientManager" /> class.
        /// </summary>
        public OnlineClientManager(IOnlineClientStore store)
        {
            Store = store;
        }

        public virtual void Add(IOnlineClient client)
        {
            var userWasAlreadyOnline = false;
            var userId = client.UserId;
            var userIsAuthenticated = !string.IsNullOrWhiteSpace(userId);

            if (userIsAuthenticated)
            {
                userWasAlreadyOnline = IsOnline(userId);
            }

            Store.Add(client);

            ClientConnected?.Invoke(this, new OnlineClientEventArgs(client));

            if (userIsAuthenticated && !userWasAlreadyOnline)
            {
                UserConnected?.Invoke(this, new OnlineUserEventArgs(userId, client));
            }
        }

        public virtual bool Remove(string connectionId)
        {
            var result = Store.TryRemove(connectionId, out var client);
            if (result)
            {
                var userId = client.UserId;

                if (!string.IsNullOrWhiteSpace(userId) && !IsOnline(userId))
                {
                    UserDisconnected?.Invoke(this, new OnlineUserEventArgs(userId, client));
                }

                ClientDisconnected?.Invoke(this, new OnlineClientEventArgs(client));
            }

            return result;
        }

        public virtual IOnlineClient GetByConnectionIdOrNull(string connectionId) => 
            Store.TryGet(connectionId, out var client) ? client : null;

        public IReadOnlyList<IOnlineClient> GetAllByUserId(string userId) =>
            GetAllClients()
                .Where(c => c.UserId == userId)
                .ToImmutableList();

        public bool IsOnline([NotNull] string userId) => GetAllByUserId(userId).Any();

        public virtual IReadOnlyList<IOnlineClient> GetAllClients() => Store.GetAll();
    }
}