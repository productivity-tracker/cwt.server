﻿using System.Threading.Tasks;

namespace RealTime.Domain
{
    public interface IMessageGateway
    {
        Task SendUnicast<T>(string topicName, T message);
        Task SendMulticast<T>(string topicName, T message);
        Task SendBroadcast<T>(string topicName, T message);
    }
}