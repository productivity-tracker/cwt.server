﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp;
using Volo.Abp.DependencyInjection;
using Volo.Abp.EventBus;
using Volo.Abp.EventBus.Distributed;
using Volo.Abp.Threading;

namespace RealTime.SignalR
{
    //[Dependency(ReplaceServices = true)]
    //[ExposeServices(typeof(IDistributedEventBus), typeof(SignalRDistributedEventBus<>))]
    //public class SignalRDistributedEventBus<THub> : EventBusBase, IDistributedEventBus, ISingletonDependency
    //    where THub : Hub
    //{
    //    //TODO: Accessing to the List<IEventHandlerFactory> may not be thread-safe!
    //    protected ConcurrentDictionary<Type, List<IEventHandlerFactory>> HandlerFactories { get; }

    //    protected ConcurrentDictionary<string, Type> EventTypes { get; }

    //    protected IHubContext<THub> HubContext { get; }

    //    public SignalRDistributedEventBus(
    //        IServiceScopeFactory serviceScopeFactory,
    //        IHubContext<THub> context)
    //        : base(serviceScopeFactory)
    //    {
    //        HubContext = context;

    //        HandlerFactories = new ConcurrentDictionary<Type, List<IEventHandlerFactory>>();
    //    }

    //    protected override IEnumerable<EventTypeWithEventHandlerFactories> GetHandlerFactories(Type eventType)
    //    {
    //        var handlerFactoryList = new List<EventTypeWithEventHandlerFactories>();

    //        foreach (var handlerFactory in HandlerFactories.Where(hf => ShouldTriggerEventForHandler(eventType, hf.Key)))
    //        {
    //            handlerFactoryList.Add(new EventTypeWithEventHandlerFactories(handlerFactory.Key, handlerFactory.Value));
    //        }

    //        return handlerFactoryList.ToArray();
    //    }

    //    public override IDisposable Subscribe(Type eventType, IEventHandlerFactory factory)
    //    {
    //        var handlerFactories = GetOrCreateHandlerFactories(eventType);

    //        if (factory.IsInFactories(handlerFactories))
    //        {
    //            return NullDisposable.Instance;
    //        }

    //        handlerFactories.Add(factory);

    //        if (handlerFactories.Count == 1) //TODO: Multi-threading!
    //        {
    //            HubContext.Groups.AddToGroupAsync(
    //                "connectionId",
    //                EventNameAttribute.GetNameOrDefault(eventType)); //TODO: Provide connectionId from HubCallerContext
    //        }

    //        return new EventHandlerFactoryUnregistrar(this, eventType, factory);
    //    }

    //    public override void Unsubscribe<TEvent>(Func<TEvent, Task> action)
    //    {
    //        Check.NotNull(action, nameof(action));

    //        GetOrCreateHandlerFactories(typeof(TEvent))
    //            .Locking(
    //                factories =>
    //                {
    //                    factories.RemoveAll(
    //                        factory =>
    //                        {
    //                            var singleInstanceFactory = factory as SingleInstanceHandlerFactory;
    //                            if (singleInstanceFactory == null)
    //                            {
    //                                return false;
    //                            }

    //                            var actionHandler = singleInstanceFactory.HandlerInstance as ActionEventHandler<TEvent>;
    //                            if (actionHandler == null)
    //                            {
    //                                return false;
    //                            }

    //                            return actionHandler.Action == action;
    //                        });
    //                });
    //    }

    //    public override void Unsubscribe(Type eventType, IEventHandler handler) =>
    //        GetOrCreateHandlerFactories(eventType)
    //            .Locking(
    //                factories =>
    //                {
    //                    factories.RemoveAll(
    //                        factory =>
    //                            factory is SingleInstanceHandlerFactory handlerFactory && handlerFactory.HandlerInstance == handler);
    //                });

    //    public override void Unsubscribe(Type eventType, IEventHandlerFactory factory) =>
    //        GetOrCreateHandlerFactories(eventType).Locking(factories => factories.Remove(factory));

    //    public override void UnsubscribeAll(Type eventType) =>
    //        GetOrCreateHandlerFactories(eventType).Locking(factories => factories.Clear());

    //    public override Task PublishAsync(Type eventType, object eventData)
    //    {
    //        var eventName = EventNameAttribute.GetNameOrDefault(eventType);

    //        return HubContext.Clients.Groups(eventName).SendCoreAsync(eventName, new[] { eventData });
    //    }

    //    public IDisposable Subscribe<TEvent>(IDistributedEventHandler<TEvent> handler)
    //        where TEvent : class =>
    //        Subscribe(typeof(TEvent), handler);

    //    private List<IEventHandlerFactory> GetOrCreateHandlerFactories(Type eventType) =>
    //        HandlerFactories.GetOrAdd(
    //            eventType,
    //            type =>
    //            {
    //                var eventName = EventNameAttribute.GetNameOrDefault(type);
    //                EventTypes[eventName] = type;
    //                return new List<IEventHandlerFactory>();
    //            });

    //    private static bool ShouldTriggerEventForHandler(Type targetEventType, Type handlerEventType)
    //    {
    //        //Should trigger same type
    //        if (handlerEventType == targetEventType)
    //        {
    //            return true;
    //        }

    //        //TODO: Support inheritance? But it does not support on subscription to RabbitMq!
    //        //Should trigger for inherited types
    //        if (handlerEventType.IsAssignableFrom(targetEventType))
    //        {
    //            return true;
    //        }

    //        return false;
    //    }
    //}
}