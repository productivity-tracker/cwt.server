﻿using Volo.Abp.EventBus;
using Volo.Abp.Modularity;

namespace RealTime.SignalR
{
    [DependsOn(typeof(AbpEventBusModule))]
    public class RealTimeEventModule : AbpModule
    {

    }
}