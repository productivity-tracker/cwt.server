﻿using System.Threading.Tasks;

namespace RealTime.SignalR
{
    public interface ISubscriptionManager
    {
        Task AddSubscriptionAsync();
        Task RemoveSubscriptionAsync();
    }
}