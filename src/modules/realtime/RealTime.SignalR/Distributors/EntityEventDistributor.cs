﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
//using RealTime.Application.Contracts.Internal.Distributors;
using RealTime.Application.Contracts.Messages;
using RealTime.Application.Contracts.Routing;
using RealTime.SignalR.Hubs;

namespace RealTime.SignalR.Distributors
{
    //public class EntityEventDistributor : IEntityEventDistributor
    //{
    //    private readonly ILogger _logger;
    //    private readonly IHubContext<EntityEventHub> _hubContext;
        
    //    public EntityEventDistributor(ILogger<EntityEventDistributor> logger, IHubContext<EntityEventHub> context)
    //    {
    //        _logger = logger;
    //        _hubContext = context;
    //    }

    //    public Task DistributeAsync<T>(T message) where T : EntityEventMessage<>
    //    {
    //        var routingKey = RoutingKeyFactory.CreateKey(MessageType.EntityEvent, typeof(T));
    //        return _hubContext.Clients.Group(routingKey).SendCoreAsync(routingKey, new object[] { message });
    //    }
    //}
}