﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Volo.Abp.Auditing;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Entities.Events;
using Volo.Abp.Domain.Entities.Events.Distributed;
using Volo.Abp.EventBus;

namespace RealTime.SignalR.Distributors
{
    //public class EntityChangedEventDistributor<TEntity, THub> : ILocalEventHandler<EntityChangedEventData<TEntity>>, ITransientDependency
    //    where THub : Hub
    //{
    //    private readonly IHubContext<THub> _hubContext;
    //    private readonly IEntityToEtoMapper _mapper;

    //    public EntityChangedEventDistributor(IHubContext<THub> hubContext, IEntityToEtoMapper mapper)
    //    {
    //        _hubContext = hubContext;
    //        _mapper = mapper;
    //    }

    //    public Task HandleEventAsync(EntityChangedEventData<TEntity> eventData)
    //    {
    //        var dto = _mapper.Map(eventData.Entity);

    //        var changeType = eventData switch
    //        {
    //            EntityCreatedEventData<TEntity> _ => EntityChangeType.Created,
    //            EntityUpdatedEventData<TEntity> _ => EntityChangeType.Updated,
    //            EntityDeletedEventData<TEntity> _ => EntityChangeType.Deleted,
    //            _ => throw new ArgumentOutOfRangeException(eventData.Entity.ToString())
    //        };
           

    //        return _hubContext.Clients.Groups("").SendCoreAsync();
    //    }
    //}
}