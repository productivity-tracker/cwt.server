﻿using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using RealTime.Domain.OnlineTracking;

namespace RealTime.SignalR.Hubs
{
    public class RealTimeMessageHub : OnlineClientHubBase
    {
        public RealTimeMessageHub(ILogger<RealTimeMessageHub> logger, IOnlineClientManager onlineClientManager)
            : base(onlineClientManager)
        {
            Logger = logger;
        }

        //public Task SubscribeAsync(string topic)
        //{

        //}

        //public Task UnSubscribeAsync(string topic)
        //{

        //}
    }
}