﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using RealTime.Domain.OnlineTracking;

namespace RealTime.SignalR.Hubs
{
    public abstract class OnlineClientHubBase : Hub
    {
        protected IOnlineClientManager OnlineClientManager { get; }

        protected ILogger Logger { get; set; }

        protected OnlineClientHubBase(
            IOnlineClientManager onlineClientManager)
        {
            OnlineClientManager = onlineClientManager;

            Logger = NullLogger.Instance;
        }

        public override async Task OnConnectedAsync()
        {
            await base.OnConnectedAsync();

            var client = CreateClientForCurrentConnection();

            Logger.LogDebug("A client is connected: " + client);

            OnlineClientManager.Add(client);
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            await base.OnDisconnectedAsync(exception);

            Logger.LogDebug("A client is disconnected: " + Context.ConnectionId);

            try
            {
                OnlineClientManager.Remove(Context.ConnectionId);
            }
            catch (Exception ex)
            {
                Logger.LogWarning(ex, ex.Message);
            }
        }

        protected virtual IOnlineClient CreateClientForCurrentConnection() =>
            new OnlineClient(
                Context.ConnectionId,
                GetIpAddressOfClient(),
                Context.UserIdentifier);

        protected virtual string GetIpAddressOfClient()
        {
            try
            {
                return Context.GetHttpContext().GetIpAddress();
            }
            catch (Exception ex)
            {
                Logger.LogError("Can not find IP address of the client! connectionId: " + Context.ConnectionId);
                Logger.LogError(ex.Message, ex);
                return "";
            }
        }
    }
}