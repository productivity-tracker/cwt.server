﻿using System.Threading.Tasks;
using Volo.Abp.Auditing;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Entities.Events;
using Volo.Abp.EventBus;

namespace RealTime.Application
{
    //public class EntityChangedEventHandler<TEntity> : ILocalEventHandler<EntityChangedEventData<TEntity>>, ITransientDependency
    //{
    //    private readonly IEntityEventDistributor _distributor;

    //    public EntityChangedEventHandler(IEntityEventDistributor distributor)
    //    {
    //        _distributor = distributor;
    //    }

    //    public async Task HandleEventAsync(EntityChangedEventData<TEntity> eventData)
    //    {
    //        var changeType = eventData switch
    //        {
    //            EntityCreatedEventData<TEntity> _ => EntityChangeType.Created,
    //            EntityUpdatedEventData<TEntity> _ => EntityChangeType.Updated,
    //            EntityDeletedEventData<TEntity> _ => EntityChangeType.Deleted,
    //            _ => EntityChangeType.Unknown
    //        };

    //        var message = new EntityChangedMessage<TEntity>(changeType, eventData.Entity);
    //        await _distributor.DistributeAsync(message);
    //    }
    //}
}