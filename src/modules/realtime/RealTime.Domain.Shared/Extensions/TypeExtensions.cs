﻿using System;

namespace RealTime.Domain.Shared.Extensions
{
    public static class TypeExtensions
    {
        public static string GetNameWithoutGenerics(this Type type) => type.GetNameWithoutGenerics(t => t.Name);

        public static string GetFullNameWithoutGenerics(this Type type) => type.GetNameWithoutGenerics(t => t.FullName);

        private static string GetNameWithoutGenerics(this Type type, Func<Type, string> nameProvider)
        {
            var name = nameProvider(type);
            return type.IsGenericType 
                ? name.Substring(0, name.IndexOf("`", StringComparison.Ordinal)) 
                : name;
        }
    }
}