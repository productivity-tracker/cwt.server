﻿using System;
using System.Linq;

namespace RealTime.Application.Contracts.Routing
{
    public static class TypeNameExtensions
    {
        private static readonly string[] _typeAliases =
        {
            "void",    // 0
            null,      // 1 (any other type)
            "DBNull",  // 2
            "bool",    // 3
            "char",    // 4
            "sbyte",   // 5
            "byte",    // 6
            "short",   // 7
            "ushort",  // 8
            "int",     // 9
            "uint",    // 10
            "long",    // 11
            "ulong",   // 12
            "float",   // 13
            "double",  // 14
            "decimal", // 15
            null,      // 16 (DateTime)
            null,      // 17 (-undefined-)
            "string"   // 18
        };

        public static string GetPronounceableName(this Type type)
        {
            if (TryGetNameAliasNonArray(type, out var friendlyName)) return friendlyName;

            if (!type.IsGenericType)
            {
                friendlyName = type.Name;
            }
            else
            {
                var generics = string.Join(
                    ":", 
                    type.GetGenericArguments().Select(t => t.GetPronounceableName()));
                var iBacktick = type.Name.IndexOf('`');
                friendlyName = (iBacktick > 0 ? type.Name.Remove(iBacktick) : type.Name)
                             + $".{generics}";
            }

            return friendlyName;
        }

        public static string GetFriendlyName(this Type type, bool aliasNullable = true, bool includeSpaceAfterComma = true)
        {
            TryGetInnerElementType(ref type, out var arrayBrackets);
            if (!TryGetNameAliasNonArray(type, out var friendlyName))
            {
                if (!type.IsGenericType)
                {
                    friendlyName = type.Name;
                }
                else
                {
                    if (aliasNullable && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        var generics = GetFriendlyName(type.GetGenericArguments()[0]);
                        friendlyName = generics + "?";
                    }
                    else
                    {
                        var generics = GetFriendlyGenericArguments(type, includeSpaceAfterComma);
                        var iBacktick = type.Name.IndexOf('`');
                        friendlyName = (iBacktick > 0 ? type.Name.Remove(iBacktick) : type.Name)
                                     + $"<{generics}>";
                    }
                }
            }

            return friendlyName + arrayBrackets;
        }

        public static bool TryGetNameAlias(this Type type, out string alias)
        {
            TryGetInnerElementType(ref type, out var arrayBrackets);
            if (!TryGetNameAliasNonArray(type, out alias))
                return false;
            alias += arrayBrackets;
            return true;
        }

        private static string GetFriendlyGenericArguments(Type type, bool includeSpaceAfterComma)
            => string.Join(
                includeSpaceAfterComma ? ", " : ",",
                type.GetGenericArguments().Select(t => t.GetFriendlyName()));

        private static bool TryGetNameAliasNonArray(Type type, out string alias)
            => (alias = _typeAliases[(int)Type.GetTypeCode(type)]) != null
            && !type.IsEnum;

        private static bool TryGetInnerElementType(ref Type type, out string arrayBrackets)
        {
            arrayBrackets = null;
            if (!type.IsArray)
                return false;
            do
            {
                arrayBrackets += "[" + new string(',', type.GetArrayRank() - 1) + "]";
                type = type.GetElementType();
            }
            while (type.IsArray);

            return true;
        }
    }
}