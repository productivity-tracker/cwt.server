﻿using System;
using RealTime.Application.Contracts.Messages;

namespace RealTime.Application.Contracts.Routing
{
    public static class RoutingKeyFactory
    {
        public const string INCLUDE_ALL_MESSAGES = "#";

        public static string CreateKey(MessageType messageType, Type entityEvent, string entityId)
        {
            var eventName = entityEvent.GetPronounceableName();
            var entityIdentifier = string.IsNullOrWhiteSpace(entityId) ? INCLUDE_ALL_MESSAGES : entityId;
            return $"{messageType}.{eventName}.{entityIdentifier}";
        }
    }
}