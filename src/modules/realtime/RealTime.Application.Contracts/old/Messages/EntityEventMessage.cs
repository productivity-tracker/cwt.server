﻿using System;

namespace RealTime.Application.Contracts.Messages
{
    public abstract class EntityEventMessage<T> : IEventMessage
    {
        //public string KeyAsString { get; }
        //public DateTime RaisedAt { get; }
        public T EventData { get; }

        protected EntityEventMessage(T eventData)
        {
            EventData = eventData;
        }
    }
}