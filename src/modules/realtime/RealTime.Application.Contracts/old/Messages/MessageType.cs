﻿namespace RealTime.Application.Contracts.Messages
{
    public enum MessageType
    {
        EntityEvent = 1,
        Notification = 2,
        Command = 3
    }
}