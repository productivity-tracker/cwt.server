﻿using System;

namespace RealTime.Application.Contracts.Messages
{
    public class EntityChangedMessage<T> : EntityEventMessage<T>
    {
        public EntityChangeType ChangeType { get; }

        public EntityChangedMessage(EntityChangeType changeType, T eventData) 
            : base(eventData)
        {
            ChangeType = changeType;
        }
    }
}