﻿namespace RealTime.Application.Contracts.Messages
{
    public enum EntityChangeType : byte
    {
        Created = 0,

        Updated = 1,

        Deleted = 2,

        Unknown = 3,
    }
}