﻿using System;

namespace RealTime.Application.Contracts
{
    public interface IMessageDispatcher
    {
        IObservable<T> On<T>();
        IDisposable Subscribe<T>(Action<T> onNext);
    }
}