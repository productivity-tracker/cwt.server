﻿namespace RealTime.Application.Contracts.Messages
{
    public class RealTimeMessage<T>
    {
        public T Data { get; set; }
        public IMessageTimestamp Timestamps { get; set; }
    }
}