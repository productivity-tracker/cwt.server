﻿namespace RealTime.Application.Contracts.Messages
{
    public interface IMessageTimestamp
    {
        long Created { get; }

        long Sent { get; }

        long Received { get; }

        long Dispatched { get; }
    }
}