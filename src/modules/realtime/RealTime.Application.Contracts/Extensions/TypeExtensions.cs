﻿using System;

namespace RealTime.Application.Contracts.Extensions
{
    public static class TypeExtensions
    {
        public static string GetNameWithoutGenerics(this Type type) => 
            type.GetNameWithoutGenerics(t => t.Name);

        public static string GetFullNameWithoutGenerics(this Type type) => 
            type.GetNameWithoutGenerics(t => t.FullName);

        private static string GetNameWithoutGenerics(this Type type, Func<Type, string> nameProvider)
        {
            var name = nameProvider(type);
            if (type.IsGenericType)
            {
                return name.Substring(0, name.IndexOf("`", StringComparison.Ordinal));
            }

            return name;
        }
    }
}