﻿using System;
using System.Reactive.Linq;
using RealTime.Application.Contracts.Messages;

namespace RealTime.Application.Contracts.Extensions
{
    public static class ObservableMessagingExtensions
    {
        public static IObservable<TMessage> OnlyNewest<TMessage, TEntity>(this IObservable<TMessage> source) 
            where TMessage : RealTimeMessage<TEntity> =>
            source.Scan((prev, next) => next.Timestamps.Created > prev.Timestamps.Created ? next : prev)
                  .DistinctUntilChanged(x => x.Timestamps.Created);
    }
}