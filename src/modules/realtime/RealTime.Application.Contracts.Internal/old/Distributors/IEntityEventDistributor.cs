﻿using System.Threading.Tasks;

namespace RealTime.Application.Contracts.Internal.Distributors
{
    public interface IEntityEventDistributor
    {
        Task DistributeAsync<T>(T message);
    }
}