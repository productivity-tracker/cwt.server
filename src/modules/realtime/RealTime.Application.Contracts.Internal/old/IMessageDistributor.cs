﻿using System;
using System.Threading.Tasks;
using RealTime.Application.Contracts.Messages;

namespace RealTime.Application.Contracts.Internal
{
    public interface IMessageDistributor
    {
        Task DistributeAsync<T>(T message, MessageType messageType, string destinationAddress = null) 
            where T : IMessage;
    }
}