﻿using System;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;

namespace AuthServer.Host
{
    public class Program
    {
        public static int Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
#if DEBUG
                         .MinimumLevel.Debug()
#else
                         .MinimumLevel.Information()
#endif
                         .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                         .Enrich.WithProperty("Application", "AuthServer")
                         .Enrich.FromLogContext()
                         .WriteTo.File("Logs/logs.txt")
                         .WriteTo.ColoredConsole()
                         .CreateLogger();

            try
            {
                Log.Information("Starting AuthServer.Host.");
                using (var host = CreateHostBuilder(args).Build())
                {
                    Console.Title = host.Services.GetService<IHostEnvironment>().ApplicationName;
                    Console.OutputEncoding = Encoding.UTF8;
                    host.Run();
                    return 0;
                }
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "AuthServer.Host terminated unexpectedly!");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        internal static IHostBuilder CreateHostBuilder(string[] args)
            => Microsoft.Extensions.Hosting.Host.CreateDefaultBuilder(args)
                        .ConfigureWebHostDefaults(webBuilder => webBuilder.UseStartup<Startup>())
                        .UseAutofac()
                        .UseSerilog();
    }
}