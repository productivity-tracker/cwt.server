﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace CWT.Domain.Abstractions.Repository
{
    public interface IBlobRepository<out T>
    {
        void Delete(string objectName);

        Task DeleteAsync(string objectName);

        void DeleteMany(IEnumerable<string> objectNames);

        Task DeleteManyAsync(IEnumerable<string> objectNames);

        string Put(string objectName, Stream stream, string contentType = null, Dictionary<string, string> metaData = null);

        Task<string> PutAsync(string objectName, Stream stream, string contentType = null, Dictionary<string, string> metaData = null);

        string Get(string objectName, Stream stream);

        Task<string> GetAsync(string objectName, Stream stream);
        
        IObservable<T> GetListObjectsAsync(string prefix, bool recursive, CancellationToken cancellationToken = default);
    }
}