﻿using System;
using Microsoft.AspNetCore.Builder;
using Volo.Abp.Modularity;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Volo.Abp.SignalR
{
    public class AbpSignalRModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddSignalR();
        }

        public override void OnApplicationInitialization(ApplicationInitializationContext context)
        {
            var app = context.GetApplicationBuilder();
            var options = context.GetConfiguration().GetSection("SignalR").Get<AbpSignalROptions>();
            app.UseRouting();
            app.UseEndpoints(builder => builder.MapSignalRHubs(options, AppDomain.CurrentDomain.GetAssemblies()));
        }
    }
}