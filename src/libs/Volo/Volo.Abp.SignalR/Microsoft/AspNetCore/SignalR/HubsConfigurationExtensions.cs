﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Volo.Abp.SignalR;
using Microsoft.AspNetCore.Routing;

namespace Microsoft.AspNetCore.SignalR
{
    public static class HubsConfigurationExtensions
    {
        private static readonly MethodInfo _mapHubMethod = typeof(HubEndpointRouteBuilderExtensions).GetMethod("MapHub", new[] { typeof(IEndpointRouteBuilder), typeof(string) });

        public static IEndpointRouteBuilder MapSignalRHubs(
            this IEndpointRouteBuilder endpoints,
            AbpSignalROptions options,
            params Assembly[] assemblies)
        {
            var targetAssemblies = assemblies.Any() ? assemblies : new[] { Assembly.GetExecutingAssembly() };
            var allHubs = targetAssemblies
                           .Select(assembly => assembly.GetTypes().Where(t => t.IsSubclassOf(typeof(Hub)) && !t.IsAbstract).ToList())
                           .SelectMany(hubTypes => hubTypes);

            foreach (var hubType in allHubs)
            {
                if (!options.TryGetValue(hubType.Name, out var pattern))
                    continue;

                _mapHubMethod.MakeGenericMethod(hubType)
                             .Invoke(endpoints, new object[] { endpoints, pattern });
            }

            return endpoints;
        }
    }
}