﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Volo.Abp.AspNetCore.SignalR;
using Volo.Abp.RealTime.OnlineTracking;
using Volo.Abp.RealTime.SignalR.Hubs;

namespace Volo.Abp.EventBus.SignalR.Hubs
{
    [HubRoute("/real-time/events")]
    public class DistributedEventHub : AbpOnlineClientHubBase
    {
        private readonly ILogger _logger;

        public DistributedEventHub(ILogger<DistributedEventHub> logger, IOnlineClientManager onlineClientManager)
            : base(onlineClientManager)
        {
            _logger = logger;
        }

        [HubMethodName("subscribe")]
        public Task SubscribeAsync(string topic)
        {
            _logger.LogDebug("Client ({@client}) subscribes to a topic '{@topic}'", CurrentClient, topic);
            return Groups.AddToGroupAsync(Context.ConnectionId, topic);
        }

        [HubMethodName("unsubscribe")]
        public Task UnSubscribeAsync(string topic)
        {
            _logger.LogDebug("Client ({@client}) unsubscribes to a topic '{@topic}'", CurrentClient, topic);
            return Groups.RemoveFromGroupAsync(Context.ConnectionId, topic);
        }
    }
}