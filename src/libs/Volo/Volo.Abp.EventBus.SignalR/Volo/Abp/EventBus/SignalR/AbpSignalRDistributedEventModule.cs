﻿using Volo.Abp.AspNetCore.SignalR;
using Volo.Abp.Modularity;
using Volo.Abp.RealTime.SignalR;

namespace Volo.Abp.EventBus.SignalR
{
    [DependsOn(
        typeof(AbpAspNetCoreSignalRModule), 
        typeof(AbpEventBusModule),
        typeof(AbpRealTimeSignalRModule))]
    public class AbpSignalRDistributedEventModule : AbpModule
    {
        
    }
}