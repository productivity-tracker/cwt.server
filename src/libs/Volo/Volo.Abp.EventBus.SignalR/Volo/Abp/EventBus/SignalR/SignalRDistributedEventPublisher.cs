﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.DependencyInjection;
using Volo.Abp.EventBus.Distributed;
using Volo.Abp.EventBus.SignalR.Hubs;

namespace Volo.Abp.EventBus.SignalR
{
    [Dependency(ReplaceServices = true)]
    [ExposeServices(typeof(IDistributedEventBus), typeof(SignalRDistributedEventPublisher))]
    public class SignalRDistributedEventPublisher: EventBusBase, IDistributedEventBus, ISingletonDependency
    {
        protected IHubContext<DistributedEventHub> HubContext { get; }

        public SignalRDistributedEventPublisher(
            IHubContext<DistributedEventHub> context,
            IServiceScopeFactory serviceScopeFactory)
            : base(serviceScopeFactory)
        {
            HubContext = context;
        }

        public override Task PublishAsync(Type eventType, object eventData)
        {
            var eventName = EventNameAttribute.GetNameOrDefault(eventType);
            return HubContext.Clients.Groups(eventName).SendCoreAsync(eventName, new[] { eventData });
        }

        #region Not Implemented

        protected override IEnumerable<EventTypeWithEventHandlerFactories> GetHandlerFactories(Type eventType) =>
            throw new NotImplementedException();

        public override void UnsubscribeAll(Type eventType) => throw new NotImplementedException();

        public override IDisposable Subscribe(Type eventType, IEventHandlerFactory factory) => throw new NotImplementedException();

        public override void Unsubscribe<TEvent>(Func<TEvent, Task> action) => throw new NotImplementedException();

        public override void Unsubscribe(Type eventType, IEventHandler handler) => throw new NotImplementedException();

        public override void Unsubscribe(Type eventType, IEventHandlerFactory factory) => throw new NotImplementedException();

        public IDisposable Subscribe<TEvent>(IDistributedEventHandler<TEvent> handler)
            where TEvent : class =>
            throw new NotImplementedException();

        #endregion
    }
}