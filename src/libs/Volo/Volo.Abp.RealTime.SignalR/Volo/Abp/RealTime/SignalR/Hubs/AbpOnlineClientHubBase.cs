﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Volo.Abp.AspNetCore.SignalR;
using Volo.Abp.RealTime.OnlineTracking;

namespace Volo.Abp.RealTime.SignalR.Hubs
{
    public abstract class AbpOnlineClientHubBase : AbpHub
    {
        protected IOnlineClientManager OnlineClientManager { get; }
        protected IOnlineClient CurrentClient => OnlineClientManager.GetByConnectionIdOrNull(Context.ConnectionId);

        protected AbpOnlineClientHubBase(IOnlineClientManager onlineClientManager)
        {
            OnlineClientManager = onlineClientManager;
        }

        public override async Task OnConnectedAsync()
        {
            await base.OnConnectedAsync();

            var client = CreateClientForCurrentConnection();

            Logger.LogDebug("A client is connected: {client}", client);

            OnlineClientManager.Add(client);
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            await base.OnDisconnectedAsync(exception);

            Logger.LogDebug("A client is disconnected: {client}", Context.ConnectionId);

            try
            {
                OnlineClientManager.Remove(Context.ConnectionId);
            }
            catch (Exception ex)
            {
                Logger.LogWarning(ex, ex.Message);
            }
        }

        protected virtual IOnlineClient CreateClientForCurrentConnection() =>
            new OnlineClient(
                Context.ConnectionId,
                GetIpAddressOfClient(),
                CurrentUser.Id?.ToString());

        protected virtual string GetIpAddressOfClient()
        {
            try
            {
                return Context.GetHttpContext().GetIpAddress();
            }
            catch (Exception ex)
            {
                Logger.LogError("Can not find IP address of the client! ConnectionId: {@connectionId}", Context.ConnectionId);
                Logger.LogError(ex.Message, ex);
                return "";
            }
        }
    }
}