﻿using Volo.Abp.Modularity;

namespace Volo.Abp.RealTime.SignalR
{
    [DependsOn(typeof(AbpRealTimeModule))]
    public class AbpRealTimeSignalRModule : AbpModule
    {
        
    }
}