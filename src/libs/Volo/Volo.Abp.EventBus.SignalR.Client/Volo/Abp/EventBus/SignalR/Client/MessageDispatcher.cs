﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;
using Volo.Abp.DependencyInjection;
using Microsoft.AspNetCore.SignalR.Client.Rx;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;

namespace Volo.Abp.EventBus.SignalR.Client
{
    public class MessageDispatcher : IMessageDispatcher, ISingletonDependency, IDisposable
    {
        private class ForeverRetryPolicy : IRetryPolicy
        {
            private const int LONG_DELAY_SECONDS = 60;
            private readonly int[] _quickReconnectDelays = { 0, 5, 15, 30 };
            public TimeSpan? NextRetryDelay(RetryContext retryContext) =>
                TimeSpan.FromSeconds(retryContext.PreviousRetryCount < _quickReconnectDelays.Length
                                         ? _quickReconnectDelays[retryContext.PreviousRetryCount]
                                         : LONG_DELAY_SECONDS);
        }

        private readonly ILogger _logger;
        private readonly IDictionary<string, Subject<object>> _subjects =
            new ConcurrentDictionary<string, Subject<object>>();

        private readonly IDisposable _irrelevantSubjectsConductor;
        private readonly object _syncObj = new object();

        private readonly HubConnection _connection;
        
        public MessageDispatcher(ILoggerProvider loggerProvider, IOptions<AbpEventBusSignalRClientOptions> options)
        {
            _logger = loggerProvider != null 
                ? loggerProvider.CreateLogger(GetType().Name) 
                : NullLogger<MessageDispatcher>.Instance;
            _connection = new HubConnectionBuilder()
                          .ConfigureLogging(lb => lb.SetMinimumLevel(options.Value.LogLevel)
                                                    .AddProvider(loggerProvider))
                          .WithUrl(options.Value.HubEndpoint)
                          .WithAutomaticReconnect(new ForeverRetryPolicy())
                          .Build();
            _connection.Reconnected += _ => SubscribeAll();
            _irrelevantSubjectsConductor = Observable.Interval(TimeSpan.FromMinutes(10))
                                                     .Do(_ => UnsubscribeFromIrrelevantTopics())
                                                     .Retry()
                                                     .Subscribe();
        }

        public void Dispose() => _irrelevantSubjectsConductor?.Dispose();

        public IObservable<T> On<T>()
        {
            lock (_syncObj)
            {
                if (!_subjects.Any() && _connection.State == HubConnectionState.Disconnected)
                {
                    _connection.ConnectWithRetryAsync().ContinueWith(
                        t =>
                        {
                            _logger.LogDebug(message: "Event listening started");
                            SubscribeAll();
                        },
                        TaskContinuationOptions.NotOnFaulted);
                }

                var eventName = EventNameAttribute.GetNameOrDefault(typeof(T));
                var wasSubscription = true;
                var sub = _subjects.GetOrAdd(eventName,
                                             () =>
                                             {
                                                 wasSubscription = false;
                                                 return new Subject<object>();
                                             });
                if (!wasSubscription)
                {
                    if (_connection.State == HubConnectionState.Connected)
                    {
                        TrySubscribe(eventName);
                    }
                }

                _connection.On<T>(eventName).Subscribe(message => sub.OnNext(message));
                return sub.Cast<T>();   
            }
        }

        private Task SubscribeAll() => Task.WhenAll(_subjects.Select(pair => TrySubscribe(pair.Key)));

        private Task TrySubscribe(string eventName) =>
            _connection.SendCoreAsync("subscribe", new object[] { eventName })
                       .ContinueWith(
                           t => _logger.LogError(
                               t.Exception,
                               "Error occurred while subscribe on '{topic}'",
                               eventName),
                           TaskContinuationOptions.OnlyOnFaulted);

        private Task UnsubscribeFromIrrelevantTopics()
        {
            lock (_syncObj)
            {
                var toUnsubscribeTopics = _subjects.Where(sub => !sub.Value.HasObservers).Select(x => x.Key);
                var tasks = new List<Task>();
                foreach (var topic in toUnsubscribeTopics)
                {
                    _subjects.Remove(topic);
                    tasks.Add(_connection.SendCoreAsync("unsubscribe", new object[] { topic })
                                         .ContinueWith(
                                             t => _logger.LogError(
                                                 t.Exception,
                                                 "Error occurred while subscribe on '{topic}'", topic),
                                             TaskContinuationOptions.OnlyOnFaulted));
                }

                return Task.WhenAll(tasks);
            }
        }
    }
}