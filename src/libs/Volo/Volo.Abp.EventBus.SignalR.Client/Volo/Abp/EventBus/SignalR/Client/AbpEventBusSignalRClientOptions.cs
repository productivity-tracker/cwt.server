﻿using System;
using Microsoft.Extensions.Logging;

namespace Volo.Abp.EventBus.SignalR.Client
{
    public class AbpEventBusSignalRClientOptions
    {
        public string ServerUrl { get; set; }
        public string HubEndpoint => new Uri(new Uri(ServerUrl), "/real-time/events").AbsoluteUri;

        public LogLevel LogLevel { get; set; } = LogLevel.Debug;
    }
}