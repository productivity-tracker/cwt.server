﻿using System;

namespace Volo.Abp.EventBus.SignalR.Client
{
    public interface IMessageDispatcher
    {
        IObservable<T> On<T>();
    }
}