﻿using System;
using EmployeeManagement.Employees.DTO;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Extensions.Logging;
using Volo.Abp.Domain.Entities.Events.Distributed;

namespace Volo.Abp.EventBus.SignalR.Client.Test
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var app = AbpApplicationFactory.Create<EventBusSignalRClientTestModule>(
                appOpt =>
                {
                    appOpt.Services.AddLogging(
                        builder => builder.AddProvider(new SerilogLoggerProvider(
                                                    new LoggerConfiguration()
                                                        .MinimumLevel.Debug()
                                                        .Enrich.FromLogContext()
                                                        .WriteTo.ColoredConsole()
                                                        .CreateLogger())));
                    appOpt.Services.Configure<AbpEventBusSignalRClientOptions>(
                        opt => opt.ServerUrl = "https://localhost:5010");
                });
            app.Initialize();

            var logger = app.ServiceProvider.GetRequiredService<ILogger<Program>>();

            var dispatcher = app.ServiceProvider.GetRequiredService<IMessageDispatcher>();
            dispatcher.On<EntityCreatedEto<EmployeeDto>>().Subscribe(x => logger.LogInformation("Created {Id}", x.Entity.Id));
            dispatcher.On<EntityDeletedEto<EmployeeDto>>().Subscribe(x => logger.LogInformation("Deleted {Id}", x.Entity.Id));

            dispatcher.On<EntityUpdatedEto<EmployeeDto>>().Subscribe(x => logger.LogInformation("Updated {Id}", x.Entity.Id));

            Console.ReadKey();
        }
    }
}
