﻿using Volo.Abp.Modularity;

namespace Volo.Abp.EventBus.SignalR.Client.Test
{
    [DependsOn(typeof(AbpEventBusSignalRClientModule))]
    public class EventBusSignalRClientTestModule : AbpModule
    {
        
    }
}