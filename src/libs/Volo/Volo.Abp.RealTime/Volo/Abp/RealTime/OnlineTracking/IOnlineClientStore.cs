﻿using System.Collections.Generic;

namespace Volo.Abp.RealTime.OnlineTracking
{
    internal interface IOnlineClientStore
    {
        /// <summary>
        /// Adds a client.
        /// </summary>
        /// <param name="client">The client.</param>
        void Add(IOnlineClient client);

        /// <summary>
        /// Determines if store contains client with connection id.
        /// </summary>
        /// <param name="connectionId">The connection id.</param>
        bool Contains(string connectionId);

        /// <summary>
        /// Gets all online clients.
        /// </summary>
        IReadOnlyList<IOnlineClient> GetAll();

        /// <summary>
        /// Removes a client by connection id.
        /// </summary>
        /// <param name="connectionId">The connection id.</param>
        /// <returns>true if the client is removed, otherwise, false</returns>
        bool Remove(string connectionId);

        /// <summary>
        /// Gets a client by connection id.
        /// </summary>
        /// <param name="connectionId">The connection id.</param>
        /// <returns>true if the client exists, otherwise, false</returns>
        bool TryGet(string connectionId, out IOnlineClient client);

        /// <summary>
        /// Removes a client by connection id.
        /// </summary>
        /// <param name="connectionId">The connection id.</param>
        /// <returns>true if the client is removed, otherwise, false</returns>
        bool TryRemove(string connectionId, out IOnlineClient client);
    }
}