﻿using JetBrains.Annotations;

namespace Volo.Abp.RealTime.OnlineTracking
{
    public class OnlineUserEventArgs : OnlineClientEventArgs
    {
        public string UserId { get; }

        public OnlineUserEventArgs([NotNull]string userId, IOnlineClient client)
            : base(client)
        {
            UserId = userId;
        }
    }
}