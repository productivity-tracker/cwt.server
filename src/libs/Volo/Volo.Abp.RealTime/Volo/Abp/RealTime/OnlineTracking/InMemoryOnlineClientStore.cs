﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using Volo.Abp.DependencyInjection;

namespace Volo.Abp.RealTime.OnlineTracking
{
    internal class InMemoryOnlineClientStore : IOnlineClientStore, ISingletonDependency
    {
        /// <summary>
        /// Online clients.
        /// </summary>
        protected ConcurrentDictionary<string, IOnlineClient> Clients { get; }

        public InMemoryOnlineClientStore()
        {
            Clients = new ConcurrentDictionary<string, IOnlineClient>();
        }

        public void Add(IOnlineClient client) => 
            Clients.AddOrUpdate(client.ConnectionId, client, (s, o) => client);

        public bool Remove(string connectionId) => 
            TryRemove(connectionId, out _);

        public bool TryRemove(string connectionId, out IOnlineClient client) => 
            Clients.TryRemove(connectionId, out client);

        public bool TryGet(string connectionId, out IOnlineClient client) => 
            Clients.TryGetValue(connectionId, out client);

        public bool Contains(string connectionId) => 
            Clients.ContainsKey(connectionId);

        public IReadOnlyList<IOnlineClient> GetAll() => 
            Clients.Values.ToImmutableList();
    }
}