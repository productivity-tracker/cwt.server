﻿using System;
using System.Collections.Generic;

namespace Volo.Abp.RealTime.OnlineTracking
{
    public class OnlineClient : IOnlineClient
    {
        private Dictionary<string, object> _properties;

        /// <summary>
        /// Unique connection Id for this client.
        /// </summary>
        public string ConnectionId { get; private set; }

        /// <summary>
        /// IP address of this client.
        /// </summary>
        public string IpAddress { get; private set; }

        /// <summary>
        /// User Id.
        /// </summary>
        public string UserId { get; private set; }

        /// <summary>
        /// Connection establishment time for this client.
        /// </summary>
        public DateTime ConnectTime { get; private set; }

        /// <summary>
        /// Shortcut to set/get <see cref="Properties" />.
        /// </summary>
        public object this[string key]
        {
            get => Properties[key];
            set => Properties[key] = value;
        }

        /// <summary>
        /// Can be used to add custom properties for this client.
        /// </summary>
        public Dictionary<string, object> Properties
        {
            get => _properties;
            set => _properties = value ?? throw new ArgumentNullException(nameof(value));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OnlineClient" /> class.
        /// </summary>
        public OnlineClient()
        {
            ConnectTime = DateTime.UtcNow;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OnlineClient" /> class.
        /// </summary>
        /// <param name="connectionId">The connection identifier.</param>
        /// <param name="ipAddress">The ip address.</param>
        /// <param name="userId">User id</param>
        public OnlineClient(string connectionId, string ipAddress, string userId)
            : this()
        {
            ConnectionId = connectionId;
            IpAddress = ipAddress;
            UserId = userId;

            Properties = new Dictionary<string, object>();
        }
    }
}