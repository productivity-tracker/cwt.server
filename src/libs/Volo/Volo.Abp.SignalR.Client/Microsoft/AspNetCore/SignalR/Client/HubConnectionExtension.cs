﻿using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Microsoft.AspNetCore.SignalR.Client
{
    public static class HubConnectionExtension
    {
        /// <summary>
        /// Keep trying to until we can start or the token is canceled.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="delayMs"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public static async Task<bool> ConnectWithRetryAsync(
            this HubConnection connection,
            ushort delayMs = 5000,
            CancellationToken token = default)
        {
            while (true)
            {
                try
                {
                    await connection.StartAsync(token);
                    Debug.Assert(connection.State == HubConnectionState.Connected);
                    return true;
                }
                catch when (token.IsCancellationRequested)
                {
                    return false;
                }
                catch
                {
                    Debug.Assert(connection.State == HubConnectionState.Disconnected);
                    await Task.Delay(delayMs, token);
                }
            }
        }
    }
}