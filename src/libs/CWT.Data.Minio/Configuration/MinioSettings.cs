﻿namespace CWT.Data.Minio.Configuration
{
    public class MinioSettings
    {
        public string Endpoint { get; set; }

        public string AccessKey { get; set; }

        public string SecretKey { get; set; }

        public string Region { get; set; }

        public string BucketName { get; set; }

        public bool IsSecureConnection { get; set; }
    }
}