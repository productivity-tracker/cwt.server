﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using CWT.Data.Minio.Configuration;
using CWT.Data.Minio.Model;
using CWT.Domain.Abstractions.Repository;
using Microsoft.Extensions.Logging;
using Minio;
using Volo.Abp.Threading;

namespace CWT.Data.Minio
{
    public class MinioBlobRepository<T> : IBlobRepository<T>
    {
        private readonly ILogger _logger;

        private readonly MinioClient _minio;

        protected string BucketName { get; }
        protected string Location { get; }

        protected IObjectMapper<T> Mapper { get; }

        public MinioBlobRepository(ILoggerFactory logeFactory, MinioSettings minioSettings, IObjectMapper<T> mapper)
        {
            if(minioSettings == null) throw new ArgumentNullException(nameof(minioSettings));

            _logger = logeFactory.CreateLogger(GetType());
            _minio = new MinioClient(minioSettings.Endpoint, minioSettings.AccessKey, minioSettings.SecretKey);
            if (minioSettings.IsSecureConnection)
            {
                _minio = _minio.WithSSL();
            }

            BucketName = minioSettings.BucketName;
            Location = minioSettings.Region;
            Mapper = mapper;
        }

        public void Delete(string objectName) => AsyncHelper.RunSync(() => DeleteAsync(objectName));

        public async Task DeleteAsync(string objectName) => await _minio.RemoveObjectAsync(BucketName, objectName);

        public void DeleteMany(IEnumerable<string> objectNames)
            => AsyncHelper.RunSync(() => DeleteManyAsync(objectNames));

        public async Task DeleteManyAsync(IEnumerable<string> objectNames)
        {
            var observable = await _minio.RemoveObjectAsync(BucketName, objectNames);
            observable.Subscribe(deleteError 
                                     => _logger.LogError("Error while delete object '{ObjectName}', (HostId). {Message}",
                                                         deleteError.Key, deleteError.HostId, deleteError.Message), 
                                 ex => _logger.LogError($"OnError: {ex}"),
                                 () => _logger.LogDebug($"Remove completed ({BucketName})"));
            observable.Wait();
        }

        public string Put(string objectName, Stream stream, string contentType = null, Dictionary<string, string> metaData = null) => AsyncHelper.RunSync(() => PutAsync(objectName, stream, contentType, metaData));

        public async Task<string> PutAsync(string objectName,
                                           Stream stream,
                                           string contentType = null,
                                           Dictionary<string, string> metaData = null)
        {
            var found = await _minio.BucketExistsAsync(BucketName);
            if (!found)
            {
                await _minio.MakeBucketAsync(BucketName, Location);
            }

            await _minio.PutObjectAsync(BucketName, objectName, stream, stream.Length, contentType, metaData);

            return BucketName;
        }

        public string Get(string objectName, Stream stream)
            => AsyncHelper.RunSync(() => GetAsync(objectName, stream));

        public async Task<string> GetAsync(string objectName, Stream stream)
        {
            await _minio.GetObjectAsync(BucketName, objectName, memoryData =>
            {
                memoryData.CopyTo(stream);
                memoryData.Dispose();
            });

            stream.Position = 0L;

            var objectInfo = await GetInfo(objectName);
            return objectInfo?.ContentType;
        }

        public async Task<ObjectInfo> GetInfo(string objectName)
        {
            var info = await _minio.StatObjectAsync(BucketName, objectName);
            return new ObjectInfo
            {
                ObjectName = info.ObjectName,
                ContentType = info.ContentType
            };
        }

        public IObservable<T> GetListObjectsAsync(string prefix, bool recursive, CancellationToken cancellationToken = default) =>
            _minio.ListObjectsAsync(BucketName, prefix, recursive)
                  .Select(Mapper.Map);
    }
}