﻿using Minio.DataModel;

namespace CWT.Data.Minio
{
    public interface IObjectMapper<T>
    {
        T Map(Item item);
    }
}