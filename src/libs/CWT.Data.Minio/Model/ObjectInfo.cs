﻿namespace CWT.Data.Minio.Model
{
    public class ObjectInfo
    {
        public string ObjectName { get; set; }
        public string ContentType { get; set; }
    }
}